﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;
using Periscope;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class CellLicenseTrigger : MonoBehaviour
{
    [SerializeField] private Button targetButton = null;

    [SerializeField] private Cell cell = null;

    [SerializeField] private CellSelectHandler cellSelectHandler = null;

    [SerializeField] private Image targetGraphic;

    [SerializeField] private CanvasGroup licenseDetails;
    [SerializeField] private TextMeshProUGUI licenseTitle;
    [SerializeField] private TextMeshProUGUI licenseCreator;
    [SerializeField] private TextMeshProUGUI licenseType;
    [SerializeField] private Button licenseTitleLink;
    [SerializeField] private Button licenseCreatorLink;
    [SerializeField] private Button licenseTypeLink;

    private string _url;
    private bool _isOn;

    private void Awake()
    {
        Debug.Assert(cell != null, $"Trigger requires a {nameof(Cell)} reference.");
        Debug.Assert(targetButton != null, $"Trigger requires a {nameof(Button)} reference.");
        Debug.Assert(cellSelectHandler != null, $"Trigger requires a {nameof(CellSelectHandler)} reference.");
        Debug.Assert(targetGraphic != null, $"Trigger requires a {nameof(Image)} reference.");

        licenseDetails.interactable = false;
        licenseDetails.alpha = 0;
        _isOn = false;
    }

    private void Start()
    {
        if (cell == null)
            return;

        targetButton.onClick.AddListener(ToggleDetails);

        Debug.Assert(cell != null);
        Debug.Assert(cellSelectHandler != null);
    }

    public void ToggleDetails()
    {
        _isOn = !_isOn;
        UpdateDetailsView();
    }

    private void UpdateDetailsView()
    {
        var details = JsonConvert.DeserializeObject<CcImageDetail>(cell.LicenseInfo);
        Debug.Log($"Show License Info: {details}");
        licenseDetails.gameObject.SetActive(_isOn);
        licenseDetails.alpha = _isOn ? 1.0f : 0;
        licenseDetails.interactable = _isOn;
        licenseDetails.blocksRaycasts = _isOn;
        if (_isOn)
        {
            licenseCreator.text = $"<b>Creator:</b> {details.creator}";
            licenseTitle.text = $"<b>Title:</b> {details.title}";
            licenseType.text =
                $"<b>License:</b> CC {details.license.ToUpper(CultureInfo.InvariantCulture)} {details.license_version}";
            licenseTitleLink.onClick.AddListener(() => Application.OpenURL(details.url));
            licenseCreatorLink.onClick.AddListener(() => Application.OpenURL(details.creator_url));
            licenseTypeLink.onClick.AddListener(() => Application.OpenURL(details.license_url));
        }
    }

    public void Update()
    {
        var hasCcImageDetail = !string.IsNullOrEmpty(cell.LicenseInfo);
        targetButton.interactable = hasCcImageDetail;
    }
}