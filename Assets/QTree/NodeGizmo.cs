using UnityEngine;

namespace QTree
{
    [RequireComponent(typeof(RectTransform))]
    public class NodeGizmo : MonoBehaviour
    {
        public QuadTree<Transform> Target;

        /// <summary>
        /// Refresh was called at least once.
        /// </summary>
        public bool IsInitialized { get; private set; }

        private void Update()
        {
            // kill self if target object is gone
            if (Target != null && (Target.IsOrphan || Target.Parent == null))
            {
                //if (runInEditMode)
                //    DestroyImmediate(gameObject);
                //else
                    Destroy(gameObject);
            }
        }

        public void Refresh()
        {
            if (Target != null)
            {
                var gizmoRt = GetComponent<RectTransform>();
                gizmoRt.SetParent(transform.parent);
                gizmoRt.position = Target.Boundary.center;
                gizmoRt.anchorMin = Vector2.one * 0.5f;
                gizmoRt.anchorMax = Vector2.one * 0.5f;
                gizmoRt.pivot = Vector2.one * 0.5f;
                gizmoRt.sizeDelta = Target.Boundary.size;
                IsInitialized = true;
            }
        }

        private void OnDisable()
        {
            IsInitialized = false;
        }
    }
}