using UnityEngine;
using UnityEngine.Events;


namespace QTree
{
    public class TransformEvent : UnityEvent<TransformObserverComponent>
    {
    }

    public class TransformObserverComponent : MonoBehaviour
    {
        public TransformEvent PositionChanged;
        private Vector3 _pos;

        private void Update()
        {
            if (!transform.hasChanged || _pos == transform.position) return;

            PositionChanged?.Invoke(this);
            transform.hasChanged = false;
            _pos = transform.position;
        }
    }
}