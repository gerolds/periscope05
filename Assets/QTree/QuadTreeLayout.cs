//#define MUNKRES_ON

using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
#if MUNKRES_ON
using Accord.Math.Optimization;

#endif

namespace QTree
{
    public class QuadTreeLayout : LayoutGroup
    {
        private QuadTree<Transform> _tree;

        //[SerializeField] private NodeGizmo nodeGizmoPrefab = null;
        //private RectTransform rectTransform => GetComponent<RectTransform>();

        public IReadOnlyList<Transform> GetItemsAtPos(Vector2 pos)
        {
            if (_tree.Contains(pos))
            {
                var leafCell = QuadTree<Transform>.GetLeafCell(_tree, pos);
                return leafCell.Bucket;
            }

            throw new ArgumentOutOfRangeException(nameof(pos), $"{pos} is out of bounds.");
        }

        public Vector2 GetPosCellBoundarySize(Vector2 pos)
        {
            if (_tree.Contains(pos))
            {
                var leafCell = QuadTree<Transform>.GetLeafCell(_tree, pos);
                return leafCell.Boundary.size;
            }

            throw new ArgumentOutOfRangeException(nameof(pos), $"{pos} is out of bounds.");
        }

        public bool Contains(Vector2 pos) => _tree.Contains(pos);

        private QuadTree<Transform> Tree
        {
            get
            {
                var minDimension = Mathf.Min(rectTransform.rect.width, rectTransform.rect.height);
                if (_tree == null)
                    _tree = new QuadTree<Transform>(
                        boundary: new Bounds(
                            rectTransform.position,
                            new Vector3(minDimension, minDimension, minDimension)),
                        parent: null,
                        bucketSizeRoot: bucketSizeRoot);
                return _tree;
            }
        }

        [FormerlySerializedAs("BucketSizeRoot")] [Range(1, 4)]
        public byte bucketSizeRoot = 1;

        protected override void OnEnable()
        {
            base.OnEnable();
            //Debug.Log($"{nameof(QuadTreeLayout)} ENABLING");

            QuadTree<Transform>.NodeCollapsed.AddListener(NodeCollapsedHandler);
            QuadTree<Transform>.NodeRemoved.AddListener(NodeRemovedHandler);
            QuadTree<Transform>.NodeCreated.AddListener(NodeCreatedHandler);
            QuadTree<Transform>.NodeSplit.AddListener(NodeSplitHandler);
            QuadTree<Transform>.ItemAdded.AddListener(ItemAddedHandler);
            QuadTree<Transform>.ItemsAdded.AddListener(ItemsAddedHandler);
            QuadTree<Transform>.ItemRemoved.AddListener(ItemRemovedHandler);

            RebuildLayout();

            //Debug.Log($"{nameof(QuadTreeLayout)} ENABLED");
        }

        protected override void OnDisable()
        {
            //Debug.Log($"{nameof(QuadTreeLayout)} DISABLING");
            Tree.Clear();
            Debug.Assert(Tree.Height == 0);
            QuadTree<Transform>.NodeCollapsed.RemoveListener(NodeCollapsedHandler);
            QuadTree<Transform>.NodeCreated.RemoveListener(NodeCreatedHandler);
            QuadTree<Transform>.NodeSplit.RemoveListener(NodeSplitHandler);
            QuadTree<Transform>.ItemAdded.RemoveListener(ItemAddedHandler);
            QuadTree<Transform>.ItemsAdded.RemoveListener(ItemsAddedHandler);
            QuadTree<Transform>.ItemRemoved.RemoveListener(ItemRemovedHandler);
            //Debug.Log($"{nameof(QuadTreeLayout)} DISABLED");

            base.OnDisable();
        }

        protected override void Start()
        {
            base.Start();
            RebuildLayout();
        }

        private void Update()
        {
            var treeParametersChanged = Tree.BucketSizeRoot != bucketSizeRoot;
            if (treeParametersChanged)
            {
                _tree = null;
                RebuildLayout();
            }
        }

        /// <summary>
        /// When a single item is removed we DO NOT re-layout the cell it was removed from.
        /// </summary>
        /// <param name="node">The node that the item was removed from.</param>
        /// <param name="item">The item that was removed.</param>
        private void ItemRemovedHandler(QuadTree<Transform> node, Component item)
        {
            Debug.Assert(node.IsLeaf, "Items can only be removed from leaf nodes");
            //Debug.Log($"Item removed from node@{node.Bounds.center}");
        }

        /// <summary>
        /// When multiple items were removed we re-layout the cell they were removed from.
        /// </summary>
        /// <param name="node">The node that the item was removed from.</param>
        private void ItemsRemovedHandler(QuadTree<Transform> node)
        {
            //Debug.Assert(node.IsLeaf, "Items can only be removed from leaf nodes");
#if MUNKRES_ON
            LayoutBucketItemsInGrid(node);
#else
            LayoutBucketItem(node);
#endif
            //Debug.Log($"Item removed from node@{node.Bounds.center}");
        }

        /// <summary>
        /// When a single item is added we DO NOT re-layout the cell it was added to.
        /// </summary>
        /// <param name="node">The node that the item was added to.</param>
        /// <param name="item">The item that was added.</param>
        private void ItemAddedHandler(QuadTree<Transform> node, Component item)
        {
            Debug.Assert(node.IsLeaf, "Items can only be added to leaf nodes");
            //Debug.Log($"Item <{item.name}> added to node@{node.Bounds.center}");
        }

        /// <summary>
        /// When multiple items were added we re-layout the cell they were added to.
        /// </summary>
        /// <param name="node">The node that the item was added to.</param>
        /// <param name="item">The item that was added.</param>
        private void ItemsAddedHandler(QuadTree<Transform> node)
        {
            //Debug.Assert(node.IsLeaf, "Items can only be added to leaf nodes");
#if MUNKRES_ON
            LayoutBucketItemsInGrid(node);
#else
            LayoutBucketItem(node);
#endif
            //Debug.Log($"Item <{item.name}> added to node@{node.Bounds.center}");
        }

        private static void LayoutBucketItem(QuadTree<Transform> node)
        {
            if (node.IsInner)
            {
                foreach (var childNode in node.ChildNodes)
                {
                    LayoutBucketItem(childNode);
                }

                return;
            }

            if (node.IsEmpty)
                return;

            Debug.Assert(node.IsLeaf, $"{nameof(LayoutBucketItem)} called on inner node");
            Debug.Assert(!node.IsEmpty, $"{nameof(LayoutBucketItem)} called on empty node");

            //Debug.Log($"NODE@{node.Bounds.center} laying out bucket items.");

            var rows = 1;
            var cols = 1;
            var tileCount = 1;

            Debug.Assert(node.Bucket != null);
            Debug.Assert(node.Bucket.Any());
            var bucketItem = node.Bucket.FirstOrDefault();
            var nodeCenter = (Vector2) node.Boundary.center;
            var nodeExtents = (Vector2) node.Boundary.extents;

            var tileSize = Vector2.one * (node.Boundary.size / node.BucketSizeRoot);
            var tilePivot = Vector2.one * 0.5f;
            var tileExtents = tileSize * 0.5f;

            // calculate the bounds of all tiles in the current node
            var tileCorner = Vector2.zero;
            var tileCenter = tileCorner + tileExtents;
            var tileCenterOffsetFromNodeCenter = tileCenter - nodeExtents;
            var tileCenterInWorld = tileCenterOffsetFromNodeCenter + nodeCenter;
            var tile = new Bounds(tileCenterInWorld, tileSize);

            var itemRectTransform = (RectTransform) bucketItem;
            itemRectTransform.pivot = tilePivot;
            itemRectTransform.anchorMin = tilePivot;
            itemRectTransform.anchorMax = tilePivot;
            itemRectTransform.DOSizeDelta(tileSize, 0.25f);
            itemRectTransform.position = tile.center;
        }

#if MUNKRES_ON
        /// <summary>
        /// Moves and scales all bucket items in such a way that they fall into a N x N grid (N = BucketSizeRoot)
        /// centered on their <paramref name="node"/>'s center.
        /// </summary>
        /// <param name="node">The node to layout.</param>
        private static void LayoutBucketItemsInGrid(QuadTree<Transform> node)
        {
            if (node.IsInner)
            {
                foreach (var childNode in node.ChildNodes)
                {
                    LayoutBucketItemsInGrid(childNode);
                }

                return;
            }

            if (node.IsEmpty)
                return;

            Debug.Assert(node.IsLeaf, $"{nameof(LayoutBucketItemsInGrid)} called on inner node");
            Debug.Assert(!node.IsEmpty, $"{nameof(LayoutBucketItemsInGrid)} called on empty node");

            //Debug.Log($"NODE@{node.Bounds.center} laying out bucket items.");

            var rows = node.BucketSizeRoot;
            var cols = node.BucketSizeRoot;
            var bucketSize = node.BucketSize;

            Debug.Assert(node.Bucket != null);
            Debug.Assert(node.Bucket.Any());
            var bucketItems = node.Bucket.ToList();
            var nodeCenter = (Vector2) node.Boundary.center;
            var nodeExtents = (Vector2) node.Boundary.extents;

            var tileSize = Vector2.one * (node.Boundary.size / node.BucketSizeRoot);
            var tilePivot = Vector2.one * 0.5f;
            var tileExtents = tileSize * 0.5f;

            // calculate the bounds of all tiles in the current node
            var tiles = new Bounds[cols * rows];

            for (int y = 0; y < rows; y++)
            {
                for (int x = 0; x < cols; x++)
                {
                    var tileCorner = new Vector2(x * tileSize.x, y * tileSize.y);
                    var tileCenter = tileCorner + tileExtents;
                    var tileCenterOffsetFromNodeCenter = tileCenter - nodeExtents;
                    var tileCenterInWorld = tileCenterOffsetFromNodeCenter + nodeCenter;
                    var tile = new Bounds(tileCenterInWorld, tileSize);
                    tiles[x + cols * y] = tile;
                }
            }

            // calculate the error matrix for item positions and tile centers
            var errorMatrix = new double[bucketItems.Count][];
            for (var itemIdx = 0; itemIdx < bucketItems.Count; itemIdx++)
            {
                errorMatrix[itemIdx] = new double[bucketSize];
                for (var tileIdx = 0; tileIdx < bucketSize; tileIdx++)
                {
                    var itemPosition = bucketItems[itemIdx].transform.position;
                    var tilePosition = tiles[tileIdx].center;
                    var posError = (tilePosition - itemPosition).sqrMagnitude; // square of euclidean distance
                    errorMatrix[itemIdx][tileIdx] = posError;
                }
            }

            // solve linear assignment problem (LAP)
            Munkres lap = new Munkres(errorMatrix);
            var success = lap.Minimize();
            var solution = lap.Solution;
            var minError = lap.Value;
            //Debug.Log($"Munkres: {string.Join(", ", solution)}");

            // assign items to tiles based on the LAP-solution
            for (int i = 0; i < bucketItems.Count; i++)
            {
                var itemRectTransform = (RectTransform) bucketItems[i];
                itemRectTransform.pivot = tilePivot;
                itemRectTransform.anchorMin = tilePivot;
                itemRectTransform.anchorMax = tilePivot;
                //itemRectTransform.sizeDelta = tileSize;
                itemRectTransform.DOSizeDelta(tileSize, 0.25f);
                //itemRectTransform.DOMove(tiles[(int) solution[i]].center, 0.25f);
                itemRectTransform.position = tiles[(int) solution[i]].center;
            }
        }
#endif

        private static void NodeSplitHandler(QuadTree<Transform> node)
        {
            //Debug.Log($"NODE@{node.Bounds.center} SPLIT");

            Debug.Assert(node.IsInner, "After a split a node is always an inner node");

            // node split means:
            // --> nothing handled here
        }

        private static void NodeCreatedHandler(QuadTree<Transform> node)
        {
            //Debug.Log($"NODE@{node.Bounds.center} CREATED");

            Debug.Assert(node.IsLeaf, "A newly created node is always a leaf node.");
            Debug.Assert(node.Count == 0, "A newly created node is always empty.");

            // node created means:
            // --> nothing handled here, TODO: we have yet no widget for nodes.
        }

        private static void NodeRemovedHandler(QuadTree<Transform> node)
        {
            //Debug.Log($"NODE@{node.Bounds.center} COLLAPSED");


            Debug.Assert(node.IsEmpty, "A removed node is always empty.");

            // node collapse means:
            // --> nothing handled here, TODO: we have yet no widget for nodes.
        }

        /*private NodeGizmo MakeNodeGizmo(QuadTree<Transform> node)
        {
            var gizmo = Instantiate(nodeGizmoPrefab);
            gizmo.Target = node;
            gizmo.transform.SetParent(transform.parent);
            gizmo.Refresh();
            return gizmo;
        }*/

        private static void NodeCollapsedHandler(QuadTree<Transform> node)
        {
            //Debug.Log($"NODE@{node.Bounds.center} COLLAPSED");


            Debug.Assert(node.IsOrphan, "A collapsed node is always orphaned.");
            Debug.Assert(node.IsEmpty, "A collapsed node is always empty.");

            // node collapse means:
            // --> nothing handled here, TODO: we have yet no widget for nodes.
        }

        public void Add(Transform item)
        {
            Tree.AddMultiple(item);
        }

        public void Remove(Transform item)
        {
            Tree.RemoveMultiple(item);
        }

        public void PositionChangedHandler(Transform t)
        {
            Remove(t);
            Add(t);
        }

        private void RebuildLayout()
        {
            //Debug.Log($"Rebuilding QTreeLayout");

            Tree.Clear();
            var children = new Transform[transform.childCount];
            for (int i = 0; i < transform.childCount; i++)
                children[i] = transform.GetChild(i);
            Tree.AddMultiple(children);

            //Debug.Log($"QTreeLayout rebuilt with {Tree.Count} items, {Tree.Height} height");
        }

        public override void CalculateLayoutInputHorizontal()
        {
            //Debug.Log("SET HORIZONTAL LAYOUT");
            RebuildLayout();
        }

        public override void CalculateLayoutInputVertical()
        {
            //Debug.Log("SET VERTICAL LAYOUT");
            //RebuildLayout();
        }

        public override void SetLayoutHorizontal()
        {
            //Debug.Log("ON SetLayoutHorizontal");
        }

        public override void SetLayoutVertical()
        {
            //Debug.Log("ON SetLayoutVertical");
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            // dispose all gizmos
        }
    }
}