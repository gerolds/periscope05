using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;

namespace QTree
{
    public class QuadTree<T> where T : Transform
    {
        public class QuadTreeNodeEvent : UnityEvent<QuadTree<T>>
        {
        }

        public class QuadTreeItemEvent : UnityEvent<QuadTree<T>, T>
        {
        }

        public readonly byte BucketSizeRoot;
        public byte BucketSize => (byte) Mathf.Clamp(BucketSizeRoot * BucketSizeRoot, 0, 255);
        public int Depth => IsRoot ? 0 : Parent.Depth + 1;
        public int Height => IsLeaf ? 0 : _childCells.Max(cell => cell.Height) + 1;

        // EVENTS
        public static QuadTreeNodeEvent NodeSplit = new QuadTreeNodeEvent();
        public static QuadTreeNodeEvent NodeCreated = new QuadTreeNodeEvent();
        public static QuadTreeNodeEvent NodeCollapsed = new QuadTreeNodeEvent();
        public static QuadTreeNodeEvent NodeRemoved = new QuadTreeNodeEvent();
        public static QuadTreeNodeEvent ItemsAdded = new QuadTreeNodeEvent();
        public static QuadTreeNodeEvent ItemsRemoved = new QuadTreeNodeEvent();
        public static QuadTreeItemEvent ItemAdded = new QuadTreeItemEvent();
        public static QuadTreeItemEvent ItemRemoved = new QuadTreeItemEvent();

        // FIELDS
        private bool _isOrphan; // true after node was merged into parent node.
        private Bounds _boundary;
        private readonly QuadTree<T>[] _childCells;
        private readonly HashSet<T> _bucket = new HashSet<T>();

        // CORE PROPERTIES
        public IReadOnlyList<T> Bucket => IsLeaf ? _bucket.ToList().AsReadOnly() : null;
        public QuadTree<T>[] ChildNodes => _childCells.ToArray();
        public Bounds Boundary => _boundary;

        // MORE PROPERTIES
        public QuadTree<T> Parent { get; private set; }
        public bool IsLeaf => _childCells.All(i => i == null);
        public bool IsInner => _childCells.All(i => i != null);
        public bool IsFull => _bucket.Count == BucketSize;
        public bool IsEmpty => _bucket.Count == 0;
        public bool IsRoot => Parent == null && !IsOrphan;
        public bool IsOrphan => Parent == null && _isOrphan; // true after node was merged into parent node.
        public bool IsCollapsible => !IsLeaf && Count <= BucketSize;

        // recursive count
        public int Count
        {
            get
            {
                if (IsInner)
                    return _childCells.Sum(i => i.Count);
                return _bucket.Count;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public QuadTree([NotNull] Bounds boundary, QuadTree<T> parent, byte bucketSizeRoot)
        {
            if (bucketSizeRoot > 4)
            {
                Debug.LogWarning(
                    $"{nameof(QuadTree<T>)} parameter <{nameof(bucketSizeRoot)}> modified to 4 (was {bucketSizeRoot}).");
                bucketSizeRoot = 4;
            }

            if (bucketSizeRoot < 1)
            {
                Debug.LogWarning(
                    $"{nameof(QuadTree<T>)} parameter <{nameof(bucketSizeRoot)}> modified to 1 (was {bucketSizeRoot}).");
                bucketSizeRoot = 1;
            }

            BucketSizeRoot = bucketSizeRoot;
            _boundary = boundary;
            Parent = parent;
            _childCells = new QuadTree<T>[4];
            //Debug.Log($"Tree created with {bounds} and bucket size {BucketSize}");
            NodeCreated?.Invoke(this);
        }

        public void AddMultiple([NotNull] params T[] objs)
        {
            foreach (var obj in objs)
                Add(obj);
            ItemsAdded?.Invoke(this);
        }

        /// <summary>
        /// Given this tree's <see cref="Boundary"/> contain the <paramref name="obj"/>'s position, add it to this tree.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool Add(T obj)
        {
            if (obj == null)
                return false;

            if (!Boundary.Contains(obj.position))
                return false;

            if (Contains(obj))
            {
                Debug.Log($"Obj already contained in {this}.");
                return true;
            }

            if (IsLeaf)
            {
                // Not full, add to this node
                if (!IsFull)
                {
                    var count = _bucket.Count;
                    _bucket.Add(obj);
                    Debug.Assert(count + 1 == _bucket.Count);
                    ItemAdded?.Invoke(this, obj);
                    return true;
                }

                // Full, split and add to dependent
                if (IsFull)
                {
                    //if (_bucket.Any(item => Vector3.Distance(obj.position, item.position) < 1f))
                    if (_bucket.Any(item => (obj.position - item.position).sqrMagnitude < 1f))
                    {
                        Debug.LogError(
                            "Can't add item so close to existing item.");
                        return false;
                    }

                    Split();
                    foreach (var child in _childCells)
                    {
                        //Debug.Assert(!child.IsFull);
                        if (child.Add(obj))
                            return true;
                    }

                    Debug.LogWarning("Leaf after Split: obj didn't fit inside any child cells. This should not happen.");
                    return false;
                }
            }
            else if (IsInner)
            {
                // recursion
                foreach (var child in _childCells)
                    if (child.Add(obj))
                        return true;

                Debug.LogWarning("Inner: obj didn't fit inside any child cells. This should not happen.");
                return false;
            }

            Debug.LogError("Unexpected: This should not happen.");
            return false;
        }

        // recursive add
//        public bool Add(Vector2 pos, T obj)
//        {
//            if (!_bounds.Contains(pos))
//                return false;
//
//            var container = new ObjectContainer();
//            container.Obj = obj;
//            container.Pos = pos;
//
//            return Add(obj);
//        }

        // recursive remove
        public bool Remove(T obj)
        {
            if (_bucket.Remove(obj))
            {
                ItemRemoved?.Invoke(this, obj);
                if (IsCollapsible)
                    Fold();
                return true;
            }

            foreach (var child in _childCells)
            {
                if (child.Remove(obj))
                {
                    if (IsCollapsible)
                        Fold();
                    return true;
                }
            }

            return false;
        }

        public void RemoveMultiple(params T[] objs)
        {
            foreach (var obj in objs)
                Remove(obj);
            ItemsRemoved?.Invoke(this);
        }

        public void Fold()
        {
            if (IsCollapsible)
            {
                // collapse dependents into this node
                foreach (var childNode in _childCells)
                {
                    // if dependent is inner node, recursive collapse
                    if (childNode.IsCollapsible)
                    {
                        Debug.Assert(childNode.IsInner);
                        childNode.Fold();
                    }

                    // if dependent is leaf node
                    Debug.Assert(childNode.IsLeaf);
                    foreach (var item in childNode.Bucket)
                    {
                        childNode.Remove(item);
                        this.Add(item);
                    }

                    childNode.Clear(); // just to be sure

                    childNode._isOrphan = true;
                    childNode.Parent = null;
                }

                ReleaseDependents();

                NodeCollapsed?.Invoke(this);
            }
        }

        private void ReleaseDependents()
        {
            //Debug.Log("Make leaf node (set all cells to null)");
            for (int i = 0; i < _childCells.Length; i++)
            {
                var node = _childCells[i];
                node._isOrphan = true; // set flag marking cell as unconnected
                node.Parent = null; // clear back-reference
                _childCells[i] = null; // clear forward-reference
                NodeRemoved?.Invoke(node);
            }
        }

        public override string ToString()
        {
            return $"Node@{Boundary.center}[ extent: {Boundary.extents}, cells: {_childCells}, bucket: {_bucket} ]";
        }

        public void Clear()
        {
            //Debug.Log($"Clearing node@{Boundary.center}");

            foreach (var item in _bucket.ToList())
                _bucket.Remove(item);
            _bucket.Clear();

            if (IsInner)
            {
                foreach (var node in _childCells)
                    node.Clear();
                ReleaseDependents();
            }

            ItemsRemoved?.Invoke(this);
        }

        private void Split()
        {
            Debug.Assert(Count == BucketSize, "Only Split full nodes");
            //Debug.Log($"Splitting node@{Boundary.center}.");
            //Debug.Log($"Collecting node@{Boundary.center}'s bucket items for redistribution.");
            var itemsToDistribute = _bucket.ToList();
            //Debug.Log($"Clearing node@{Bounds.center}'s bucket");
            _bucket.Clear();

            var subCellSize = _boundary.extents;
            Vector3 cell2CenterOffset = new Vector3(1f, 1f, 0) * 0.5f;
            Vector3 cell1CenterOffset = new Vector3(-1f, 1f, 0) * 0.5f;
            Vector3 cell3CenterOffset = new Vector3(1f, -1f, 0) * 0.5f;
            Vector3 cell4CenterOffset = new Vector3(-1f, -1f, 0) * 0.5f;
            var cell1Center = _boundary.center + Vector3.Scale(_boundary.extents, cell1CenterOffset);
            var cell2Center = _boundary.center + Vector3.Scale(_boundary.extents, cell2CenterOffset);
            var cell3Center = _boundary.center + Vector3.Scale(_boundary.extents, cell3CenterOffset);
            var cell4Center = _boundary.center + Vector3.Scale(_boundary.extents, cell4CenterOffset);
            var cell1Bounds = new Bounds(cell1Center, subCellSize);
            var cell2Bounds = new Bounds(cell2Center, subCellSize);
            var cell3Bounds = new Bounds(cell3Center, subCellSize);
            var cell4Bounds = new Bounds(cell4Center, subCellSize);
            //Debug.Log("Creating new tree for cell 0");
            _childCells[0] = new QuadTree<T>(cell1Bounds, this, BucketSizeRoot);
            //Debug.Log("Creating new tree for cell 1");
            _childCells[1] = new QuadTree<T>(cell2Bounds, this, BucketSizeRoot);
            //Debug.Log("Creating new tree for cell 2");
            _childCells[2] = new QuadTree<T>(cell3Bounds, this, BucketSizeRoot);
            //Debug.Log("Creating new tree for cell 3");
            _childCells[3] = new QuadTree<T>(cell4Bounds, this, BucketSizeRoot);

            //Debug.Log($"Distributing node@{Boundary.center}'s items to new cells");
            foreach (var item in itemsToDistribute)
            {
                foreach (var childCell in _childCells)
                {
                    if (childCell.Contains(item.position))
                    {
                        childCell.Add(item);
                        break;
                    }
                }
            }

            Debug.Assert(IsEmpty, "Has empty bucket");
            Debug.Assert(IsInner, "Has 4 dependent nodes");
            NodeSplit?.Invoke(this);
        }

        public bool Contains(Vector3 pos) => _boundary.Contains(pos);

        public bool Contains(Component item)
        {
            if (!Contains(item.transform.position))
                return false;
            if (IsLeaf)
                return _bucket.Contains(item);
            var cellFound = _childCells.FirstOrDefault(cell => cell.Contains(item));
            return cellFound != null;
        }

        /// <summary>
        /// Returns the smallest node <see cref="UnityEngine.Bounds"/> that would currently contain <paramref name="pos"/>
        /// </summary>
        /// <remarks>Tile size for <paramref name="pos"/> would be returned <see cref="UnityEngine.Bounds"/>.<see cref="UnityEngine.Bounds.size"/>
        /// / <see cref="QuadTree{T}"/>.<see cref="QuadTree{T}.BucketSizeRoot"/>.</remarks>
        public Bounds FindSmallestBounds(Vector3 pos)
        {
            if (Boundary.Contains(pos))
            {
                Renderer x;
                    
                if (IsLeaf)
                    return Boundary;
                var cell = ChildNodes.First(c => c.Contains(pos));
                return cell.FindSmallestBounds(pos);
            }

            throw new ArgumentOutOfRangeException($"Cell with Boundary {Boundary} does not contain {pos}.");
        }

        [NotNull] public static QuadTree<T> GetLeafCell([NotNull] QuadTree<T> t, Vector3 pos)
        {
            if (!t.Contains(pos))
                throw new ArgumentOutOfRangeException(nameof(pos), $"{pos} is out of bounds.");
            
            if (t.IsLeaf)
                return t;

            Debug.Assert(t._childCells != null, "non-leaf cells always have four children");
            
            foreach (var cell in t._childCells)
            {
                if (cell.Contains(pos))
                {
                    if (cell.IsLeaf)
                        return cell;
                    else
                        return GetLeafCell(cell, pos);
                }
            }

            throw new ArgumentOutOfRangeException(nameof(pos), $"{pos} is out of bounds.");
        }
    }
}
