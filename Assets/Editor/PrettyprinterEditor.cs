using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Periscope
{
    [CustomEditor(typeof(Prettyprinter))]
    public class PrettyprinterEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            FileInfo f;
            Prettyprinter t = (Prettyprinter) target;
            if (t.root == null)
                f = new FileInfo(Application.dataPath + $"/{SceneManager.GetActiveScene().name.AlphaNumOnly()}.gv");
            else
                f = new FileInfo(Application.dataPath + $"/{t.root.name.AlphaNumOnly()}.gv");

            if (GUILayout.Button("Generate w/ record labels"))
            {
                t.Walk();
            }
            
            if (GUILayout.Button("Generate w/ plain labels"))
            {
                t.WalkPlain();
            }

            if (GUILayout.Button("Generate w/ HTML labels"))
            {
                t.WalkHtml();
            }

            if (GUILayout.Button("Copy to clipboard"))
            {
                EditorGUIUtility.systemCopyBuffer = t.output;
            }

            var style = new GUIStyle(GUI.skin.label);
            style.wordWrap = true;
            GUILayout.Label($"Save path: {f.FullName}", style, GUILayout.Height(50));

            if (GUILayout.Button($"Write to file"))
            {
                try
                {
                    if (f.Exists)
                        f.Delete();
                    File.WriteAllText(f.FullName, t.output);
                }
                catch (Exception e)
                {
                    Debug.Log(e.Message);
                }

                Debug.Log($"{nameof(Prettyprinter)} output saved to {f.FullName}");
            }
        }
    }
}