// Only works on ARGB32, RGB24 and Alpha8 textures that are marked readable

using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Periscope
{
    /// <summary>
    /// Based on http://wiki.unity3d.com/index.php/TextureScale
    /// Adapted to async/await concurrency.
    /// </summary>
    public class TextureScale
    {
        private static Color[] _texColors;
        private static Color[] _newColors;
        private static int _w;
        private static float _ratioX;
        private static float _ratioY;
        private static int _w2;

        public static void Point(Texture2D tex, int newWidth, int newHeight)
        {
            var task = ScaleAsync(tex, newWidth, newHeight, false);
            Task.Run(async () => await task).Wait();
        }
        
        public static void Bilinear(Texture2D tex, int newWidth, int newHeight)
        {
            var task = ScaleAsync(tex, newWidth, newHeight, true);
            Task.Run(async () => await task).Wait();
        }

        public static async Task PointAsync(Texture2D tex, int newWidth, int newHeight)
        {
            await ScaleAsync(tex, newWidth, newHeight, true);
        }
        
        public static async Task BilinearAsync(Texture2D tex, int newWidth, int newHeight)
        {
            await ScaleAsync(tex, newWidth, newHeight, true);
        }

        private static async Task ScaleAsync(Texture2D tex, int newWidth, int newHeight, bool useBilinear)
        {
            _texColors = tex.GetPixels();
            _newColors = new Color[newWidth * newHeight];
            if (useBilinear)
            {
                _ratioX = 1.0f / ((float) newWidth / (tex.width - 1));
                _ratioY = 1.0f / ((float) newHeight / (tex.height - 1));
            }
            else
            {
                _ratioX = ((float) tex.width) / newWidth;
                _ratioY = ((float) tex.height) / newHeight;
            }

            _w = tex.width;
            _w2 = newWidth;
            var cores = Mathf.Max(1, Mathf.Min(SystemInfo.processorCount, newHeight));
            var slice = newHeight / cores;

            int i = 0;
            var tasks = new List<Task>();
            for (i = 0; i < cores - 1; i++)
            {
                var t = useBilinear
                    ? BilinearScaleAsync((start: slice * i, end: slice * (i + 1)))
                    : PointScale((start: slice * i, end: slice * (i + 1)));
                tasks.Add(t);
            }

            await Task.WhenAll(tasks);

            tex.Resize(newWidth, newHeight);
            tex.SetPixels(_newColors);
            tex.Apply();

            _texColors = null;
            _newColors = null;
        }

        public static async Task BilinearScaleAsync((int start, int end) slice)
        {
            for (var y = slice.start; y < slice.end; y++)
            {
                int yFloor = (int) Mathf.Floor(y * _ratioY);
                var y1 = yFloor * _w;
                var y2 = (yFloor + 1) * _w;
                var yw = y * _w2;

                for (var x = 0; x < _w2; x++)
                {
                    int xFloor = (int) Mathf.Floor(x * _ratioX);
                    var xLerp = x * _ratioX - xFloor;
                    _newColors[yw + x] = ColorLerpUnclamped(
                        ColorLerpUnclamped(_texColors[y1 + xFloor], _texColors[y1 + xFloor + 1], xLerp),
                        ColorLerpUnclamped(_texColors[y2 + xFloor], _texColors[y2 + xFloor + 1], xLerp),
                        y * _ratioY - yFloor);
                }
            }

            await Task.CompletedTask;
        }

        public static async Task PointScale((int start, int end) slice)
        {
            for (var y = slice.start; y < slice.end; y++)
            {
                var thisY = (int) (_ratioY * y) * _w;
                var yw = y * _w2;
                for (var x = 0; x < _w2; x++)
                {
                    _newColors[yw + x] = _texColors[(int) (thisY + _ratioX * x)];
                }
            }
            await Task.CompletedTask;

        }

        private static Color ColorLerpUnclamped(Color c1, Color c2, float value)
        {
            return new Color(c1.r + (c2.r - c1.r) * value,
                c1.g + (c2.g - c1.g) * value,
                c1.b + (c2.b - c1.b) * value,
                c1.a + (c2.a - c1.a) * value);
        }
    }
}