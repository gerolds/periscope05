using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

namespace Periscope
{
    public partial class Periscope
    {
        #region Big mess of things (Refactor this)

        public StackCellDropHandler GetActiveStack()
        {
            return stackToggleGroup.ActiveToggles().First().GetComponent<StackCellDropHandler>();
        }

        public RectTransform GetContainerDropTarget(ContainerId target)
        {
            switch (target)
            {
                case ContainerId.Canvas1:
                    return canvas1Target;
                case ContainerId.Canvas2:
                    return canvas2Target;
                case ContainerId.Canvas3:
                    return canvas3Target;
                case ContainerId.Canvas4:
                    return canvas4Target;
                case ContainerId.Stack1:
                    return stack1Target;
                case ContainerId.Stack2:
                    return stack2Target;
                case ContainerId.Stack3:
                    return stack3Target;
                case ContainerId.Stack4:
                    return stack4Target;
                case ContainerId.Stack5:
                    return stack5Target;
                case ContainerId.Trash:
                    return stack6Target;
                case ContainerId.Canvas5:
                case ContainerId.Canvas6:
                default:
                    throw new ArgumentOutOfRangeException(nameof(target), target, null);
            }
        }

        public bool IsInWorldSpace(ContainerId target)
            => GetContainerDropTarget(target).GetComponentInParent<Canvas>().rootCanvas.renderMode ==
               RenderMode.WorldSpace;

        public StackCellDropHandler GetContainerDropHandler(ContainerId target)
        {
            switch (target)
            {
                case ContainerId.Canvas1:
                    return canvas1Target.GetComponentInParent<StackCellDropHandler>();
                case ContainerId.Canvas2:
                    return canvas2Target.GetComponentInParent<StackCellDropHandler>();
                case ContainerId.Canvas3:
                    return canvas3Target.GetComponentInParent<StackCellDropHandler>();
                case ContainerId.Canvas4:
                    return canvas4Target.GetComponentInParent<StackCellDropHandler>();
                case ContainerId.Stack1:
                    return stack1Target.GetComponentInParent<StackCellDropHandler>();
                case ContainerId.Stack2:
                    return stack2Target.GetComponentInParent<StackCellDropHandler>();
                case ContainerId.Stack3:
                    return stack3Target.GetComponentInParent<StackCellDropHandler>();
                case ContainerId.Stack4:
                    return stack4Target.GetComponentInParent<StackCellDropHandler>();
                case ContainerId.Stack5:
                    return stack5Target.GetComponentInParent<StackCellDropHandler>();
                case ContainerId.Trash:
                    return stack6Target.GetComponentInParent<StackCellDropHandler>();
                case ContainerId.Canvas5:
                case ContainerId.Canvas6:
                default:
                    throw new ArgumentOutOfRangeException(nameof(target), target, null);
            }
        }

        public Transform GetActiveStackDropTarget()
        {
            return stackToggleGroup.ActiveToggles().First().GetComponent<StackCellDropHandler>().dropTarget;
        }

        private (Texture2D texture, Vector2 focus) _peristentCursor = (null, Vector2.zero);
        private (bool active, Texture2D texture, Vector2 focus) _momentaryCursor = (false, null, Vector2.zero);

        [FormerlySerializedAs("DefaultCursor")] [SerializeField]
        private Texture2D defaultCursor = null;

        [FormerlySerializedAs("PanCursor")] [SerializeField]
        private Texture2D panCursor = null;

        [FormerlySerializedAs("TextCursor")] [SerializeField]
        private Texture2D textCursor = null;

        [FormerlySerializedAs("DrawCursor")] [SerializeField]
        private Texture2D drawCursor = null;

        public void InjectCursor(Texture2D tex, Vector2 focus)
        {
            _injectCursor = true;
            _injectedCursor = tex;
            _injectedCursorFocus = focus;
        }

        public void EndCursorInjection()
        {
            _injectCursor = false;
            _injectedCursor = null;
            _injectedCursorFocus = Vector2.zero;
        }

        private void UpdateCursor()
        {
            _momentaryCursor = (false, null, Vector2.zero);
            Cursor.SetCursor(_peristentCursor.texture, _peristentCursor.focus, CursorMode.Auto);

            if (_injectCursor)
                _momentaryCursor = (true, _injectedCursor, _injectedCursorFocus);

            if (InputRouter.MomentaryPanning)
                _momentaryCursor = (true, panCursor, new Vector2(16.0f, 16.0f));

            if (InputRouter.MomentaryPanTool)
                _momentaryCursor = (true, panCursor, new Vector2(16.0f, 16.0f));

            if (_momentaryCursor.active)
                Cursor.SetCursor(_momentaryCursor.texture, _momentaryCursor.focus, CursorMode.Auto);
        }

        public void SetModeDefault()
        {
            InputRouter.Mode = InputMode.Default;
            _peristentCursor = (defaultCursor, new Vector2(2.0f, 2.0f));
            Cursor.SetCursor(_peristentCursor.texture, _peristentCursor.focus, CursorMode.Auto);
            Debug.Log($"Input Mode: {InputRouter.Mode}");
        }

        public void SetModeDrawing()
        {
            InputRouter.Mode = InputMode.Drawing;
            _peristentCursor = (drawCursor, new Vector2(16.0f, 16.0f));
            Cursor.SetCursor(_peristentCursor.texture, _peristentCursor.focus, CursorMode.Auto);
            Debug.Log($"Input Mode: {InputRouter.Mode}");
        }

        public void SetModeTextInput()
        {
            InputRouter.Mode = InputMode.TextEdit;
            _peristentCursor = (textCursor, new Vector2(16.0f, 25.0f));
            Cursor.SetCursor(_peristentCursor.texture, _peristentCursor.focus, CursorMode.Auto);
            Debug.Log($"Input Mode: {InputRouter.Mode}");
        }

        #endregion
    }
}