using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Periscope
{
//#define DEBUG_ON

    public class InputRouter : MonoBehaviour
    {
        public static InputMode Mode = InputMode.Default;

        public static bool MomentaryPanning =>
            (Mode == InputMode.Default || Mode == InputMode.Drawing)
            && (
                MomentaryPanTool && Input.GetKey(KeyCode.Mouse0) 
                ||
                Input.GetKey(KeyCode.Mouse2)
            );

        public static bool CmdFrameAll =>
            (Mode == InputMode.Default || Mode == InputMode.Drawing)
            && MomentaryAlt
            && Input.GetKeyDown(KeyCode.F);
        
        public static bool CmdResetView =>
            (Mode == InputMode.Default || Mode == InputMode.Drawing)
            && MomentaryShift
            && Input.GetKeyDown(KeyCode.F);

        public static bool CmdFrameSelected =>
            (Mode == InputMode.Default || Mode == InputMode.Drawing)
            && Input.GetKeyDown(KeyCode.F)
            && !CmdResetView
            && !CmdFrameAll;

        public static bool MomentaryPanTool =>
            (
                Mode == InputMode.Default ||
                Mode == InputMode.Drawing
            )
            && (Input.GetKey(KeyCode.Space));

        public static bool MomentaryCtrl =>
            (Mode == InputMode.Default || Mode == InputMode.Drawing)
            && (
                Input.GetKey(KeyCode.LeftControl) ||
                Input.GetKey(KeyCode.RightControl) ||
                Input.GetKey(KeyCode.LeftCommand) ||
                Input.GetKey(KeyCode.RightCommand)
            );
        
        public static bool CmdRandomise =>
            Mode == InputMode.Default
            && MomentaryCtrl
            && MomentaryShift
            && Input.GetKeyDown(KeyCode.R);

        public static bool MomentaryShift =>
            (Mode == InputMode.Default || Mode == InputMode.Drawing)
            && (
                Input.GetKey(KeyCode.LeftShift) ||
                Input.GetKey(KeyCode.RightShift)
            );

        public static bool MomentarySpace =>
            (Mode == InputMode.Default || Mode == InputMode.Drawing)
            && Input.GetKey(KeyCode.Space);

        public static bool MomentaryAlt =>
            (Mode == InputMode.Default || Mode == InputMode.Drawing)
            && (
                Input.GetKey(KeyCode.LeftAlt) ||
                Input.GetKey(KeyCode.RightAlt)
            );
        
        public static bool CmdCopy =>
            Mode == InputMode.Default
            && MomentaryCtrl
            && Input.GetKeyDown(KeyCode.C);
        
        public static bool CmdPaste =>
            Mode == InputMode.Default
            && MomentaryCtrl
            && Input.GetKeyDown(KeyCode.V);

        public static bool CmdShowContextMenu =>
            (Mode == InputMode.Default || Mode == InputMode.Drawing)
            && Input.GetKeyDown(KeyCode.Mouse1);

        public static bool CmdSelectStackOne =>
            Mode == InputMode.Default
            && Input.GetKeyDown(KeyCode.Alpha1);

        public static bool CmdSelectStackTwo =>
            Mode == InputMode.Default
            && Input.GetKeyDown(KeyCode.Alpha2);

        public static bool CmdSelectStackThree =>
            Mode == InputMode.Default
            && Input.GetKeyDown(KeyCode.Alpha3);

        public static bool CmdSelectStackFour =>
            Mode == InputMode.Default
            && Input.GetKeyDown(KeyCode.Alpha4);

        public static bool CmdSelectStackFive =>
            Mode == InputMode.Default
            && Input.GetKeyDown(KeyCode.Alpha5);

        public static bool CmdSelectStackSix =>
            Mode == InputMode.Default
            && Input.GetKeyDown(KeyCode.Alpha6);

        public static bool CmdSendToStack =>
            Mode == InputMode.Default
            && MomentaryCtrl
            && Input.GetKeyDown(KeyCode.X);

        public static bool CmdPullFromStack =>
            Mode == InputMode.Default
            && MomentaryCtrl
            && Input.GetKeyDown(KeyCode.V);

        public static bool AnyKey =>
            (Mode == InputMode.Default || Mode == InputMode.Drawing)
            && Input.anyKey;

        public static bool CmdEditCell =>
            Mode == InputMode.Default
            && (
                Input.GetKeyDown(KeyCode.Return)
                || Input.GetKeyDown(KeyCode.KeypadEnter)
            );

        public static bool CmdCancel
            => Input.GetKeyDown(KeyCode.Escape);

        public static bool CmdDivideCell =>
            Mode == InputMode.Default
            && !MomentaryAlt
            && Input.GetKeyDown(KeyCode.D);
        
        public static bool CmdDuplicateCell =>
            Mode == InputMode.Default
            && MomentaryAlt
            && Input.GetKeyDown(KeyCode.D);
        
        public static bool CmdMarkCell =>
            Mode == InputMode.Default
            //&& MomentaryCtrl
            && Input.GetKeyDown(KeyCode.M);
        
        public static bool CmdPlacePicture =>
            Mode == InputMode.Default
            //&& MomentaryCtrl
            && Input.GetKeyDown(KeyCode.P);

        public static bool CmdDeleteCell =>
            (Mode == InputMode.Default || Mode == InputMode.Drawing || Mode == InputMode.TextEdit)
            && (Input.GetKeyDown(KeyCode.Delete) || Input.GetKeyDown(KeyCode.Backspace));

        public static bool CmdSave =>
            (Mode == InputMode.Default || Mode == InputMode.Drawing || Mode == InputMode.TextEdit)
            && MomentaryCtrl
            && Input.GetKeyDown(KeyCode.S);

        public static bool CmdSaveAs =>
            (Mode == InputMode.Default || Mode == InputMode.Drawing || Mode == InputMode.TextEdit)
            && MomentaryCtrl
            && MomentaryShift
            && Input.GetKeyDown(KeyCode.S);

        public static bool CmdOpen =>
            (Mode == InputMode.Default || Mode == InputMode.Drawing || Mode == InputMode.TextEdit)
            && MomentaryCtrl
            && Input.GetKeyDown(KeyCode.O);

        public static bool CmdNew =>
            (Mode == InputMode.Default || Mode == InputMode.Drawing || Mode == InputMode.TextEdit)
            && MomentaryCtrl
            && Input.GetKeyDown(KeyCode.N);

        public static bool CmdImport =>
            (Mode == InputMode.Default || Mode == InputMode.Drawing || Mode == InputMode.TextEdit)
            && MomentaryCtrl
            && Input.GetKeyDown(KeyCode.I);
        
        public static bool CmdRight =>
            Mode == InputMode.Default
            && Input.GetKeyDown(KeyCode.RightArrow);
        
        public static bool CmdLeft =>
            Mode == InputMode.Default
            && Input.GetKeyDown(KeyCode.LeftArrow);
        
        public static bool CmdUp =>
            Mode == InputMode.Default
            && Input.GetKeyDown(KeyCode.UpArrow);
        
        public static bool CmdDown =>
            Mode == InputMode.Default
            && Input.GetKeyDown(KeyCode.DownArrow);
        
        public static bool CmdVertical =>
            CmdUp || CmdDown;
        
        public static bool CmdHorizontal =>
            CmdLeft || CmdRight;

        public static bool CmdTogglePanels =>
            (Mode == InputMode.Default || Mode == InputMode.Drawing)
            && Input.GetKeyDown(KeyCode.Caret);

        public GameObject DefaultSelect;

        private void Update()
        {
            // TODO: Unhook this from the update loop?
            
            if (InputRouter.CmdCancel)
            {
                Debug.Log("DESELECT ALL");
                EventSystem.current.SetSelectedGameObject(DefaultSelect);
            }
        }

#if DEBUG_ON
        private void OnGUI()
        {
            var yStart = Screen.height / 2f;
            var lineHeight = 20f;
            var lineY = yStart;
            GUI.Label(new Rect(10, lineY, 100, 20), $"{nameof(Mode)}: {Mode}");
            lineY += lineHeight;
            GUI.Label(new Rect(10, lineY, 100, 20), $"{nameof(MomentaryPanView)}: {MomentaryPanView}");
        }
#endif
    }


    public enum InputMode
    {
        TextEdit,
        Drawing,
        Default
    }
}