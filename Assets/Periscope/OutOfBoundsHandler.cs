﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Periscope
{
    public class OutOfBoundsHandler : MonoBehaviour, IDropHandler, IPointerClickHandler
    {
        public void OnDrop(PointerEventData eventData)
        {
            Periscope.Singleton.message.Log("Dropped out of Bounds", MessagePresenter.MessageType.Warning);
            eventData.pointerDrag.transform.DOMove(
                Camera.main.ScreenToWorldPoint(eventData.pressPosition),
                0.25f);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Periscope.Singleton.message.Log("Clicked out of Bounds", MessagePresenter.MessageType.Warning);
        }
    }
}