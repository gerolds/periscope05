using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Periscope
{
    public class InteractableOnSize : MonoBehaviour
    {
        private float _ratio = 1.0f;
        [FormerlySerializedAs("target")] public Selectable selectableTarget = null;
        public float minRelativeSize = 0.125f;
        public float maxRelativeSize = 1.1f;
        public float minAbsoluteSize = 1;
        public float maxAbsoluteSize = 4097;
        public Camera referenceCamera = null;
        public RectTransform referenceRect = null;

        private void Awake()
        {
            Debug.Assert(selectableTarget != null,
                $"{nameof(InteractableOnSize)} requires a {nameof(Selectable)} reference.");
            Debug.Assert(selectableTarget != null,
                $"{nameof(InteractableOnSize)} requires a {nameof(RectTransform)} reference.");
        }

        private void Start()
        {
            if (referenceCamera == null)
                referenceCamera = Camera.main;
            Debug.Assert(referenceCamera != null, "Camera.main is always != null");
            if (!referenceCamera.orthographic)
                Debug.LogWarning($"Camera must be orthographic for {nameof(InteractableOnSize)} to work.");
        }

        void Update()
        {
            
            if (!referenceCamera.orthographic)
                return;

            UpdateInteractable();
        }

        /// <summary>
        /// disable the selectable based on its relative size when outside the specified range.
        /// </summary>
        private void UpdateInteractable()
        {
            var sizeDelta = referenceRect.sizeDelta;
            var avgSize = (sizeDelta.x + sizeDelta.y) / 2f;
            _ratio = avgSize / (referenceCamera.orthographicSize * 2f);
            var outsideAbsoluteBounds = referenceRect.rect.width <= minAbsoluteSize ||
                                        referenceRect.rect.width >= maxAbsoluteSize ||
                                        referenceRect.rect.height <= minAbsoluteSize ||
                                        referenceRect.rect.height >= maxAbsoluteSize;
            var outsideRelativeBounds = _ratio < minRelativeSize ||
                                        _ratio > maxRelativeSize;

            selectableTarget.interactable = !outsideRelativeBounds && !outsideAbsoluteBounds;
        }
    }
}