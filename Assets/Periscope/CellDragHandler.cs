﻿using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Periscope
{
    public class CellDragHandler :
        MonoBehaviour,
        IBeginDragHandler,
        IDragHandler,
        IEndDragHandler,
        IPointerEnterHandler,
        IPointerExitHandler
    {
        [SerializeField] public DragGizmo dragGizmo;
        [SerializeField] private Color dragColor = Color.yellow;
        [SerializeField] private Color defaultColor = Color.white;
        [SerializeField] private Image graphicTarget = null;
        private bool _canDropHere = false;
        private Vector3 _dragStartPos = Vector3.zero;
        private bool _dragInterrupted = false;
        private bool BlockDrag => InputRouter.MomentarySpace || InputRouter.Mode == InputMode.Drawing;

        private void Start()
        {
            if (dragGizmo == null)
                Debug.LogError($"{nameof(CellDragHandler)} not assigned a drag gizmo.");
        }

        public void OnBeginDrag(PointerEventData data)
        {
            if (BlockDrag)
                return;
            _dragInterrupted = false;
            Debug.Log($"{name} OnBeginDrag called.");
            dragGizmo.gameObject.SetActive(true);
            _dragStartPos = Camera.main.WorldToScreenPoint(transform.position);
            graphicTarget.color = dragColor;
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }

        public void OnDrag(PointerEventData eventData)
        {
            // interrupt/terminate dragging of the cell when panning mode was started
            if (!_dragInterrupted && InputRouter.MomentaryPanning)
            {
                OnEndDrag(eventData);
                _dragInterrupted = true;
            }

            if (_dragInterrupted || BlockDrag)
            {
                dragGizmo.transform.position = Vector3.zero;
                return;
            }

            var gizmo = dragGizmo.transform;
            gizmo.position = eventData.position;
            gizmo.localScale = Vector3.one; // * Camera.main.orthographicSize / 512f;

            // refresh only if pointer leaves the rect of the currently dragged (position and size need to change only then)
            // OnPointerExit triggers unreliably for this purpose
            var rectTrans = (RectTransform) eventData.pointerDrag.transform;
            Debug.Assert(rectTrans != null);
            Debug.Assert(eventData.pointerCurrentRaycast.screenPosition != null);
            var pointerInsideDraggableRect = RectTransformUtility.RectangleContainsScreenPoint(
                rect: rectTrans,
                screenPoint: Input.mousePosition,
                cam: Camera.main
            );

            // while dragging, observe valid/invalid drop locations
            var dropRecvObj = eventData.hovered.FirstOrDefault(x => x.GetComponent(typeof(ICellDrop)) != null);
            if (dropRecvObj != null)
            {
                // pointer is over an object that can receive a drop
                dragGizmo.SetValid(true);
                _canDropHere = true;

                if (pointerInsideDraggableRect)
                    return;

                // while dragging over a qtree cell, actually move the cell around
                var qTreeDropHandler = dropRecvObj.GetComponent(typeof(QTreeCellDropHandler)) as QTreeCellDropHandler;
                if (qTreeDropHandler != null)
                {
                    var dragWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    dragWorldPos.z = 0;

                    // if the position change would make the cell too small, skip the update
                    if (qTreeDropHandler.CanReceiveDrop(dragWorldPos))
                    {
                        eventData.pointerDrag.transform.position = dragWorldPos;
                        LayoutRebuilder.MarkLayoutForRebuild((RectTransform) eventData.pointerDrag.transform);
                        transform.SetAsLastSibling();
                    }
                    else
                    {
                        dragGizmo.SetValid(false);
                        _canDropHere = false;
                    }
                }
            }
            else
            {
                // pointer is outside a valid drop area
                dragGizmo.SetValid(false);
                _canDropHere = false;
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (_dragInterrupted)
                return;

            Debug.Log($"{name} OnEndDrag called.");
            dragGizmo.gameObject.SetActive(false);

            // drag post protocol
            LayoutRebuilder.MarkLayoutForRebuild(transform as RectTransform);
            graphicTarget.color = defaultColor;
            GetComponent<CanvasGroup>().blocksRaycasts = true;
            _canDropHere = false;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
        }

        public void OnPointerExit(PointerEventData eventData)
        {
        }
    }
}