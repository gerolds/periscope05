using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Design;
using System.IO;
using System.Linq;
using System.Net;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Periscope
{
    public class CellEvent : UnityEvent<Cell>
    {
    }

    // TODO: refactor this... the class should only handle the "selection" behaviour
    [RequireComponent(typeof(RectTransform))]
    public class CellSelectHandler :
        Selectable
    {
        private int _frameOfLastSelect;
        
        public Cell cellData = null;
        [SerializeField] private Image markedTargetGraphic = null;
        //[SerializeField] internal CellDetail cellDetailTarget = null;
        [SerializeField] private bool isSelected = false;
        
        public CellEvent selected;
        public CellEvent deselected;
        public CellEvent marked;
        public CellEvent unMarked;

        public bool IsSelected => isSelected;
        public bool IsResponsive => !InputRouter.MomentaryPanTool;
        public bool WasSelectedThisFrame => _frameOfLastSelect == Time.frameCount;
        protected override void Awake()
        {
            base.Awake();

            Debug.Assert(GetComponentInParent(typeof(Canvas)) != null,
                $"{nameof(CellSelectHandler)} only works on a {nameof(Canvas)}");

            if (MarkedCells.Contains(this))
                markedTargetGraphic.enabled = true;
            else
                markedTargetGraphic.enabled = false;
            if (selected == null) selected = new CellEvent();
            if (deselected == null) deselected = new CellEvent();
            //cellDetailTarget = Periscope.Singleton.cellDetailPanel;
            
            _allCells.Add(this);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            _allCells.Remove(this);
            RemoveMarked(this);
        }

        public override void Select()
        {
            // prevent selecting a cell while panning
            if (!IsResponsive)
                return;
                
            base.Select();
            //InputRouter.Mode = InputMode.Default;
            //CellDetailTarget.SetDataObject(cellData);

            _frameOfLastSelect = Time.frameCount;
            isSelected = true;
            if (InputRouter.MomentaryShift)
                AddMarked(this);
            else if (InputRouter.MomentaryCtrl)
                ToggleMarked(this);
            else
                ClearAndSetMarked(this);
        }

        public void Deselect()
        {
            if (!IsResponsive)
                return;
            
            InputRouter.Mode = InputMode.Default;
            isSelected = false;
            //RemoveMarked(this);
        }

        public override void OnSelect(BaseEventData eventData)
        {
            if (!IsResponsive)
                return;
            base.OnSelect(eventData);
            Debug.Log($"{name} OnSelect called.");
            Select();
        }

        public override void OnDeselect(BaseEventData eventData)
        {
            if (!IsResponsive)
                return;
            base.OnDeselect(eventData);
            Debug.Log($"{name} OnDeselect called.");
            Deselect();
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            base.OnPointerEnter(eventData);
            Periscope.Singleton.lastHoveredCell = GetComponent<Cell>();
        }

        #region Marking Cells

        public static List<CellSelectHandler> MarkedCells = new List<CellSelectHandler>();
        private static List<CellSelectHandler> _allCells = new List<CellSelectHandler>();
        public static ReadOnlyCollection<CellSelectHandler> AllCells => _allCells.AsReadOnly();

        public void MarkExclusive()
        {
            ClearAndSetMarked(this);
        }
        
        public void UnMark()
        {
            RemoveMarked(this);
        }
        
        public void Mark()
        {
            AddMarked(this);
        }
        
        public static void ClearAndSetMarked(CellSelectHandler cell)
        {
            if (!cell.IsResponsive)
                return;
            
            ClearMarked(false);
            MarkedCells.Add(cell);
            cell.markedTargetGraphic.enabled = true;
            cell.marked?.Invoke(cell.cellData);
            CellDetail.Notify();
        }

        public static void AddMarked(CellSelectHandler cell)
        {
            if (!cell.IsResponsive)
                return;
            
            if (!MarkedCells.Contains(cell))
            {
                MarkedCells.Add(cell);
                cell.markedTargetGraphic.enabled = true;
                cell.marked?.Invoke(cell.cellData);
                CellDetail.Notify();
            }
        }

        public static void RemoveMarked(CellSelectHandler cell, bool notify = true)
        {
            if (!cell.IsResponsive)
                return;
            
            MarkedCells.Remove(cell);
            cell.markedTargetGraphic.enabled = false;
            cell.unMarked?.Invoke(cell.cellData);
            if (notify)
                CellDetail.Notify();
        }

        public static void ToggleMarked(CellSelectHandler cell)
        {
            if (!cell.IsResponsive)
                return;
            
            if (!MarkedCells.Contains(cell))
                AddMarked(cell);
            else
                RemoveMarked(cell);
        }

        public static void ClearMarked(bool notify = true)
        {
            for (var i = MarkedCells.Count - 1; i >= 0; i--)
            {
                if (MarkedCells[i] != null)
                {
                    RemoveMarked(MarkedCells[i], false);
                }
            }

            MarkedCells.Clear();
            if (notify)
                CellDetail.Notify();
        }

        #endregion

    }
}