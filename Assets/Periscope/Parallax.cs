﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Periscope
{
    public class Parallax : MonoBehaviour
    {
         public Camera parallaxCamera = null;
         public RectTransform parallaxTarget = null;
         public float parallaxDistance = 512f;

        // Update is called once per frame
        void Update()
        {
            // TODO: Unhook this from the update loop?
            parallaxTarget.pivot = new Vector2(
                parallaxCamera.transform.position.x / parallaxDistance + 0.5f,
                parallaxCamera.transform.position.y / parallaxDistance + 0.5f
            );
        }
    }
}