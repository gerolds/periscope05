using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Periscope
{
    public static class StringExtensions
    {
        public static string Truncate(this string value, int maxLength, bool ellispis)
        {
            if (string.IsNullOrEmpty(value)) return value;
            if (ellispis)
                return value.Length <= maxLength ? value : value.Substring(0, maxLength) + "…";
            else
                return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        public static string AlphaNumOnly(this string value)
        {
            char[] arr = value.ToCharArray();

            arr = Array.FindAll<char>(arr, (c => (char.IsLetterOrDigit(c) 
                                                  || char.IsWhiteSpace(c) 
                                                  || c == '-')));
            return new string(arr);
        }
        
        public static uint ToUInt32(this int value)
        {
            var bytes = BitConverter.GetBytes(value);
            return BitConverter.ToUInt32(bytes, 0);
        }
    }
}