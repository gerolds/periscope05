﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Periscope
{
    public class ContextMenu : MonoBehaviour
    {
         [SerializeField] private CanvasGroup targetGroup = null;
         [SerializeField] private Camera referenceCamera = null;

        // Start is called before the first frame update
        void Awake()
        {
        }

        // Update is called once per frame
        void Update()
        {
            // TODO: Unhook this from the update loop?
            if (InputRouter.CmdShowContextMenu)
            {
                targetGroup.gameObject.SetActive(true);
            
                var mouseScreenPos = Input.mousePosition;
                mouseScreenPos.z = referenceCamera.nearClipPlane;
                var mouseWorldPos = referenceCamera.ScreenToWorldPoint(mouseScreenPos);

                targetGroup.transform.position = mouseScreenPos;
            }
        }

        private void Toggle()
        {
            targetGroup.gameObject.SetActive(!targetGroup.gameObject.activeSelf);
        }
    }
}