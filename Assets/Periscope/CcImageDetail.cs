using System;
using System.Text;
using Newtonsoft.Json;

namespace Periscope
{
    [Serializable]
    public class CcTag
    {
        [JsonProperty] public string name;
    }

    [Serializable]
    public class CcImageDetail
    {
        public string title;
        public string id;
        public string creator;
        public string creator_url;
        public CcTag[] tags;
        public string url;
        public string thumbnail;
        public string source;
        public string license;
        public string license_version;
        public string license_url;
        public string foreign_landing_url;
        public string detail_url;
        public string related_url;
        public int height;
        public int width;
        public string attribution;

        public override string ToString()
        {
            var str = new StringBuilder()
                .Append($"<b>Attribution:</b> {attribution}\n")
                .Append($"<b>Creator url:</b> {creator_url}")
                ;

            return str.ToString();
        }
    }
}