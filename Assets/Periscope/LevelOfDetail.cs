using UnityEngine;
using UnityEngine.Serialization;

namespace Periscope
{
    public static class RendererExtensions
    {
        /// <summary>
        /// Counts the bounding box corners of the given RectTransform that are visible from the given Camera in screen space.
        /// </summary>
        /// <returns>The amount of bounding box corners that are visible from the Camera.</returns>
        /// <param name="rectTransform">Rect transform.</param>
        /// <param name="camera">Camera.</param>
        private static int CountCornersVisibleFrom(this RectTransform rectTransform, Camera camera)
        {
            Rect screenBounds =
                new Rect(0f, 0f, Screen.width,
                    Screen.height); // Screen space bounds (assumes camera renders across the entire screen)
            Vector3[] objectCorners = new Vector3[4];
            rectTransform.GetWorldCorners(objectCorners);

            int visibleCorners = 0;
            Vector3 tempScreenSpaceCorner; // Cached
            for (var i = 0; i < objectCorners.Length; i++) // For each corner in rectTransform
            {
                tempScreenSpaceCorner =
                    camera.WorldToScreenPoint(
                        objectCorners[i]); // Transform world space position of corner to screen space
                if (screenBounds.Contains(tempScreenSpaceCorner)) // If the corner is inside the screen
                {
                    visibleCorners++;
                }
            }

            return visibleCorners;
        }

        /// <summary>
        /// Determines if this RectTransform is fully visible from the specified camera.
        /// Works by checking if each bounding box corner of this RectTransform is inside the cameras screen space view frustrum.
        /// </summary>
        /// <returns><c>true</c> if is fully visible from the specified camera; otherwise, <c>false</c>.</returns>
        /// <param name="rectTransform">Rect transform.</param>
        /// <param name="camera">Camera.</param>
        public static bool IsFullyVisibleFrom(this RectTransform rectTransform, Camera camera)
        {
            return CountCornersVisibleFrom(rectTransform, camera) == 4; // True if all 4 corners are visible
        }

        /// <summary>
        /// Determines if this RectTransform is at least partially visible from the specified camera.
        /// Works by checking if any bounding box corner of this RectTransform is inside the cameras screen space view frustrum.
        /// </summary>
        /// <returns><c>true</c> if is at least partially visible from the specified camera; otherwise, <c>false</c>.</returns>
        /// <param name="rectTransform">Rect transform.</param>
        /// <param name="camera">Camera.</param>
        public static bool IsVisibleFrom(this RectTransform rectTransform, Camera camera)
        {
            return CountCornersVisibleFrom(rectTransform, camera) > 0; // True if any corners are visible
        }
    }

    [RequireComponent(typeof(CanvasGroup))]
    public class LevelOfDetail : MonoBehaviour
    {
        private bool _isOn = true; 
        private CanvasGroup _group = null;
        private RectTransform _rect = null;
         [SerializeField] private float min = 0.25f;
         [SerializeField] private float max = 1.0f;
         [SerializeField] private float hiddenAlpha = 0.1f;

        private void Awake()
        {
            _group = GetComponent<CanvasGroup>();
            _rect = GetComponent<RectTransform>();

            Debug.Assert(_group != null, "Required component");
            Debug.Assert(_rect != null, "Required component");
        }

        // TODO: Unhook this from the update loop?
        private void Update()
        {
            var t = Camera.main.orthographicSize;
            if (!_isOn && t >= min && t <= max)
            {
                var viewportPos = Camera.main.WorldToViewportPoint(transform.position);
                if (viewportPos.x > 0 && viewportPos.x < 1f && viewportPos.y > 0 && viewportPos.y < 1f)
                {
                    _group.alpha = 1f;
                    _group.blocksRaycasts = true;
                    _group.interactable = true;
                    _isOn = true;
                }
            }
            else if (_isOn && t < min && t > max)
            {
                _group.alpha = hiddenAlpha;
                _group.blocksRaycasts = false;
                _group.interactable = false;
                _isOn = false;
            }
        }
    }
}
