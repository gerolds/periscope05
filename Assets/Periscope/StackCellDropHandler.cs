﻿using System;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

namespace Periscope
{
    public class StackCellDropHandler : MonoBehaviour, IDropHandler, IPointerEnterHandler, ICellDrop
    {
        public ContainerId id = ContainerId.Invalid;
        public Transform dropTarget = null;
        private RectTransform _rect = null;

        public void OnDrop(PointerEventData eventData)
        {
            if (eventData.pointerDrag == null) 
                return;
            
            var cell = eventData.pointerDrag.GetComponentInChildren(typeof(Cell)) as Cell;
            Drop(cell, Vector2.zero);
        }

        private void Start()
        {
            _rect = GetComponent<RectTransform>();
        }

        public void Drop(Cell cell, Vector2 targetPos)
        {
            //Debug.Log("An object was dropped into stack: " + obj);
            if (cell == null)
                return;
            cell.Container = id;

            // animate depending on its current canvas render mode
            var objCanvas = cell.GetComponentInParent<Canvas>().rootCanvas;
            Debug.Assert(objCanvas != null);
            var trans = cell.GetComponent<RectTransform>();
            Debug.Assert(trans != null);

            Vector3 animStartPos;
            switch (objCanvas.renderMode)
            {
                case RenderMode.ScreenSpaceOverlay:
                    animStartPos = cell.transform.position;
                    break;
                case RenderMode.ScreenSpaceCamera:
                    throw new NotImplementedException();
                case RenderMode.WorldSpace:
                    animStartPos = Camera.main.WorldToScreenPoint(cell.transform.position);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            trans.SetParent(dropTarget, false);
            trans.localScale = Vector3.one;
            trans.sizeDelta = Vector2.one * 64;
            trans.SetAsLastSibling();
            trans.position = new Vector3(animStartPos.x, animStartPos.y, 0);
            //Debug.Log($"World pos: {obj.transform.position}, Screen pos: {trans.position}");

            trans.DOSizeDelta(_rect.sizeDelta, 0.3f);
            trans.DOLocalMove(targetPos, 0.25f);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (eventData.pointerDrag != null)
            {
                //eventData.pointerDrag.transform.SetParent(transform.parent, false);
            }
        }
    }
}