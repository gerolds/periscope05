using System;
using UnityEngine;
using UnityEngine.UI;

namespace Periscope
{
    [Obsolete]
    public class ContrastWithBackground : MonoBehaviour
    {
        [SerializeField] private Image background;
        [SerializeField] private Graphic target;
        [SerializeField] private byte colorSampleCount;
        [SerializeField] private Cell cell;
        [SerializeField] private Color color;

        private void Start()
        {
            cell.changed.AddListener(_ => UpdateContrast());
        }

        private void OnEnable()
        {
            UpdateContrast();
        }

        private void UpdateContrast()
        {
            color = GetAvgBgColor().grayscale > 0.5f ? Color.black : Color.white;
            target.color = color;
        }
        
        public Color GetAvgBgColor()
        {
            if (background.sprite == null)
            {
                //Debug.Log($"No Bg Sprite");
                return new Color(1f, 1f, 1f);
            }

            var xStep = background.sprite.texture.width / colorSampleCount + 1;
            var yStep = background.sprite.texture.height / colorSampleCount + 1;
            Vector3 sum = new Vector3Int(0x00, 0x00, 0x00);
            Vector3 min = new Vector3Int(0x00, 0x00, 0x00);
            Vector3 max = new Vector3Int(0x00, 0x00, 0x00);
            for (int i = 0; i < (colorSampleCount * colorSampleCount); i++)
            {
                var color = background.sprite.texture.GetPixel(xStep + (i % colorSampleCount) * xStep,
                    yStep + (i / colorSampleCount) * yStep);
                var vec = new Vector3(color.r, color.g, color.b);
                sum += vec;
                if (vec.sqrMagnitude < min.sqrMagnitude)
                    min = vec;
                if (vec.sqrMagnitude > max.sqrMagnitude)
                    max = vec;
            }

            var avg = sum / (colorSampleCount * colorSampleCount);
            return new Color(avg.x, avg.y, avg.z, (max - min).magnitude);
        }
    }
}