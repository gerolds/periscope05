﻿using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Periscope
{
    public class CellDivideTrigger : MonoBehaviour
    {
        [FormerlySerializedAs("splitButton")] [SerializeField]
        private Button divideButton = null;

        [SerializeField] private Button duplicateButton = null;

        [FormerlySerializedAs("selectHandler")] [SerializeField]
        private CellSelectHandler cellSelectHandler = null;

        [FormerlySerializedAs("cellData")] [SerializeField]
        private Cell cell = null;


        private void Awake()
        {
            Debug.Assert(cell != null, $"Trigger requires a {nameof(Cell)} reference.");
            Debug.Assert(divideButton != null, $"Trigger requires a {nameof(Button)} reference.");
            Debug.Assert(duplicateButton != null, $"Trigger requires a {nameof(Button)} reference.");
            Debug.Assert(cellSelectHandler != null, $"Trigger requires a {nameof(CellSelectHandler)} reference.");
        }

        private void Start()
        {
            divideButton.onClick.AddListener(CreateEmptyCell);
            duplicateButton.onClick.AddListener(CreateDuplicateCell);
        }

        private void CreateEmptyCell()
        {
            Debug.Log("Divide cell");
            var newCell =
                Periscope.Singleton.CreateEmptyCell(cell.Container, cell.transform.position + Vector3.right);
            var selectHandler = newCell.GetComponent(typeof(CellSelectHandler)) as CellSelectHandler;
            if (selectHandler != null)
            {
                selectHandler.Select();
                CellSelectHandler.ClearAndSetMarked(selectHandler);
            }
        }

        private void CreateDuplicateCell()
        {
            Debug.Log("Duplicate cell");
            var newCell = Periscope.Singleton.CreateCopyOfCell(cell, cell.Container,
                cell.transform.position + Vector3.right);
            var selectHandler = newCell.GetComponent(typeof(CellSelectHandler)) as CellSelectHandler;
            if (selectHandler != null)
            {
                selectHandler.Select();
                CellSelectHandler.ClearAndSetMarked(selectHandler);
            }
        }

        private void Update()
        {
            // TODO: Unhook this from the update loop
            if (InputRouter.MomentaryAlt)
            {
                divideButton.gameObject.SetActive(false);
                duplicateButton.gameObject.SetActive(true);
            }
            else
            {
                divideButton.gameObject.SetActive(true);
                duplicateButton.gameObject.SetActive(false);
            }

            if (cellSelectHandler.IsSelected && InputRouter.CmdDivideCell && divideButton.interactable)
                CreateEmptyCell();
            if (cellSelectHandler.IsSelected && InputRouter.CmdDuplicateCell && duplicateButton.interactable)
                CreateDuplicateCell();
            //if (!splitButton.interactable)
            //    Periscope.Singleton.Message.Log("Card division limit reached (10).", MessagePresenter.MessageType.Normal);
        }
    }
}