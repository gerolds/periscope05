using Accord.MachineLearning;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Periscope
{
    public class StackShuffle : MonoBehaviour
    {
         public Transform target = null;
         public Toggle toggle = null;

        private void Update()
        {
            // TODO: Unhook this from the update loop?
            if (InputRouter.CmdRandomise && toggle.isOn)
            {
                if (target != null)
                    Shuffle(target);
            }
        }

        public void Shuffle(Transform parent)
        {
            Debug.Log("Shuffle");
            var childCount = parent.childCount;
            for (int i = 0; i < parent.childCount; i++)
            {
                parent.GetChild(i).SetSiblingIndex(Random.Range(0, childCount));
            }
            Periscope.Singleton.message.Log("Stack shuffled", MessagePresenter.MessageType.Normal);
        }
    }
}