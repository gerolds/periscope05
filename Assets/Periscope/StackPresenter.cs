﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Periscope
{
    public class StackPresenter : MonoBehaviour
    {
        //[SerializeField] private TextMeshProUGUI Label = null;
         [SerializeField] private TextMeshProUGUI counter = null;
         [SerializeField] private Transform itemContainer = null;
         [SerializeField] private Toggle toggle = null;

        //private int _initialChildren;

        // Start is called before the first frame update
        void Start()
        {
            //_initialChildren = transform.childCount;
            toggle.onValueChanged.AddListener(Scalorama);
        }

        private void Scalorama(bool isOn)
        {
            if (isOn)
                ScaleUp();
            else
                ScaleDown();
        }

        // Update is called once per frame
        // TODO: Unhook this from the update loop?
        void Update()
        {
            if (Time.frameCount % 5 == 0)
            {
                counter.text = $"{itemContainer.childCount}";
                
                // deactivate items in the middle of the stack to save draw calls
                for (int i = 0; i < itemContainer.childCount; i++)
                {
                    if (i > 4 && i < itemContainer.childCount - 4)
                        itemContainer.GetChild(i).gameObject.SetActive(false);
                    else
                        itemContainer.GetChild(i).gameObject.SetActive(true);
                }
            }
        }

        public void ScaleUp()
        {
            var rect = GetComponent<RectTransform>();
            rect.DOComplete(true);
            rect.DOSizeDelta(new Vector2(256, 256), 0.25f);

            void ScaleOtherSiblings()
            {
                for (int i = 0; i < itemContainer.childCount - 1; i++)
                {
                    var r = itemContainer.GetChild(i).GetComponent<RectTransform>();
                    r.DOComplete(true);
                    r.sizeDelta = new Vector2(256, 256);
                }
            }

            if (itemContainer.childCount > 0)
                itemContainer
                    .GetChild(itemContainer.childCount - 1)
                    .GetComponent<RectTransform>()
                    .DOSizeDelta(new Vector2(256, 256), 0.25f)
                    .OnComplete(ScaleOtherSiblings);
        }

        public void ScaleDown()
        {
            var rect = GetComponent<RectTransform>();
            rect.DOComplete(true);
            rect.DOSizeDelta(new Vector2(128, 128), 0.25f);

            void ScaleOtherSiblings()
            {
                for (int i = 0; i < itemContainer.childCount - 1; i++)
                {
                    var r = itemContainer.GetChild(i).GetComponent<RectTransform>();
                    r.DOComplete(true);
                    r.sizeDelta = new Vector2(128, 128);
                }
            }

            if (itemContainer.childCount > 0)
            {
                ScaleOtherSiblings();
                itemContainer
                    .GetChild(itemContainer.childCount - 1)
                    .GetComponent<RectTransform>()
                    .DOSizeDelta(new Vector2(128, 128), 0.25f);
            }
        }
    }
}