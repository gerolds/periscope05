using System;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

namespace Periscope
{
    [Serializable]
    public class Session
    {
        public string Format = "SCOPE"; // New in 0.5.8
        public string Product = "";
        public string Version = "";
        public string Build = "";
        public string Company = "";
        public string Identifier = ""; // application identifier
        public string DataPath = ""; // application install location
        public string PersistentDataPath = "";
        public string Timestamp = "";
        public string CollectionPath = "";
        public string Checksum = "";
        public List<int> Container = new List<int>();
        
        // A local attachment file.
        // Deprecated in 0.5.8
        public List<string> Path = new List<string>();
        
        // A local attachment file.
        // New in 0.5.8
        public List<string> LocalAttachmentUri = new List<string>();
        
        // The URI from which the attachment was originally imported.
        // New in 0.5.8
        public List<string> SourceAttachmentUri = new List<string>(); 
        
        // Card Text
        public List<string> Text = new List<string>();
        
        // Card Backside Text
        public List<string> Reverse = new List<string>();
        
        // Card X position
        public List<float> X = new List<float>();
        
        // Card Y position
        public List<float> Y = new List<float>();

        // Card Tags
        public List<string> Tags = new List<string>();
        
        // Card License Info
        public List<string> LicenseInfo = new List<string>();
        
        // unused fields
        public List<int> Rating = new List<int>();
        public List<bool> Favorite = new List<bool>();
        public List<bool> Flag = new List<bool>();
        public List<bool> Checked = new List<bool>();
        public List<bool> Reversed = new List<bool>();

        private Session()
        {
        }

        public static Session CreateEmpty()
        {
            var session = new Session
            {
                Product = Application.productName,
                Version = Application.version,
                Build = Periscope.Build,
                Company = Application.companyName,
                Identifier = Application.identifier,
                DataPath = Application.dataPath,
                PersistentDataPath = Application.persistentDataPath,
                Timestamp = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture)
            };
            return session;
        }
    }
}