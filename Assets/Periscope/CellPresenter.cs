﻿using System;
using DG.Tweening;
using ICSharpCode.SharpZipLib.Zip;
using JetBrains.Annotations;
using Newtonsoft.Json;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Periscope
{
    [RequireComponent(typeof(Cell))]
    [RequireComponent(typeof(CanvasGroup))]
    public class CellPresenter : MonoBehaviour
    {
         [SerializeField] private Cell cellData = null;
         [SerializeField] private TextMeshProUGUI cellText = null;
         [SerializeField] private Image cellGraphic = null;
         [SerializeField] private Image cellTextBg = null;
         [SerializeField] private CellLicenseTrigger cellLicense = null;
         [SerializeField] private Color shadeDisabledColor = new Color(0, 0, 0, 0.0f);
         [SerializeField] private float rejectedAlpha = 0.2f;
         [SerializeField] private int colorSampleCount = 5;

        public CanvasGroup Group => GetComponent<CanvasGroup>();
        
        private void Awake()
        {
            Debug.Assert(cellData != null, $"{nameof(cellData)} is assignable via editor");
            cellData.changed.AddListener(Refresh);
        }

        public void Reverse()
        {
            void ToggleReversedState()
            {
                cellData.Reversed = !cellData.Reversed;
                cellGraphic.color = cellData.Reversed ? Color.gray : Color.white;
            }

            void MirrorGraphic()
            {
                var graphRect = cellGraphic.GetComponent<RectTransform>();
                graphRect.localRotation
                    = Quaternion.Euler(graphRect.localRotation.eulerAngles + new Vector3(0, 180f, 0));
            }

            Debug.Log("Reverse");
            var rect = GetComponent<RectTransform>();
            rect.DOComplete(true);
            var sq = DOTween.Sequence();
            sq.Append(rect.DOScale(new Vector3(0, 1f), 0.25f));
            sq.AppendCallback(ToggleReversedState);
            sq.AppendCallback(MirrorGraphic);
            sq.Append(rect.DOScale(new Vector3(1f, 1f), 0.25f));
            sq.Play();
        }

        private void Refresh(Cell data)
        {
            //Debug.Log($"Cell {name} refreshed.");
            cellText.text = data.Text.Substring(
                0,
                Mathf.Clamp(250, 0, data.Text.Length));
            cellText.text = data.Reversed ? data.Reverse : data.Text;
            cellGraphic.sprite = data.GraphicSprite;

            if (cellText.text.Length > 0)
            {
                var bg = GetAvgBgColor();
                cellTextBg.color = new Color(bg.r, bg.g, bg.b, 0.5f);
                cellText.color = bg.grayscale > 0.5f ? Color.black : Color.white;
            }
            else
            {
                cellTextBg.color = shadeDisabledColor;
            }

            if (this.cellData.Rating < 0)
                Group.alpha = rejectedAlpha;
            else
                Group.alpha = 1.0f;
        }

        #region Contrast Text with Background

        public Color GetAvgBgColor()
        {
            if (cellGraphic.sprite == null)
            {
                //Debug.Log($"No Bg Sprite");
                return new Color(1f, 1f, 1f);
            }

            return GetAvgColor(cellGraphic.sprite.texture, colorSampleCount);
        }
        
        public static Color GetAvgColor([NotNull] Texture2D tex, int samples = 5)
        {
            var xStep = tex.width / samples + 1;
            var yStep = tex.height / samples + 1;
            Vector3 sum = new Vector3Int(0x00, 0x00, 0x00);
            Vector3 min = new Vector3Int(0x00, 0x00, 0x00);
            Vector3 max = new Vector3Int(0x00, 0x00, 0x00);
            for (int i = 0; i < (samples * samples); i++)
            {
                var color = tex.GetPixel(xStep + (i % samples) * xStep,
                    yStep + (i / samples) * yStep);
                var vec = new Vector3(color.r, color.g, color.b);
                sum += vec;
                if (vec.sqrMagnitude < min.sqrMagnitude)
                    min = vec;
                if (vec.sqrMagnitude > max.sqrMagnitude)
                    max = vec;
            }

            var avg = sum / (samples * samples);
            return new Color(avg.x, avg.y, avg.z, (max - min).magnitude);
        }

        #endregion
    }
    
    
}