﻿using JetBrains.Annotations;
using QTree;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Periscope
{
    public class QTreeCellDropHandler : MonoBehaviour, IPointerEnterHandler, ICellDrop
    {
        public ContainerId id = ContainerId.Invalid;
        [SerializeField] private QuadTreeLayout layout = null;

        public const int MinSquareToRecvDrop = 2 * 2 + 2 * 2;

        public bool CanReceiveDrop(Vector2 pos)
        {
            var can = layout.Contains(pos)
                      && (layout.GetPosCellBoundarySize(pos).sqrMagnitude > MinSquareToRecvDrop ||
                          (layout.GetPosCellBoundarySize(pos).sqrMagnitude == MinSquareToRecvDrop &&
                           layout.GetItemsAtPos(pos).Count == 0)
                      );
            return can;
        }

        //public void OnDrop(PointerEventData eventData) =>
        //    Drop(eventData.pointerDrag.GetComponentInChildren(typeof(Cell)) as Cell);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="targetPos">pos after drop in world space</param>
        public void Drop(Cell cell, Vector2 targetPos)
        {
            if (cell == null)
                return;

            // if the cell isn't positioned inside the tree, skip the drop
            // if the position change would make the cell too small, skip the update
            if (!CanReceiveDrop(targetPos))
            {
                Debug.Log($"{name} can't receive drop of {cell.name} at {targetPos}.");
                return;
            }

            //Debug.Log($"{cell.name} dropped into {name}.");
            cell.Container = id;
            var t = cell.transform;
            t.SetParent(transform);
            t.localScale = Vector3.one;
            t.position = targetPos;
            LayoutRebuilder.MarkLayoutForRebuild((RectTransform) t);
            transform.SetAsLastSibling();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!eventData.dragging)
                return;

            var cell = eventData.pointerDrag.GetComponentInChildren(typeof(Cell)) as Cell;
            Drop(cell, eventData.pointerCurrentRaycast.worldPosition);
        }
    }
}