﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Periscope
{
    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(RawImage))]
    public class DrawingCanvas :
        MonoBehaviour,
        IPointerEnterHandler,
        IPointerExitHandler
    {
        private bool _isDrawing = false;
        private bool _isOnCanvas = false;
        private RectTransform _rectTransform = null;
        private Color[] _penBlock;
        private Color[] _eraserBlock;
        private const byte EraserScale = 5;
        private const byte BlockSize = 4;

        private const int Resolution = 512;

        //private int frameCount = 0;
        [SerializeField] private RawImage target;
        [HideInInspector] public Texture2D texture;
        private Vector2Int _prevPos;

        // Start is called before the first frame update
        void Start()
        {
            _rectTransform = GetComponent<RectTransform>();

            texture = new Texture2D(Resolution, Resolution, TextureFormat.RGBA32, false);
            var rawTexData = texture.GetRawTextureData<Color32>();
            for (int i = 0; i < Resolution * Resolution; i++)
            {
                rawTexData[i] = Color.clear;
            }

            texture.Apply();
            target.texture = texture;
            _penBlock = new Color[BlockSize * BlockSize];
            for (int i = 0; i < BlockSize * BlockSize; i++)
            {
                _penBlock[i] = Color.black;
            }

            _eraserBlock = new Color[(BlockSize * EraserScale) * (BlockSize * EraserScale)];
            for (int i = 0; i < (BlockSize * EraserScale) * (BlockSize * EraserScale); i++)
            {
                _eraserBlock[i] = Color.clear;
            }
        }

        public void Line(Vector2Int start, Vector2Int end, Vector2Int blockSize, Color[] block,
            Func<Color, Color, Color> mode)
        {
            Line(start.x, start.y, end.x, end.y, blockSize.x, blockSize.y, block, mode);
        }

        public void Line(int startX, int startY, int endX, int endY, int blockWidth, int blockHeight, Color[] block,
            Func<Color, Color, Color> mode)
        {
            // Bresenham's line algorithm
            // https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
            // Based on https://stackoverflow.com/questions/11678693
            
            int w = endX - startX;
            int h = endY - startY;
            int dx1 = 0;
            int dy1 = 0;
            int dx2 = 0;
            int dy2 = 0;
            if (w < 0)
                dx1 = -1;
            else if (w > 0)
                dx1 = 1;
            if (h < 0)
                dy1 = -1;
            else if (h > 0)
                dy1 = 1;
            if (w < 0)
                dx2 = -1;
            else if (w > 0)
                dx2 = 1;
            int longest = Mathf.Abs(w);
            int shortest = Mathf.Abs(h);
            if (!(longest > shortest))
            {
                longest = Mathf.Abs(h);
                shortest = Mathf.Abs(w);
                if (h < 0) dy2 = -1;
                else if (h > 0) dy2 = 1;
                dx2 = 0;
            }

            int numerator = longest >> 1;
            for (int i = 0; i <= longest; i++)
            {
                texture.SetPixels(startX - blockWidth / 2, startY - blockHeight / 2, blockWidth, blockHeight, block);
                numerator += shortest;
                if (!(numerator < longest))
                {
                    numerator -= longest;
                    startX += dx1;
                    startY += dy1;
                }
                else
                {
                    startX += dx2;
                    startY += dy2;
                }
            }
        }

        void Update()
        {
            // TODO: Unhook this from the update loop?
            if (InputRouter.Mode != InputMode.Drawing)
                return;

            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                _isDrawing = _isOnCanvas;
                if (_isDrawing)
                {
                    _prevPos = MousePxPos();
                    Debug.Log($"Drawing");
                }
            }

            if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                _isDrawing = false;
                Debug.Log($"Stop Drawing");
            }

            if (_isDrawing &&
                RectTransformUtility.RectangleContainsScreenPoint(_rectTransform, Input.mousePosition, Camera.main))
            {
                var pxPos = MousePxPos();
                if (InputRouter.MomentaryAlt)
                {
                    Line(pxPos,
                        _prevPos,
                        new Vector2Int(BlockSize * EraserScale, BlockSize * EraserScale),
                        _eraserBlock,
                        (a, b) => b);
                }
                else
                {
                    Line(pxPos,
                        _prevPos,
                        new Vector2Int(BlockSize, BlockSize),
                        _penBlock,
                        (a, b) => b);
                }

                //Debug.Log($"LineTo x{pxPos.x} y{pxPos.y}");
                _prevPos = pxPos;
                texture.Apply();
                target.texture = texture;
                //string.Join("\n", _texture.GetPixels());
            }
        }

        private Vector2Int MousePxPos()
        {
            var rect = _rectTransform.rect;
            Vector2 localToScreenFactor = new Vector2(
                texture.width / rect.width,
                texture.height / rect.height
            );
            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                _rectTransform,
                Input.mousePosition,
                Camera.main,
                out var localMouse
            );
            var pxPos = new Vector2Int(
                (int) (localMouse.x * localToScreenFactor.x) + Resolution / 2,
                (int) (localMouse.y * localToScreenFactor.y) + Resolution / 2
            );
            return pxPos;
        }

        private void FixedUpdate()
        {
            /*  frameCount++;
          if (frameCount % 3 == 0)
              _texture.Apply();*/
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _isOnCanvas = true;
            //Debug.Log($"Enter Drawing Canvas");
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _isOnCanvas = false;
            //Debug.Log($"Exit Drawing Canvas");
        }
    }
}