﻿using System;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Periscope
{
    public class CellAttachmentTrigger : MonoBehaviour
    {
        [SerializeField] private Button targetButton = null;

        [FormerlySerializedAs("cellData")] [SerializeField]
        private Cell cell = null;

        [FormerlySerializedAs("selectHandler")] [SerializeField]
        private CellSelectHandler cellSelectHandler = null;

        [SerializeField] private Image targetGraphic = null;
        [SerializeField] private Sprite addPicture = null;
        [SerializeField] private Sprite removePicture = null;

        private void Awake()
        {
            Debug.Assert(cell != null, $"Trigger requires a {nameof(Cell)} reference.");
            Debug.Assert(targetButton != null, $"Trigger requires a {nameof(Button)} reference.");
            Debug.Assert(cellSelectHandler != null, $"Trigger requires a {nameof(CellSelectHandler)} reference.");
            Debug.Assert(targetGraphic != null, $"Trigger requires a {nameof(Image)} reference.");
            Debug.Assert(addPicture != null, $"Trigger requires a {nameof(Sprite)} reference.");
            Debug.Assert(removePicture != null, $"Trigger requires a {nameof(Sprite)} reference.");
        }

        private void Start()
        {
            if (cell == null)
                return;

            targetButton.onClick.AddListener(ToggleAttachment);

            Debug.Assert(cell != null);
            Debug.Assert(cellSelectHandler != null);
        }

        private void ToggleAttachment()
        {
            if (cell.Equals(null))
                return;

            if (string.IsNullOrEmpty(cell.LocalAttachmentUri))
            {
                var select = Periscope.Singleton.SelectSingleImageOnDisk();
                var uri = new Uri(select, UriKind.Absolute);
                cell.AddAttachment(uri);
                targetGraphic.sprite = removePicture;
            }
            else
            {
                targetGraphic.sprite = addPicture;
                cell.RemoveAttachment();
            }
        }

      public void UpdateView()
        {
            if (string.IsNullOrEmpty(cell.LocalAttachmentUri))
            {
                targetGraphic.sprite = addPicture;
            }
            else
            {
                targetGraphic.sprite = removePicture;
            }
        }

        private void Update()
        {
            // TODO: Can this be unhooked from the update loop?
            if (cellSelectHandler.IsSelected && InputRouter.CmdPlacePicture && targetButton.interactable)
            {
                ToggleAttachment();
            }
            UpdateView();
        }
    }
}