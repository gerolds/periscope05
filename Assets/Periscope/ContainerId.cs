namespace Periscope
{
    public enum ContainerId
    {
        Canvas1,
        Canvas2,
        Canvas3,
        Canvas4,
        Canvas5,
        Canvas6,
        Stack1,
        Stack2,
        Stack3,
        Stack4,
        Stack5,
        Trash,
        Invalid
    }
}