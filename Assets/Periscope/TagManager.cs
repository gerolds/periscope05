﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Serialization;

namespace Periscope
{
    public class TagManager : MonoBehaviour
    {
         [SerializeField] private TagButton tagWidgetPrefab = null;
        private readonly Queue<GameObject> _dependents = new Queue<GameObject>();

        public void RebuildTags([NotNull] IEnumerable<string> tags)
        {
            var tagNames = tags.ToList();
            
            // remove previous tags if any
            while (_dependents.Count > 0)
                Destroy(_dependents.Dequeue());

            // add new tags
            foreach (var tagName in tagNames)
            {
                var widget = Instantiate(tagWidgetPrefab);
                widget.label.text = tagName;
                widget.delete.interactable = true;
                widget.highlight.interactable = true;
                widget.transform.SetParent(transform);
                widget.transform.localPosition = Vector3.zero;
                widget.transform.localScale = Vector3.one;
                widget.delete.onClick.AddListener(() => DeleteTagHandler(tagName));
                widget.highlight.onClick.AddListener(() => HighlightTagHandler(tagName));
                _dependents.Enqueue(widget.gameObject);
            }
        }

        private void HighlightTagHandler([NotNull] string tag)
        {
            Debug.Log($"{nameof(TagManager)}: HighlightTagHandler <{tag}>");
            if (tag.Length == 0)
                return;
            //Periscope.Singleton.Cells.ForEach(entry => entry.ClearHighlight());
            //var cellsWithTag = Grid.AllCells.Where(entry => entry.HasTag(tag));
            //cellsWithTag.ToList().ForEach(cell => cell.Highlight());
            //Debug.Log($"{nameof(TagGroup)}: {cellsWithTag.Count()} cellsWithTag found");
        }

        private void DeleteTagHandler([NotNull] string tag)
        {
            Debug.Log($"{nameof(TagManager)}: DeleteTagHandler <{tag}>");
            if (tag.Length == 0)
                return;

            CellSelectHandler.MarkedCells
                .ForEach(cell => cell.cellData.RemoveTag(tag));
        }
    }
}