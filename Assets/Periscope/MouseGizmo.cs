﻿using UnityEngine;

namespace Periscope
{
    public class MouseGizmo : MonoBehaviour
    {
        void FixedUpdate()
        {
            var mouseWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(mouseWorld.x, mouseWorld.y, 0);
        }
    }
}
