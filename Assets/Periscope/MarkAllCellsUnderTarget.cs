﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

namespace Periscope
{
    public class MarkAllCellsUnderTarget : MonoBehaviour, IPointerClickHandler
    {
         [SerializeField] private GameObject target = null;

        public void OnPointerClick(PointerEventData eventData)
        {
            var cellsUnderTarget = target.GetComponentsInChildren<CellSelectHandler>();
            foreach (var cell in cellsUnderTarget)
            {
                CellSelectHandler.ClearMarked();
                CellSelectHandler.AddMarked(cell);
            }
        }
    }
}
