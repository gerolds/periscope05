﻿using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Periscope
{
    public class CellDeleteTrigger : MonoBehaviour
    {
        [FormerlySerializedAs("cellData")] [SerializeField] private Cell cell = null;
        [SerializeField] private Button targetButton = null;
    
        private void Awake()
        {
            Debug.Assert(cell != null, $"Trigger requires a {nameof(Cell)} reference.");
            Debug.Assert(targetButton != null, $"Trigger requires a {nameof(Button)} reference.");
        }

        private void Start()
        {
            if (cell == null)
                return;

            targetButton.onClick.AddListener(Delete);
        }

        private void Delete()
        {
            cell.Delete();
        }
    }
}
