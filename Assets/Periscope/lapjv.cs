namespace Periscope
{
    /// <summary>
    /// Jonker-Volgenant algorithm for LAP. Based on the cpp implementation by R. Jonker, 1996
    /// </summary>
    public static class LAPJV
    {
        // Original Source Notes:
        /************************************************************************
    *
    *  lap.cpp
       version 1.0 - 4 September 1996
       author: Roy Jonker @ MagicLogic Optimization Inc.
       e-mail: roy_jonker@magiclogic.com
    
       Code for Linear Assignment Problem, according to
    
       "A Shortest Augmenting Path Algorithm for Dense and Sparse Linear
        Assignment Problems," Computing 38, 325-340, 1987
    
       by
    
       R. Jonker and A. Volgenant, University of Amsterdam.
    
    *
       CHANGED 2016-05-13 by Yong Yang(yongyanglink@gmail.com) in column reduction part according to
       matlab version of LAPJV algorithm(Copyright (c) 2010, Yi Cao All rights reserved)--
       https://www.mathworks.com/matlabcentral/fileexchange/26836-lapjv-jonker-volgenant-algorithm-for-linear-assignment-problem-v3-0:
    *
    *************************************************************************/

        private const int Big = 100000;

        /// <summary>
        /// This function is the jv shortest augmenting path algorithm to solve the assignment problem
        /// </summary>
        /// <param name="dim">problem size</param>
        /// <param name="costMatrix">cost matrix</param>
        /// <param name="rowSolution">column assigned to row in solution</param>
        /// <param name="colSolution">row assigned to column in solution</param>
        /// <param name="u">dual variables, row reduction numbers</param>
        /// <param name="v">dual variables, column reduction numbers</param>
        /// <returns></returns>
        public static double Solve(int dim, double[][] costMatrix, out int[] rowSolution, out int[] colSolution, double[] u, double[] v)
        {
            int i = 0;
            int numFree = 0;
            int f = 0;
            int k = 0;
            int[] predecessor = new int[dim]; // row-predecessor of column in augmenting/alternating path.
            int[] free = new int[dim]; // list of unassigned rows.
            int j = 0;
            int j1 = 0;
            int j2 = 0;
            int endOfPath = 0;
            int last = 0;
            var columns = new int[dim]; // list of columns to be scanned in various ways.
            var matches = new int[dim]; // counts how many times a row could be assigned.
            double min = 0;
            double h = 0;
            var d = new double[dim]; // 'cost-distance' in augmenting path calculation.
            rowSolution = new int[dim];
            colSolution = new int[dim];
        
            // init how many times a row will be assigned in the column reduction.
            for (i = 0;
                i < dim;
                i++)
                matches[i] = 0;

            // COLUMN REDUCTION
            for (j = dim; j > 0; j--) // reverse order gives better results.
            {
                // find minimum cost over rows.
                min = costMatrix[0][j];
                var iMin = 0;
                for (i = 1; i < dim; i++)
                    if (costMatrix[i][j] < min)
                    {
                        min = costMatrix[i][j];
                        iMin = i;
                    }

                v[j] = min;
                if (++matches[iMin] == 1)
                {
                    // init assignment if minimum row assigned for first time.
                    rowSolution[iMin] = j;
                    colSolution[j] = iMin;
                }
                else if (v[j] < v[rowSolution[iMin]])
                {
                    j1 = rowSolution[iMin];
                    rowSolution[iMin] = j;
                    colSolution[j] = iMin;
                    colSolution[j1] = -1;
                }
                else
                    colSolution[j] = -1; // row already assigned, column not assigned.
            }

            // REDUCTION TRANSFER
            for (i = 0; i < dim; i++)
                if (matches[i] == 0) // fill list of unassigned 'free' rows.
                    free[numFree++] = i;
                else if (matches[i] == 1) // transfer reduction from rows that are assigned once.
                {
                    j1 = rowSolution[i];
                    min = Big;
                    for (j = 0;
                        j < dim;
                        j++)
                        if (j != j1)
                            if (costMatrix[i][j] - v[j] < min)
                                min = costMatrix[i][j] - v[j];
                    v[j1] = v[j1] - min;
                }

            //   AUGMENTING ROW REDUCTION
            int loopcnt = 0; // do-loop to be done twice.
            do
            {
                loopcnt++;

                //     scan all free rows.
                //     in some cases, a free row may be replaced with another one to be scanned next.
                k = 0;
                var prvNumFree = numFree;
                numFree = 0; // start list of rows still free after augmenting row reduction.
                while (k < prvNumFree)
                {
                    i = free[k];
                    k++;

                    //       find minimum and second minimum reduced cost over columns.
                    var uMin = costMatrix[i][0] - v[0];
                    j1 = 0;
                    double uSubMin = Big;
                    for (j = 1; j < dim; j++)
                    {
                        h = costMatrix[i][j] - v[j];
                        if (h < uSubMin)
                            if (h >= uMin)
                            {
                                uSubMin = h;
                                j2 = j;
                            }
                            else
                            {
                                uSubMin = uMin;
                                uMin = h;
                                j2 = j1;
                                j1 = j;
                            }
                    }

                    var i0 = colSolution[j1];
                    if (uMin < uSubMin)
                        //         change the reduction of the minimum column to increase the minimum
                        //         reduced cost in the row to the subminimum.
                        v[j1] = v[j1] - (uSubMin - uMin);
                    else // minimum and subminimum equal.
                    if (i0 > -1) // minimum column j1 is assigned.
                    {
                        //           swap columns j1 and j2, as j2 may be unassigned.
                        j1 = j2;
                        i0 = colSolution[j2];
                    }

                    //       (re-)assign i to j1, possibly de-assigning an i0.
                    rowSolution[i] = j1;
                    colSolution[j1] = i;

                    if (i0 > -1) // minimum column j1 assigned earlier.
                        if (uMin < uSubMin)
                            //           put in current k, and go back to that k.
                            //           continue augmenting path i - j1 with i0.
                            free[--k] = i0;
                        else
                            //           no further augmenting reduction possible.
                            //           store i0 in list of free rows for next phase.
                            free[numFree++] = i0;
                }
            } while (loopcnt < 2); // repeat once.

            // AUGMENT SOLUTION for each free row.
            for (f = 0; f < numFree; f++)
            {
                var freeRow = free[f];

                // Dijkstra shortest path algorithm.
                // runs until unassigned column added to shortest path tree.
                for (j = dim; j > 0; j--)
                {
                    d[j] = costMatrix[freeRow][j] - v[j];
                    predecessor[j] = freeRow;
                    columns[j] = j; // init column list.
                }

                var low = 0;
                var up = 0;

                // columns in up..dim-1 are to be considered later to find new minimum,
                // at this stage the list simply contains all columns
                var unassignedFound = false;
                do
                {
                    if (up == low) // no more columns to be scanned for current minimum.
                    {
                        last = low - 1;

                        // scan columns for up..dim-1 to find all indices for which new minimum occurs.
                        // store these indices between low..up-1 (increasing up).
                        min = d[columns[up++]];
                        for (k = up; k < dim; k++)
                        {
                            j = columns[k];
                            h = d[j];
                            if (h <= min)
                            {
                                if (h < min) // new minimum.
                                {
                                    up = low; // restart list at index low.
                                    min = h;
                                }

                                // new index with same minimum, put on undex up, and extend list.
                                columns[k] = columns[up];
                                columns[up++] = j;
                            }
                        }

                        // check if any of the minimum columns happens to be unassigned.
                        // if so, we have an augmenting path right away.
                        for (k = low; k < up; k++)
                            if (colSolution[columns[k]] < 0)
                            {
                                endOfPath = columns[k];
                                unassignedFound = true;
                                break;
                            }
                    }

                    if (!unassignedFound)
                    {
                        // update 'distances' between freerow and all unscanned columns, via next scanned column.
                        j1 = columns[low];
                        low++;
                        i = colSolution[j1];
                        h = costMatrix[i][j1] - v[j1] - min;

                        for (k = up; k < dim; k++)
                        {
                            j = columns[k];
                            var v2 = costMatrix[i][j] - v[j] - h;
                            if (v2 < d[j])
                            {
                                predecessor[j] = i;
                                if (v2 == min) // new column found at same minimum value
                                    if (colSolution[j] < 0)
                                    {
                                        // if unassigned, shortest augmenting path is complete.
                                        endOfPath = j;
                                        unassignedFound = true;
                                        break;
                                    }
                                    // else add to list to be scanned right away.
                                    else
                                    {
                                        columns[k] = columns[up];
                                        columns[up++] = j;
                                    }

                                d[j] = v2;
                            }
                        }
                    }
                } while (!unassignedFound);

                // update column prices.
                for (k = last + 1; k > 0; k--)
                {
                    j1 = columns[k];
                    v[j1] = v[j1] + d[j1] - min;
                }

                // reset row and column assignments along the alternating path.
                do
                {
                    i = predecessor[endOfPath];
                    colSolution[endOfPath] = i;
                    j1 = endOfPath;
                    endOfPath = rowSolution[i];
                    rowSolution[i] = j1;
                } while (i != freeRow);
            }

            // calculate optimal cost.
            double lapcost = 0;
            //  for (i = 0; i < dim; i++)
            for (i = dim; i > 0; i--)
            {
                j = rowSolution[i];
                u[i] = costMatrix[i][j] - v[j];
                lapcost = lapcost + costMatrix[i][j];
            }

            return lapcost;
        }
    }
}