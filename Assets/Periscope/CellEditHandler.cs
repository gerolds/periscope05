﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Periscope;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class CellEditHandler : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private Cell data = null;
    [SerializeField] private CellSelectHandler selectHandler = null;
    [SerializeField] private TMP_InputField inputField = null;
    [SerializeField] private CellAttachmentTrigger attachmentTrigger = null;

    //[SerializeField] private CellPresenter presenter = null;
    //public Cell Data => data;
    //public CellSelectHandler Selectable => selectHandler;
    //public CellPresenter Presenter => presenter;

    void Awake()
    {
        Debug.Assert(data != null, $"{data} is assignable in editor.");
        Debug.Assert(selectHandler != null, $"{nameof(selectHandler)} is assignable in editor.");
        Debug.Assert(inputField != null, $"{nameof(inputField)} is assignable in editor.");
        //Debug.Assert(Presenter != null, $"{nameof(Presenter)} is assignable in editor.");

        inputField.interactable = false;
        inputField.gameObject.SetActive(false);

        void InputEndedHandler(string arg)
        {
            Debug.Log($"{Time.frameCount} Input Ended");
            UpdateDataSource(arg);
            CellDetail.Notify();
        }

        void InputSubmitHandler(string arg)
        {
            selectHandler.Select();
        }

        inputField.onEndEdit.AddListener(InputEndedHandler);
        inputField.onSubmit.AddListener(InputSubmitHandler);
    }

    private void Update()
    {
        // TODO: Can this be unhooked from the update loop?

        // activate input field
        if (InputRouter.CmdEditCell
            && selectHandler.IsSelected
            && !selectHandler.WasSelectedThisFrame
        )
        {
            selectHandler.Mark();
            ActivateInputFiled();
        }

        // copy
        if (InputRouter.CmdCopy)
        {
            CopyToClipboard();
        }

        // paste
        if (InputRouter.CmdPaste)
        {
            PasteIntoCell();
        }
    }

    private void ActivateInputFiled()
    {
        if (!selectHandler.IsSelected)
            return;

        selectHandler.Select();
        selectHandler.MarkExclusive();
        //Debug.Log($"{Time.frameCount} R E T U R N   ACTIVATING INPUT ");
        inputField.interactable = true;
        inputField.gameObject.SetActive(true);
        inputField.text = data.Text;
        inputField.Select();
        inputField.onDeselect.AddListener(DeactivateInputField);
        Periscope.Periscope.Singleton.SetModeTextInput();
    }

    private void DeactivateInputField(string arg)
    {
        inputField.gameObject.SetActive(false);
        Periscope.Periscope.Singleton.SetModeDefault();
    }

    private void UpdateDataSource(string text)
    {
        data.Text = text;
    }

    #region Copy and Paste

    public static void CopyToClipboard()
    {
        var texts = CellSelectHandler.MarkedCells.Select(c => c.cellData.Text);
        GUIUtility.systemCopyBuffer = string.Join("\n", texts);
    }

    public void PasteIntoCell()
    {
        try
        {
            var selectedCell = CellSelectHandler.MarkedCells.FirstOrDefault();
            if (selectedCell != null && selectHandler == selectedCell)
            {
                selectedCell.MarkExclusive();
                // what's in the buffer?
                if (Uri.IsWellFormedUriString(GUIUtility.systemCopyBuffer, UriKind.Absolute))
                {
                    Debug.Log($"Uri '{GUIUtility.systemCopyBuffer}' pasted into cell");
                    selectedCell.cellData.AddAttachment(new Uri(GUIUtility.systemCopyBuffer));
                    var rt = Periscope.Periscope.Singleton.ImportAttachmentIntoCellRt(GUIUtility.systemCopyBuffer,
                        selectedCell.cellData, Periscope.Periscope.Singleton.ActiveSessionCollectionPath);
                    StartCoroutine(rt);
                }
                else
                {
                    var ellipsis = GUIUtility.systemCopyBuffer.Length > 16 ? "..." : "";
                    var pasteStr = $"{GUIUtility.systemCopyBuffer.Substring(0, 16)}{ellipsis}";
                    Periscope.Periscope.Singleton.message.Log($"Paste into cell ignored: '{pasteStr}' is no valid URI.",
                        MessagePresenter.MessageType.Normal);
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }

    #endregion


    public void OnPointerClick(PointerEventData eventData)
    {
        if (!selectHandler.IsResponsive)
            return;

        if (eventData.clickCount > 1)
            ActivateInputFiled();
    }
}