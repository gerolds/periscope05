﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Periscope
{
    public class CellDetail : MonoBehaviour
    {
         [SerializeField] public VerticalLayoutGroup box;
         [SerializeField] private GameObject panel = null;
         [SerializeField] private TextMeshProUGUI title = null;
         [SerializeField] private TextMeshProUGUI tags = null;
         [SerializeField] private TextMeshProUGUI count = null;
         [SerializeField] private Toggle favorite = null;
         [SerializeField] private Toggle flag = null;
         [SerializeField] private Toggle confirmed = null;
         [SerializeField] private Toggle[] stars = null;
         [SerializeField] private Toggle reject = null;
         [SerializeField] private TagManager tagManager = null;

        List<Cell> _selectedCells = new List<Cell>();

        public static void Notify()
        {
#if DEBUG_ON
            Debug.Log("UpdateDetailView");
#endif
            var markedCellsData = CellSelectHandler.MarkedCells.Select(i => i.GetComponent<Cell>()).ToList();
            Periscope.Singleton.cellDetailPanel.SetDataObject(markedCellsData);
        }
        
        public void SetDataObject(List<Cell> data)
        {
            if (data.Count == 0)
                Debug.Log("SetDataObject Empty");

            _selectedCells = data;
            Refresh();
        }

        public void SetDataObject(Cell data)
        {
            _selectedCells = new List<Cell> {data};
            Refresh();
        }

        public void Refresh()
        {
            var activeCount = _selectedCells.Count;
            count.text = $"{activeCount} items selected";

            if (activeCount == 0)
            {
                Debug.Log($"{nameof(CellDetail)}: none active");
                panel.SetActive(false);
                return;
            }

            var firstData = _selectedCells.First();

            if (activeCount == 1)
            {
#if DEBUG_ON
                Debug.Log($"{nameof(CellDetail)}: one active");
#endif

                panel.SetActive(true);
                title.text = firstData.Text;
                tags.text = firstData.Tags;
                LayoutRebuilder.ForceRebuildLayoutImmediate(box.GetComponent<RectTransform>());

                foreach (Transform o in transform)
                    o.gameObject.SetActive(false);

                favorite.isOn = firstData.Favorite;
                flag.isOn = firstData.Flag;
                confirmed.isOn = firstData.Checked;
                reject.isOn = firstData.Rating < 0;
                stars[0].isOn = firstData.Rating > 0;
                stars[1].isOn = firstData.Rating > 1;
                stars[2].isOn = firstData.Rating > 2;
                stars[3].isOn = firstData.Rating > 3;
                stars[4].isOn = firstData.Rating > 4;

                foreach (Transform o in transform)
                    o.gameObject.SetActive(true);
            }
            else if (activeCount > 1)
            {
#if DEBUG_ON
                Debug.Log($"{nameof(CellDetail)}: multiple active");
#endif
                panel.SetActive(true);
                Debug.Assert(firstData != null, "Got > 1 active, so data should be there");

                if (_selectedCells.All(cell => cell.Text == firstData.Text))
                    title.text = firstData.Text;
                else
                    title.text = $"<{activeCount} values>";

                if (_selectedCells.All(cell => cell.Tags == firstData.Tags))
                    tags.text = firstData.Tags;
                else
                    tags.text = $"<{activeCount} values>";

                LayoutRebuilder.ForceRebuildLayoutImmediate(box.GetComponent<RectTransform>());

                foreach (Transform o in transform)
                    o.gameObject.SetActive(false);

                if (_selectedCells.All(cell => cell.Favorite == firstData.Favorite))
                    favorite.isOn = firstData.Favorite;
                else
                    favorite.isOn = false;

                if (_selectedCells.All(cell => cell.Flag == firstData.Flag))
                    flag.isOn = firstData.Flag;
                else
                    flag.isOn = false;

                if (_selectedCells.All(cell => cell.Checked == firstData.Checked))
                    confirmed.isOn = firstData.Checked;
                else
                    confirmed.isOn = false;

                if (_selectedCells.All(cell => cell.Rating == -1))
                    reject.isOn = true;
                else
                    reject.isOn = false;

                if (_selectedCells.All(cell => cell.Rating == firstData.Rating))
                {
                    stars[0].isOn = firstData.Rating == 0;
                    stars[1].isOn = firstData.Rating == 1;
                    stars[2].isOn = firstData.Rating == 2;
                    stars[3].isOn = firstData.Rating == 3;
                    stars[4].isOn = firstData.Rating == 4;
                }
                else
                {
                    stars[0].isOn = false;
                    stars[1].isOn = false;
                    stars[2].isOn = false;
                    stars[3].isOn = false;
                    stars[4].isOn = false;
                }

                foreach (Transform o in transform)
                    o.gameObject.SetActive(true);
            }

            // redraw tag list
            var selectedTags = new HashSet<string>(_selectedCells.SelectMany(c => c.Tags.Split(',')).ToList());
            tagManager.RebuildTags(selectedTags);
        }

        public bool FavoriteToggle
        {
            //get => TargetCell.Data.Favorite;
            set { _selectedCells.ForEach(cell => cell.Favorite = value); }
        }

        public bool FlagToggle
        {
            //get => TargetCell.Data.Flag;
            set { _selectedCells.ForEach(cell => cell.Flag = value); }
        }

        public bool CheckedToggle
        {
            //get => TargetCell.Data.Checked;
            set { _selectedCells.ForEach(cell => cell.Checked = value); }
        }

        public void SetRating(int r)
        {
            foreach (var cell in _selectedCells)
                cell.Rating = r;
        }

        public bool SetColor0
        {
            set => _selectedCells.ForEach(cell => cell.Color = value ? 0 : cell.Color);
        }

        public bool SetColor1
        {
            set => _selectedCells.ForEach(cell => cell.Color = value ? 1 : cell.Color);
        }

        public bool SetColor2
        {
            set => _selectedCells.ForEach(cell => cell.Color = value ? 2 : cell.Color);
        }

        public bool SetColor3
        {
            set => _selectedCells.ForEach(cell => cell.Color = value ? 3 : cell.Color);
        }

        public bool SetColor4
        {
            set => _selectedCells.ForEach(cell => cell.Color = value ? 4 : cell.Color);
        }
    }
}