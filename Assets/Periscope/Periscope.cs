﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DG.Tweening;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Periscope
{
/*
    public class Edge : IEqualityComparer
    {
        public Edge(int start, int end)
        {
            Start = start;
            End = end;
        }

        public int Start = -1;
        public int End = -1;

        public new bool Equals(object a, object b)
        {
            var aEdge = a as Edge;
            var bEdge = b as Edge;
            if (aEdge != null &&
                bEdge != null)
                return aEdge.Start == bEdge.Start && aEdge.End == bEdge.End;

            return false;
        }

        public int GetHashCode(object obj) => base.GetHashCode();
    }
*/

    // BUG: scroll on stacks also zooms the main view.
    // BUG: Buttons on stacks don't work
    // TODO: Periscope class needs refactoring: At least extract file operations
    // BACKLOG: Paste and resolve URIs (load pictures/texts/RDF)
    // BACKLOG: Make cards reversible ("note on back" or blank)... doubles as a "hide card" feature
    // BACKLOG: Add button to shuffle stacks
    // BACKLOG: Add button to select all cells in a stack
    // BACKLOG: Save drawings
    // BACKLOG: Reduce GC memory allocation during idle operation (currently 9KB per frame)
    // BACKLOG: Reduce GUI-Layout re-draw-calls and texture size by utilising sub-canvases
    // BACKLOG: Improve Performance by rendering small, non-interactive cells as flat textures.
    // BACKLOG: Check performance limits of TextMeshPro.
    // BACKLOG: Simple Markdown to RTF compiler
    // BACKLOG: Import/attach Audio/Video to cells, play on hover.
    // BACKLOG: Attach a video/audio recording to a specific cell.
    // BACKLOG: Make .collection files more robust (maybe provide a "find asset" dialog)
    // BACKLOG: Add a "show asset library in finder" button
    // BACKLOG: Add persistent user settings (i.e. default session)
    // BACKLOG: Paste-support. Add pasted data (image-paths/text) into new cells.
    // BACKLOG: Drag-Move multiple cells on grid.
    // BACKLOG: Command to quarter all cells in a given quad (maybe only at root level) to make room (like split, but for multiple cells).
    // BACKLOG: Make Save/Load async
    // BACKLOG: Button to mark all bookmarked cells
    // DONE: FIXED sometimes cells loose grid alignment during drag&drop
    // DONE: FIXED sometimes cells become unresponsive and the tree icon overlays the cells in that tree
    // DONE: Validate integrity of .json files.
    // DONE: FIXED  bad things happen when a session is in a read-only directory. Check if path is writeable on save (esp. for recovery)
    // DONE: FIXED horizontal framing movement (F) gets stuck half way
    // DONE: Attach an image to a specific cell
    // DONE: PRIO FEATURE auto-select a newly created cell
    // DONE: PRIO FEATURE delete cells
    // DONE: FIXED make drop out of bounds impossible 
    // DONE: FIXED some loaded cells are not added correctly (remain at tiny scale and are unresponsive)
    // DONE: text input cursor grows relatively larger as cells get smaller
    // DONE: Auto-save the session in a recovery file
    // DONE: Option to remove cells.
    // DONE: Collect image references
    // DONE: Import text files (one line = one card).
    public partial class Periscope : MonoBehaviour
    {
       
        #region Members

        public static string Build;
        //[NotNull] public CameraRig MainCameraRig;

        public PanAndZoom panAndZoom = null;
        public SidebarController sidebarController = null;
        [HideInInspector] public Cell lastHoveredCell;
        public ToggleGroup stackToggleGroup;
        public GameObject cellPrefab;
        public List<Cell> cells;
        public RectTransform canvas1Target;
        public RectTransform canvas2Target;
        public RectTransform canvas3Target;
        public RectTransform canvas4Target;
        public RectTransform stack1Target;
        public RectTransform stack2Target;
        public RectTransform stack3Target;
        public RectTransform stack4Target;
        public RectTransform stack5Target;
        public RectTransform stack6Target;
        public CellDetail cellDetailPanel;
        public DragGizmo dragGizmo = null;
        public MessagePresenter message;
        public Camera exportCamera;

        #endregion

        #region Singleton

        [NotNull]
        public static Periscope Singleton
        {
            get
            {
                if (_singleton != null)
                    return _singleton;
                throw new Exception("Periscope singleton instance not initialized.");
            }
        }

        private static Periscope _singleton;

        #endregion

        #region Unity

        private void Awake()
        {
            Build = "P05" + Application.buildGUID;
            Session.CreateEmpty();

            // there can only be one
            if (_singleton != null)
            {
                Destroy(this);
                return;
            }

            _singleton = this;
        }

        private void OnDestroy()
        {
            DeleteUnsavedCollectionItems();
        }

        private void DeleteUnsavedCollectionItems()
        {
            var dir = new DirectoryInfo(UnsavedCollectionPath);
            if (dir.Exists)
            {
                foreach (var file in dir.GetFiles().ToList())
                {
                    file.Delete();
                    Debug.Log($"Unsaved reference {file.Name} deleted.");
                }
            }
        }

        public void Start()
        {
            if (_singleton != null && _singleton != this)
                throw new Exception($"There can only be one instance of {nameof(Periscope)} in the scene.");
            _singleton = this;

            SetModeDefault();
            panAndZoom.ResetView();
            Invoke(nameof(WelcomeMessage), 1f);
            Application.targetFrameRate = TargetFpsActive;
            _timeOfLastActivity = Time.time;

            var tx = Texture2D.whiteTexture;
            tx.wrapMode = TextureWrapMode.Repeat;
            tx.SetPixel(0, 0, Color.white);
            tx.Apply();
            _guiBox = new GUIStyle {normal = {background = tx}};

            DOTween.SetTweensCapacity(500, 20);
            DOTween.CompleteAll(true);
            if (startupSession != null)
                StartCoroutine(BuildSessionFromJson(startupSession.text));
            Debug.Log($"Processor Count {SystemInfo.processorCount}");
        }

        private void Update()
        {
            // TODO: Unhook this from the update loop?
            ThrottleFps();
            UpdateCursor();
            KeyShortcutsHandler();
            SaveRecoveryFileHandler();
        }

        private void OnApplicationFocus(bool hasFocus)
        {
            Application.targetFrameRate = TargetFpsActive;
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            Application.targetFrameRate = TargetFpsSleep;
        }

        #endregion

        #region FPS Throttle

        private float _timeOfLastActivity;
        private Vector3 _prevMousePos;
        private float _smoothFps;
        private const int TargetFpsActive = 60;
        private const int TargetFpsIdle = 30;
        private const int TargetFpsSleep = 5;
        private const float IdleTimeout = .5f;
        private const float SleepTimeout = 10f;

        private void ThrottleFps()
        {
            var mouseDistanceMoved = (Input.mousePosition - _prevMousePos).sqrMagnitude;
            _prevMousePos = Input.mousePosition;

            if (Input.anyKey
                || mouseDistanceMoved > 1f
                || Input.mouseScrollDelta.sqrMagnitude > 0
                || Input.mouseScrollDelta.sqrMagnitude < 0)
            {
                _timeOfLastActivity = Time.time;
            }

            if (Time.time - _timeOfLastActivity > SleepTimeout)
            {
                if (Application.targetFrameRate != TargetFpsSleep)
                {
                    Application.targetFrameRate = TargetFpsSleep;
                    //Debug.Log("Speed off");
                }
            }
            else if (Time.time - _timeOfLastActivity > IdleTimeout)
            {
                if (Application.targetFrameRate != TargetFpsIdle)
                {
                    Application.targetFrameRate = TargetFpsIdle;
                    // terminate all animations that that are still running 
                }
            }
            else
            {
                if (Application.targetFrameRate != TargetFpsActive)
                {
                    Application.targetFrameRate = TargetFpsActive;
                    //Debug.Log("Speed on");
                }
            }
        }

        #endregion

        #region Hotkeys (Refactor this)

        private void KeyShortcutsHandler()
        {
            var ev = EventSystem.current;
            Debug.Assert(ev != null);

            if (InputRouter.CmdCancel)
            {
                ev.SetSelectedGameObject(null);
                CellSelectHandler.ClearMarked();
            }

            if (InputRouter.CmdSave)
            {
                ev.SetSelectedGameObject(null);
                Save();
            }

            if (InputRouter.CmdSaveAs)
            {
                ev.SetSelectedGameObject(null);
                SaveAs();
            }

            if (InputRouter.CmdOpen)
            {
                ev.SetSelectedGameObject(null);
                Open();
            }

            if (InputRouter.CmdNew)
            {
                ev.SetSelectedGameObject(null);
                NewSession();
            }

            if (InputRouter.CmdImport)
            {
                ev.SetSelectedGameObject(null);
                Import();
            }
        }

        #endregion

        #region Startup

        private void WelcomeMessage() => message.Log($"Welcome to {Application.productName} {Application.version}",
            MessagePresenter.MessageType.Normal);

        #endregion

        //private Session _session;

        /*public static async Task<Texture2D> LoadImageFromDiskAsync(string filePath, int targetSize = 512)
        {
            Texture2D tex = null;

            if (File.Exists(filePath))
            {
                var fileData = File.ReadAllBytes(filePath);
                tex = new Texture2D(2, 2, TextureFormat.RGB24, true, false);
                tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.

                var texSize = new Vector2Int(tex.width, tex.height);
                var longSide = Mathf.Max(texSize.x, texSize.y);
                var ratio = (float) longSide / targetSize;
                var newSize = new Vector2Int((int) (texSize.x * ratio), (int) (texSize.y * ratio));
                //Debug.Log($"LoadImageFromDisk {fileData.Length}, {texSize}, {longSide}, {ratio}, {newSize}.");
                await TextureScale.BilinearAsync(tex, newSize.x, newSize.y);
                tex.filterMode = FilterMode.Bilinear;
                tex.name = new FileInfo(filePath).Name;
                //tex.Compress(true);
            }

            return tex;
        }*/


        /*public static bool TryLoadCellGraphic(ref Cell cellData, params string[] searchPaths)
        {
            if (string.IsNullOrEmpty(cellData.GraphicSource))
                return false;

            var loadPath = "";
            if (File.Exists(cellData.GraphicSource))
            {
                loadPath = cellData.GraphicSource;
            }
            else if (searchPaths != null &&
                     searchPaths.Any(i => !string.IsNullOrEmpty(i)))
            {
                var searchPath = new FileInfo(cellData.GraphicSource);
                if (!FindInPaths(searchPath.Name, searchPaths, out loadPath))
                    return false;
            }
            else
            {
                return false;
            }

            var tex = GetTextureFromDisk(loadPath, 1024);
            if (tex != null)
            {
                var shortSide = Mathf.Min(tex.width, tex.height);
                var sprite = Sprite.Create(
                    texture: tex,
                    rect: Rect.MinMaxRect(
                        (tex.width - shortSide) / 2f,
                        (tex.height - shortSide) / 2f,
                        shortSide,
                        shortSide),
                    pivot: new Vector2(0.5f, 0.5f),
                    pixelsPerUnit: 100f,
                    extrude: 0,
                    meshType: SpriteMeshType.FullRect);
                sprite.name = loadPath;
                cellData.GraphicSprite = sprite;
            }
            else
            {
                return false;
            }

            cellData.changed?.Invoke(cellData);
            return true;
        }*/


        // Used to create cells based on images, images that fail to load generate no cell
        /*private IEnumerator ImportImages(string[] paths)
        {
            var sprites = new List<Sprite>();
            var yieldCounter = 0;
            foreach (var path in paths)
            {
                var tex = LoadImageFromDisk(path, 1024);
                if (tex != null)
                {
                    var shortSide = Mathf.Min(tex.width, tex.height);
                    var sprite = Sprite.Create(
                        texture: tex,
                        rect: Rect.MinMaxRect(0, 0, shortSide, shortSide),
                        pivot: new Vector2(0.5f, 0.5f),
                        pixelsPerUnit: 100f);
                    sprite.name = path;
                    sprites.Add(sprite);
                }

                if (++yieldCounter % ImportBatchSize == 0) yield return null;
            }


            message.Log($"Importing {sprites.Count} items.", MessagePresenter.MessageType.Normal);
            //yield return null;
            foreach (var sprite in sprites)
            {
                var cell = CreateEmptyCell(GetActiveStack().id, GetActiveStackDropTarget().position);
                cell.GraphicSource = sprite.name;
                cell.GraphicSprite = sprite;
                if (++yieldCounter % ImportBatchSize == 0) yield return null;
            }
        }*/

        /*public Cell CreateEmptyCell(Transform parent, Vector2 pos)
        {
            GameObject cell = Instantiate(CellPrefab, pos, Quaternion.identity, parent);
            Debug.Assert(cell != null, "Instantiate doesn't fail");

            var dragHandler = cell.GetComponent<CellDragHandler>();
            dragHandler.DragGizmo = DragGizmo;

            var cellData = cell.GetComponent<Cell>();
            cellData.Text = "";
            cellData.GraphicSource = "";
            cellData.Flag = false;
            cellData.Rating = 0;
            cellData.Checked = false;
            cellData.Favorite = false;
            cellData.Tags = "";

            return cellData;
        }*/

        #region Cell Factory

        public Cell CreateCopyOfCell(Cell original, ContainerId container, Vector2 spawnPos)
        {
            Transform parent = GetContainerDropTarget(container);
            Debug.Assert(parent != null);
            var obj = Instantiate(original.gameObject, spawnPos, Quaternion.identity, parent);
            Debug.Assert(obj != null, "Instantiate doesn't fail");

            // nice starting size for animation
            var rect = obj.GetComponent<RectTransform>();            
            rect.sizeDelta = Vector2.one;
            Debug.Log(rect.rect.size);
            
            var cell = obj.GetComponent<Cell>();
            var dropReceiver = parent.GetComponent<ICellDrop>();
            var dragHandler = obj.GetComponent<CellDragHandler>();
            var isWorldSpawn = IsInWorldSpace(container);
            var dropPos = isWorldSpawn ? spawnPos : Vector2.zero;
            dropReceiver.Drop(cell, dropPos);
            dragHandler.dragGizmo = dragGizmo;
            
            Debug.Log(rect.rect.size);
            return cell;
        }

        private void InitNewCell(GameObject cell)
        {
            
        }

        public Cell CreateEmptyCell(ContainerId container, Vector2 spawnPos)
        {
            Transform parent = GetContainerDropTarget(container);
            Debug.Assert(parent);
            GameObject obj = Instantiate(cellPrefab, spawnPos, Quaternion.identity, parent);
            Debug.Assert(obj, "Instantiate doesn't fail");
            
            // nice starting size for animation
            var rect = obj.GetComponent<RectTransform>();            
            rect.sizeDelta = Vector2.one;
            Debug.Log(rect.rect.size);
            
            var cell = obj.GetComponent<Cell>();
            var dropReceiver = parent.GetComponent<ICellDrop>();
            var dragHandler = obj.GetComponent<CellDragHandler>();
            var isWorldSpawn = IsInWorldSpace(container);
            var dropPos = isWorldSpawn ? spawnPos : Vector2.zero;
            dropReceiver.Drop(cell, dropPos);
            dragHandler.dragGizmo = dragGizmo;
            cell.Text = "";
            cell.GraphicSource = "";
            cell.Flag = false;
            cell.Rating = 0;
            cell.Checked = false;
            cell.Favorite = false;
            cell.Tags = "";
            cell.Container = container;
            
            Debug.Log(rect.rect.size);
            return cell;
        }

        #endregion

        #region Commands

        public void About()
        {
            ModalCanvas.Instance.PushChoice(
                $"{Application.productName} Version {Application.version}\n" +
                $"Build {Build} Platform {Application.platform}\n\n" +
                "WARNING - PROTOTYPE SOFTWARE - FOR REVIEW AND DEMONSTRATION PURPOSES ONLY. " +
                "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING " +
                "BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE " +
                "AND NON-INFRINGEMENT.\n\n" +
                "All rights to the software are reserved by the original author. This software contains " +
                "copyrighted material. Distribution of this software is strictly prohibited. If you received " +
                "this software from anyone but the original author or one of their authorized representatives, " +
                "please contact the author. No part of this software (including screenshots and audio-visual " +
                "recordings) may be shared with third parties or published without written consent of all " +
                "rights holders.\n\n" +
                $"For inquiries about the software please refer to the contact information you received " +
                $"with your copy.",
                ("Ok", ModalCanvas.Instance.CancelDialog)
            );
        }

        public void Quit()
        {
            void SaveAndQuit()
            {
                SaveAs();
                Debug.Log("Quitting.");
                ClearRecovery();
                Application.Quit();
            }

            void QuitImmediately()
            {
                Debug.Log("Quitting.");
                ClearRecovery();
                Application.Quit();
            }

            if (!IsSaved)
            {
                ModalCanvas.Instance.PushChoice(
                    "Save changes to the session before quitting?",
                    ("Don't save", QuitImmediately),
                    ("Cancel", ModalCanvas.Instance.CancelDialog),
                    ("SaveAs", SaveAndQuit)
                );
            }
            else
            {
                ClearRecovery();
                QuitImmediately();
            }
        }

        #endregion

        #region Debug

        private void OnGUI()
        {
            const float smoothing = 0.75f;
            var currentFps = 1f / Time.deltaTime;
            _smoothFps = _smoothFps * smoothing + currentFps * (1f - smoothing);
            GUI.Box(new Rect(Screen.width - TargetFpsActive, 0, 1, 1), GUIContent.none, _guiBox);
            GUI.Box(new Rect(Screen.width - (int) _smoothFps, 0, (int) _smoothFps, 1), GUIContent.none, _guiBox);
            //GUI.Label(new Rect(Screen.width - 64, 0, 64, 20), $"{System.GC.GetTotalMemory(false) / 1048576}MB");
        }

        #endregion
    }
}