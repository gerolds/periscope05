using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Periscope
{
    public class DragGizmo : MonoBehaviour
    {
         public Image graphicTarget;
         public Sprite valid;
         public Sprite invalid;

        public void SetValid(bool value) => graphicTarget.sprite = value ? valid : invalid;
        public void SetPosition(Vector2 pos) => transform.position = new Vector3(pos.x, pos.y, transform.position.z);
    }
}