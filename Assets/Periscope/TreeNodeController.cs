﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Periscope
{
    [RequireComponent(typeof(GridLayoutGroup))]
    [ExecuteInEditMode]
    public class TreeNodeController : MonoBehaviour
    {
        public GridLayoutGroup GridLayoutComponent = null;
        public List<TreeNodeController> Bucket = null;
        public int Root = 2;
        public int BucketSize => Root * Root;
        public Vector2 SizeDelta = new Vector2(512, 512);

        private RectTransform _rect = null;
        
        public bool IsFull() => Bucket.Count >= BucketSize;
    
        void Start()
        {
            _rect = GetComponent<RectTransform>();
        }

        // TODO: Unhook this from the update loop?
        void Update()
        {
            GridLayoutComponent.constraintCount = Root;
            GridLayoutComponent.cellSize = new Vector2(_rect.rect.width / 2f, _rect.rect.height / 2f);
            _rect.pivot = new Vector2(0.5f, 0.5f);
            _rect.anchorMin = new Vector2(0.0f, 0.0f);
            _rect.anchorMax = new Vector2(1.0f, 1.0f);
            _rect.localPosition = Vector3.zero;
            _rect.anchoredPosition = Vector3.zero;
            _rect.sizeDelta = SizeDelta;
        }
    }
}