﻿using System;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Periscope
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class ModalCanvas : MonoBehaviour
    {
        [SerializeField] private GameObject[] gameObjectsToDisable = null;
        [SerializeField] private GameObject dialogCanvas = null;
        [SerializeField] private Button buttonPrefab = null;
        [SerializeField] private RectTransform buttonContainer = null;
        [SerializeField] private TextMeshProUGUI note = null;
        [SerializeField] private RectTransform progressBar = null;


        private (GameObject targetObject, bool isActive)[] _savedComponentState = null;
        private static ModalCanvas _instance;
        private Vector2 _baseProgressBarSize;

        public static ModalCanvas Instance
        {
            get
            {
                if (!_instance)
                {
                    _instance = FindObjectOfType(typeof(ModalCanvas)) as ModalCanvas;
                    if (!_instance)
                        Debug.LogError("There needs to be one active ModalPanel script in the scene.");
                }

                return _instance;
            }
        }

        private void Awake()
        {
            Debug.Assert(note != null);
            Debug.Assert(buttonPrefab != null);
            Debug.Assert(buttonContainer != null);
            Debug.Assert(dialogCanvas != null);
            _baseProgressBarSize = progressBar.sizeDelta;
            Deactivate();
        }

        public void PushChoice([NotNull] string text, params (string label, Action action)[] options)
        {
            if (options.Length == 0)
                return;

            this.note.text = text;

            foreach (var (label, action) in options)
            {
                var button = Instantiate(buttonPrefab, buttonContainer);
                button.onClick.AddListener(new UnityAction(action));
                button.onClick.AddListener(Deactivate);
                button.GetComponentInChildren<TextMeshProUGUI>().text = $"{label}";
            }

            /*
        var cancelButton = Instantiate(ButtonPrefab, ButtonContainer);
        cancelButton.GetComponentInChildren<TextMeshProUGUI>().text = "Cancel";
        cancelButton.onClick.AddListener(CancelDialog);
        */

            dialogCanvas.SetActive(true);
            Activate();
            // default-select last child
            buttonContainer.GetChild(buttonContainer.childCount - 1).GetComponent<Selectable>().Select();
        }

        public void PushProgress(string text, float progress, params (string label, Action action)[] options)
        {
            note.text = text;
            progressBar.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _baseProgressBarSize.x * progress);
            dialogCanvas.SetActive(true);
            if (progress >= 1.0f || progress < 0)
                Deactivate();
        }

        private void Update()
        {
            // TODO: Unhook this from the update loop?
            if (InputRouter.CmdCancel)
                CancelDialog();
        }

        public void CancelDialog()
        {
            Deactivate();
        }

        private void Activate()
        {
            _savedComponentState = new (GameObject, bool)[gameObjectsToDisable.Length];
            for (var i = 0; i < gameObjectsToDisable.Length; i++)
            {
                var target = gameObjectsToDisable[i];
                _savedComponentState[i] = (targetObject: target, target.activeSelf);
                target.SetActive(false);
            }
        }

        private void Deactivate()
        {
            for (int i = 0; i < buttonContainer.childCount; i++)
                Destroy(buttonContainer.GetChild(i).gameObject);

            dialogCanvas.SetActive(false);

            if (_savedComponentState == null)
                return;

            foreach (var state in _savedComponentState)
            {
                state.targetObject.SetActive(state.isActive);
            }
        }
    }
}