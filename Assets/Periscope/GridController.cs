﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Periscope
{
    [Obsolete]
    [RequireComponent(typeof(EventTrigger))]
    public class GridController : MonoBehaviour
    {
        public CellDetail CellDetailView;
        public readonly Vector2Int CellCount = new Vector2Int(25, 16);
        public GridMarker Marker;
        public TMP_InputField EditField;
        public List<Cell> AllCells => Periscope.Singleton.cells;
        //public Cell CurrentlyHoveredCell;
        public readonly HashSet<Cell> ActiveCells = new HashSet<Cell>();
        public TagManager tagManager;
        public UnityEvent SelectionChanged;

        private void Awake()
        {
            if (SelectionChanged == null) SelectionChanged = new UnityEvent();
        }

        // Use this for initialization
        void Start()
        {
            foreach (var cell in Periscope.Singleton.cells)
            {
                var cellData = cell.GetComponent<Cell>();
                var cellSelect = cell.GetComponent<CellSelectHandler>();
                //cellSelect.Selected.AddListener(CellSelectHandler);
                //cellSelect.Deselected.AddListener(CellDeselectHandler);
                //cellData.Changed.AddListener(CellChangedHandler);
            }
        }

        private void CellChangedHandler(Cell cell)
        {
            if (ActiveCells.Contains(cell))
                CellDetailView.Refresh();
        }

        private void CellSelectHandler(Cell cell)
        {
            if (!InputRouter.MomentaryCtrl)
            {
                ActiveCells.Clear();
            }

            if (ActiveCells.Contains(cell))
            {
                ActiveCells.Remove(cell);
            }
            else
            {
                ActiveCells.Add(cell);
                Debug.Log($"Activated {cell}");
            }

            // create a set of all words in all active cellData items.
            var tagsInActive = new HashSet<string>();
            ActiveCells.Select(i => i.Tags).ToList()
                .ForEach(txt => txt.Split(' ').ToList()
                    .ForEach(token => tagsInActive.Add(token))
                );
            tagManager.RebuildTags(tagsInActive.ToList());
        
            CellDetailView.Refresh();
        }
    
        private void CellDeselectHandler(Cell cell)
        {
            if (ActiveCells.Contains(cell))
            {
                ActiveCells.Remove(cell);
                Debug.Log($"Deactivated {cell}");
            }
        }
    }
}
