﻿using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

//using UnityEngine.Experimental.UIElements;

namespace Periscope
{
    public class PanAndZoom : MonoBehaviour
    {
        [SerializeField] private Camera targetCamera = null;
        [SerializeField] private BoxCollider cameraPosBoundary = null;
        [SerializeField] private Vector3 speed = Vector3.one;
        [SerializeField] private float minZoom = .0675f;
        [SerializeField] private float maxZoom = 2f;
        [SerializeField] private float safeArea = 0.65f;

        private Vector3 _posTarget = Vector3.zero;
        private float _orthoSizeTarget = 100f;

        [FormerlySerializedAs("overlayCanvas")] [SerializeField]
        private Canvas blockingOverlayCanvas;

        private GraphicRaycaster[] _blockingOverlayRaycasters;
        private bool _isPointerOnOverlay;
        private Vector3 _oldMousePos;
        private Vector3 _currentMousePos;
        private Vector3 _worldMouseDelta;

        private Vector3 WorldMouseDelta => _worldMouseDelta;

        public Cell HoveredCell => Periscope.Singleton.lastHoveredCell;
        //public bool IsReset { get; set; }

        #region Unity

        void Awake()
        {
            Debug.Assert(cameraPosBoundary != null, "Assign in editor");
            _orthoSizeTarget = maxZoom;
            _posTarget = transform.position;
            _blockingOverlayRaycasters = blockingOverlayCanvas.GetComponentsInChildren<GraphicRaycaster>();
        }


        void Update()
        {
            // TODO: Unhook this from the update loop?
            UpdateMouseDelta();
            UpdateBlockingState();
            SetViewTargetsFromMouseInput();
            HotkeyHandler();
        }

        private void UpdateBlockingState()
        {
            var ev = new PointerEventData(EventSystem.current);
            ev.position = Input.mousePosition;
            var raycastResults = new List<RaycastResult>();
            foreach (var caster in _blockingOverlayRaycasters)
                caster.Raycast(ev, raycastResults);
            _isPointerOnOverlay = raycastResults.Count > 0;
        }

        private void UpdateMouseDelta()
        {
            _oldMousePos = _currentMousePos;
            _currentMousePos = Input.mousePosition;
            _worldMouseDelta = targetCamera.ScreenToWorldPoint(_currentMousePos) -
                               targetCamera.ScreenToWorldPoint(_oldMousePos);
        }

        #endregion

        #region Commands

        public void FrameAll()
        {
            var allCells = CellSelectHandler.AllCells;

            // get marked rects in world space (tree cells)
            var worldCells = allCells
                .Where(cell => cell.GetComponentInParent<Canvas>().rootCanvas.renderMode == RenderMode.WorldSpace)
                .Select(cell => (RectTransform) cell.transform)
                .ToList();
            if (worldCells.Count == 0)
                return;

            //Periscope.Singleton.message.Log("Framing selected", MessagePresenter.MessageType.Normal);
            var frame = FindFrameAround(worldCells);
            SetFrameViewTarget(frame.position, Mathf.Max(frame.height, frame.width));
            AnimateMovement();
        }

        public void FrameSelected()
        {
            if (CellSelectHandler.MarkedCells.Count == 0)
                return;

            // get marked rects in world space (tree cells)
            var markedRects = CellSelectHandler.MarkedCells
                .Where(cell => cell.GetComponentInParent<Canvas>().rootCanvas.renderMode == RenderMode.WorldSpace)
                .Select(cell => (RectTransform) cell.transform)
                .ToList();
            if (markedRects.Count == 0)
                return;

            //Periscope.Singleton.message.Log("Framing selected", MessagePresenter.MessageType.Normal);
            var frame = FindFrameAround(markedRects);
            SetFrameViewTarget(frame.position, Mathf.Max(frame.height, frame.width));
            AnimateMovement();
        }

        public void ResetView()
        {
            //Periscope.Singleton.message.Log("Framing all", MessagePresenter.MessageType.Normal);
            SetResetViewTarget();
            AnimateMovement();
        }

        public void CenterViewOn(RectTransform target)
        {
            //var delta = target.position - targetCamera.transform.position;
            SetPanViewTarget(target.position);
            AnimateMovement();
        }

        private void HotkeyHandler()
        {
            // reset view
            if (InputRouter.CmdResetView)
            {
                ResetView();
            }

            if (InputRouter.CmdFrameAll)
            {
                FrameAll();
            }

            // frame view
            if (InputRouter.CmdFrameSelected)
            {
                FrameSelected();
            }

            // keep keyboard navigation in view
            if (InputRouter.CmdHorizontal || InputRouter.CmdVertical)
            {
                KeepLastMarkedCellInView();
            }
        }

        #endregion

        private void AnimateMovement()
        {
            targetCamera.DOOrthoSize(_orthoSizeTarget, 0.25f);

            // update panning movement immediately, animate other movements smoothly
            if (!InputRouter.MomentaryPanning)
                transform.DOMove(_posTarget, 0.25f);
            else
                transform.position = _posTarget;
        }

        private void KeepLastMarkedCellInView()
        {
            // skip if nothing is marked
            if (CellSelectHandler.MarkedCells.Count <= 0)
                return;

            var lastMarked = CellSelectHandler.MarkedCells.Last().GetComponent<RectTransform>();

            // skip if last marked obj is not in world space
            var markedRootCanvas = lastMarked.GetComponentInParent<Canvas>().rootCanvas;
            Debug.Assert(markedRootCanvas != null, "A cell is always member of a canvas.");
            if (markedRootCanvas.renderMode != RenderMode.WorldSpace)
                return;

            // get pan target
            var markedScreenPos = targetCamera.WorldToScreenPoint(lastMarked.position);
            var camFullRect = targetCamera.pixelRect;
            var camSafeRect = new Rect(camFullRect.position, camFullRect.size * safeArea);
            if (!camSafeRect.Contains(markedScreenPos))
                CenterViewOn(lastMarked);
        }

        private static Rect FindFrameAround(IEnumerable<RectTransform> rects)
        {
            var xMin = float.MaxValue;
            var yMin = float.MaxValue;
            var xMax = float.MinValue;

            var yMax = float.MinValue;
            foreach (var rect in rects)
            {
                var pos = rect.position;
                xMin = Mathf.Min(pos.x + rect.rect.xMin, xMin);
                yMin = Mathf.Min(pos.y + rect.rect.yMin, yMin);
                xMax = Mathf.Max(pos.x + rect.rect.xMax, xMax);
                yMax = Mathf.Max(pos.y + rect.rect.yMax, yMax);
            }

            var frame = new Rect(
                new Vector2((xMin + xMax) / 2f, (yMin + yMax) / 2f),
                new Vector2(Mathf.Abs(xMax - xMin), Mathf.Abs(yMax - yMin))
            );
            return frame;
        }

        // TODO: Refactor this
        /// <summary>
        /// Each call aggregates all pan/zoom inputs into a vector by which the camera is moved.
        /// </summary>
        private void SetViewTargetsFromMouseInput()
        {
            //var deltaX = 0.0f; //Input.GetAxis("Horizontal");
            //var deltaY = 0.0f; //Input.GetAxis("Vertical");
            //var deltaZ = 0.0f;
            var delta = Vector3.zero;

            delta.z = Input.GetAxis("Mouse ScrollWheel") * speed.z * targetCamera.orthographicSize;
            delta.x = WorldMouseDelta.x;
            delta.y = WorldMouseDelta.y;

            if (_isPointerOnOverlay)
                delta.z = 0;

            if (!IsMouseInViewport())
                delta.z = 0;

            if (!InputRouter.MomentaryPanning)
            {
                delta.x = 0;
                delta.y = 0;
            }
            if (IsCameraDistinctlyInsideMinMaxZoom())
            {
                // mouse screen pos
                var zoomPanDelta = CalculateZoomPanDelta(delta.z);
                delta.x -= zoomPanDelta.x;
                delta.y -= zoomPanDelta.y;
            }

            delta = new Vector3(-delta.x, -delta.y, delta.z);
            if (delta.sqrMagnitude <= 0)
                return;

            // SET TARGETS

            PanTo(transform.position + new Vector3(delta.x, delta.y, 0));
            ZoomTo(_orthoSizeTarget - delta.z);

            // snap zoom & position
            var isFrustumSmallerThanHoveredCell = HoveredCell && HoveredCell.BoxSize > _orthoSizeTarget * 2f;
             if (isFrustumSmallerThanHoveredCell)
             {
                 // snap position and zoom to frame hovered cell
                 PanTo(HoveredCell.transform.position);
                 ZoomTo(HoveredCell.BoxSize / 2f);
             }

            var isOrthoTargetAtMax = HoveredCell && _orthoSizeTarget == maxZoom;
            var isOrthoTargetAtMin = HoveredCell && _orthoSizeTarget == minZoom;
            var isZoomingOut = delta.z < 0;

            if (isOrthoTargetAtMax && isZoomingOut)
                ResetView();

            if (isOrthoTargetAtMin && !isZoomingOut)
            {
                PanTo(HoveredCell.transform.position);
                ZoomTo(HoveredCell.BoxSize / 2f);
            }

            AnimateMovement();
        }

        private void PanTo(Vector2 pos)
        {
            // constrain camera movement and set targets
            if (!cameraPosBoundary.bounds.Contains(pos))
                _posTarget = cameraPosBoundary.bounds.ClosestPoint(pos);
            else
                _posTarget = pos;
        }

        private void ZoomTo(float target)
        {
            // constrain zoom to min/max
            _orthoSizeTarget = Mathf.Clamp(target, minZoom, maxZoom);
        }

        private void SetZoomTarget(Vector3 aggregateDelta)
        {
        }

        private void SetPanTarget(Vector3 delta)
        {
        }

        private Vector2 CalculateZoomPanDelta(float z)
        {
            var mouse = Input.mousePosition;
            mouse.z = targetCamera.nearClipPlane;
            var zoomTowards = targetCamera.ScreenToWorldPoint(mouse);

            // zoom to mouse position -> pan while zooming -> mock inputs to xy to do this
            var scale = (1.0f / targetCamera.orthographicSize) * z;
            var zoomPanDelta = (zoomTowards - transform.position) * scale;
            
            return zoomPanDelta;
        }

        private bool IsCameraDistinctlyInsideMinMaxZoom()
        {
            var isInsideMinMaxZoom = targetCamera.orthographicSize < maxZoom && targetCamera.orthographicSize > minZoom;
            var camDistinctlyBelowMaxZoom = targetCamera.orthographicSize < maxZoom - 1f;
            var camDistinctlyAboveMinZoom = targetCamera.orthographicSize > minZoom + 1f;

            var isDistinctlyInsideMinMaxZoom = camDistinctlyBelowMaxZoom && camDistinctlyAboveMinZoom;
            return isDistinctlyInsideMinMaxZoom;
        }

        private bool IsMouseInViewport()
        {
            var mouseViewportPos = targetCamera.ScreenToViewportPoint(Input.mousePosition);
            var b = mouseViewportPos.x > 0 && mouseViewportPos.x < 1f && mouseViewportPos.y > 0 &&
                    mouseViewportPos.y < 1f;
            return b;
        }

        private void SetPanViewTarget(Vector2 pos)
        {
            _posTarget = pos;
            _posTarget.z = transform.position.z;
        }

        private void SetResetViewTarget()
        {
            _posTarget = cameraPosBoundary.transform.localToWorldMatrix * cameraPosBoundary.center;
            _posTarget.z = transform.position.z;
            _orthoSizeTarget = maxZoom;
        }

        private void SetFocusViewTarget(Vector2 pos)
        {
            //Debug.Log($"Focus view on {pos}");
            _posTarget = new Vector3(pos.x, pos.y, transform.position.z);
        }

        private void SetFrameViewTarget(Vector3 position, float size)
        {
            //Debug.Log($"Frame view on {position}@{size}");
            _posTarget = position;
            _posTarget.z = transform.position.z;
            _orthoSizeTarget = size * 0.6f;
        }

        // Orthographic camera zoom towards a point (in world coordinates). Negative amount zooms in, positive zooms out
        /*void ZoomOrthoCamera(float zoomStep)
        {
            // Calculate how much we will have to move towards the zoomTowards position
    
            // Move camera
            var maxZoom = _camera.orthographicSize <= MaxZoom - 0.01f;
            var minZoom = _camera.orthographicSize >= MinZoom + 0.01f;
            if (minZoom && maxZoom)
            {
                if (_camera.orthographicSize < MinZoom * 2f)
                    SetFocusViewTargets(Periscope.Singleton.LastHoveredCell.transform.position);
            }
            else
            {
                //if (_camera.orthographicSize > MaxZoom - 0.1f)
                ResetView();
                return;
            }
            //_camera.orthographicSize = _orthoSizeTarget;
        }*/

        /*void OnGUI()
        {
            Vector3 world = new Vector3();
            Event currentEvent = Event.current;
            Vector2 mousePos = new Vector2();

            // Get the mouse position from Event.
            // Note that the y position from Event is inverted.
            mousePos.x = currentEvent.mousePosition.x;
            mousePos.y = targetCamera.pixelHeight - currentEvent.mousePosition.y;

            world = targetCamera.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, targetCamera.nearClipPlane));

            GUILayout.BeginArea(new Rect(20, 20, 250, 250));
            GUILayout.Label("World rect px: " + targetCamera.pixelWidth + ":" + targetCamera.pixelHeight);
            GUILayout.Label("Event mouse: " + mousePos);
            GUILayout.Label("Input mouse: " + Input.mousePosition);
            GUILayout.Label("World mouse: " + world.ToString("F3"));
            GUILayout.Label("World mouse dt: " + _worldMouseDelta);
            GUILayout.EndArea();
        }*/
    }
}