﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareCamera : MonoBehaviour
{
    [SerializeField] private Camera targetCam;

    // Update is called once per frame
    // TODO: Unhook this from the update loop?
    void Update()
    {
        var ratio = Screen.height / (float) Screen.width;
        targetCam.rect = new Rect(ratio/2f, 0, ratio, 1f);
    }
}