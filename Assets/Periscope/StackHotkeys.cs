﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Periscope
{
    public class StackHotkeys : MonoBehaviour
    {
         [SerializeField] private ToggleGroup toggleGroup = null;
        [SerializeField] private Transform togglesParent = null;
        private readonly List<Toggle> _toggles = new List<Toggle>(6);

        private void Start()
        {
            foreach (var toggle in togglesParent.GetComponentsInChildren<Toggle>())
            {
                toggleGroup.RegisterToggle(toggle);
                _toggles.Add(toggle);
                toggle.isOn = _toggles.Count == 1;
            }
        }

        // TODO: Unhook this from the update loop?
        void Update()
        {
            if (!InputRouter.AnyKey)
                return;

            if (InputRouter.CmdSelectStackOne)
            {
                _toggles[0].isOn = true;
            }

            if (InputRouter.CmdSelectStackTwo)
            {
                _toggles[1].isOn = true;
            }

            if (InputRouter.CmdSelectStackThree)
            {
                _toggles[2].isOn = true;
            }

            if (InputRouter.CmdSelectStackFour)
            {
                _toggles[3].isOn = true;
            }

            if (InputRouter.CmdSelectStackFive)
            {
                _toggles[4].isOn = true;
            }

            if (InputRouter.CmdSelectStackSix)
            {
                _toggles[5].isOn = true;
            }
        }
    }
}