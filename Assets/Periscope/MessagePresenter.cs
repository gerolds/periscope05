using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Periscope
{
    public class MessagePresenter : MonoBehaviour
    {
        //[SerializeField] private CanvasGroup CanvasGroupTarget = null;
        // [SerializeField] private Transform messageParent = null;
        [SerializeField] private GameObject messagePrefab = null;
        [SerializeField] private RectTransform progressBar = null;
        [SerializeField] private CanvasGroup progressBarGroup = null;
        [SerializeField] private float messageLifetime = 5.75f;
        [SerializeField] private float fadeInDuration = .25f;
        [SerializeField] private float fadeOutDuration = 4f;
        [SerializeField] private Color defaultTextColor = Color.white;
        [SerializeField] private Color defaultBackgroundColor = Color.black;
        [SerializeField] private Color warningTextColor = Color.yellow;
        [SerializeField] private Color warningBackgroundColor = Color.black;
        [SerializeField] private Color errorTextColor = Color.red;
        [SerializeField] private Color errorBackgroundColor = Color.black;
        private Vector2 _baseProgressBarSize;
        [SerializeField] private Button progressCancelButton;

        private const float TimePadding = 0.25f;

        public UnityEvent progressCanceled;

        public enum MessageType
        {
            Normal,
            Warning,
            Error
        }

        private void Awake()
        {
            _baseProgressBarSize = progressBar.sizeDelta;
            if (progressCancelButton != null)
                progressCancelButton.onClick.AddListener(() => progressCanceled?.Invoke());
        }

        private void Start()
        {
            Progress(1.0f);
        }

        public void Log(string message, MessageType type)
        {
            var msgObj = Instantiate(messagePrefab, Vector3.zero, Quaternion.identity, transform);
            msgObj.transform.SetAsFirstSibling();
            var textComp = msgObj.GetComponentInChildren<TextMeshProUGUI>();
            var groupComp = msgObj.GetComponentInChildren<CanvasGroup>();
            var imageComp = msgObj.GetComponentInChildren<Image>();
            switch (type)
            {
                case MessageType.Normal:
                    imageComp.color = defaultBackgroundColor;
                    textComp.color = defaultTextColor;
                    break;
                case MessageType.Warning:
                    imageComp.color = warningBackgroundColor;
                    textComp.color = warningTextColor;
                    break;
                case MessageType.Error:
                    imageComp.color = errorBackgroundColor;
                    textComp.color = errorTextColor;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }

            groupComp.alpha = 0;
            groupComp.DOFade(1f, fadeInDuration).SetDelay(TimePadding);
            groupComp.DOFade(0f, fadeOutDuration).SetDelay(messageLifetime - fadeOutDuration - TimePadding);
            textComp.text = message;
            Destroy(msgObj, messageLifetime);
        }

        public void Progress(float progress)
        {
            progressBar.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _baseProgressBarSize.x * progress);
            if (progress >= 1.0f || progress < 0)
            {
                progressBarGroup.alpha = 0;
            }
            else
                progressBarGroup.alpha = 1.0f;
        }
    }
}