﻿using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Periscope
{
    public class SessionFilePresenter : MonoBehaviour
    {
        public TextMeshProUGUI textTarget = null;
        public Graphic graphicTarget = null;
        public Color savedColor = new Color(1f, 1f, 1f, 0.1f);
        public Color unsavedChangesColor = new Color(1f, .5f, .5f, 0.1f);

        // TODO: Unhook this from the update loop?
        void Update()
        {
            if (string.IsNullOrEmpty(Periscope.Singleton.ActiveSessionSavePath))
            {
                textTarget.text = $"<i>Unsaved session</i>";
                return;
            }

            var file = new FileInfo(Periscope.Singleton.ActiveSessionSavePath);
            textTarget.text = $"{file.DirectoryName}/<b>{file.Name}</b>";

            if (graphicTarget)
                graphicTarget.color = Periscope.Singleton.IsSaved ? savedColor : unsavedChangesColor;
        }
    }
}