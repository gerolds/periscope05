﻿using QTree;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

namespace Periscope
{
    [RequireComponent(typeof(QuadTreeLayout))]
    public class CreateCellHandler : MonoBehaviour, IPointerClickHandler
    {
         [SerializeField] private Camera referenceCamera = null;
         [SerializeField] private float maxSize = 2048f;
         [SerializeField] private float minSize = 2f;
         public ContainerId id;
        //[SerializeField] private Texture2D _cursorTexture;
        //[SerializeField] private Vector2 _cursorFocus;
        private QuadTreeLayout _tree;


        public void OnPointerClick(PointerEventData eventData)
        {
            // respond only to clean double-clicks
            if (Input.anyKey || eventData.clickCount < 2)
                return;

            // ignore anything but LMB
            if (eventData.button != PointerEventData.InputButton.Left)
                return;

            // create cells only if they aren't too small
            if (referenceCamera.orthographicSize < minSize ||
                referenceCamera.orthographicSize > maxSize)
                return;

            _tree = GetComponent<QuadTreeLayout>();
            var boundarySize = _tree.GetPosCellBoundarySize(eventData.pointerCurrentRaycast.worldPosition);
            if (
                boundarySize.y < referenceCamera.orthographicSize * 2f &&
                boundarySize.y > referenceCamera.orthographicSize / 16f &&
                boundarySize.y > 1.0f)
            {
                var newCell = Periscope.Singleton.CreateEmptyCell(id, eventData.pointerCurrentRaycast.worldPosition);

                var selectHandler = newCell.GetComponentInChildren<CellSelectHandler>();
                if (selectHandler != null)
                {
                    Debug.Log("Select new Cell");
                    selectHandler.Select();
                }
                else
                    selectHandler.Select();
            }
        }
    }
}