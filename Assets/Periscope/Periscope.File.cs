using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using JetBrains.Annotations;
using Newtonsoft.Json;
using QTree;
using StandaloneFileBrowser;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Rendering;
using Debug = UnityEngine.Debug;

namespace Periscope
{
    public partial class Periscope
    {
        #region Save, load and other file related things

        private const string PngExt = "png";
        private const string JpgExt = "jpg";
        private const string JpegExt = "jpeg";
        private const string TxtExt = "txt";
        private const string MdExt = "md";
        private const string Mp3Ext = "mp3";
        private const string WavExt = "wav";
        private const string AacExt = "aac";
        private const string FlacExt = "flac";
        private const string Mp4Ext = "mp4";
        private const string MovExt = "mov";
        private const string MkvExt = "mkv";
        private const string CollectionSuffix = "collection";
        private const string RecoverySuffix = "recovery";
        private const string UnsavedCollectionName = "unsaved";
        private const string SessionExt = "scope";
        private const int ImportFrameDuration = 83; // Milliseconds (83 ms = 12 fps)

        private static readonly JsonSerializerSettings _ccImageDetailSerializerSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore
        };

        public static readonly string[] ValidImageExtensions = {PngExt, JpgExt, JpegExt};
        public static readonly string[] ValidTextExtensions = {TxtExt, MdExt};
        public static readonly string[] ValidAudioExtensions = {Mp3Ext, WavExt, AacExt, FlacExt};
        public static readonly string[] ValidVideoExtensions = {Mp4Ext, MovExt, MkvExt};

        [SerializeField] private TextAsset startupSession = null; // autoload this session
        [SerializeField] private float recoverySaveInterval = 15f; // seconds

        private bool _isSaved = false;
        private bool _saveForRecoveryQueued = true;
        private bool _injectCursor = false;
        private float _recoverySaveTimeout = 0;
        private string _lastImportDir = null;
        private GUIStyle _guiBox = null;
        private Texture2D _injectedCursor = null;
        private Vector2 _injectedCursorFocus = Vector2.zero;

        public string ActiveSessionSavePath { get; private set; } = "";

        private string UnsavedCollectionPath
        {
            get
            {
                var d = new DirectoryInfo(
                    $"{Application.persistentDataPath}{Path.DirectorySeparatorChar}{UnsavedCollectionName}.{CollectionSuffix}");
                if (!d.Exists)
                {
                    d.Create();
                    Debug.Log($"Unsaved collection path {d.FullName} created");
                }
                else
                {
                    //Debug.Log($"Unsaved collection path {d.FullName} exists");
                }

                return d.FullName;
            }
        }

        public static string GetCollectionPathFor(string sessionSavePath)
            => $"{Path.GetFullPath(sessionSavePath)}.{CollectionSuffix}";

        public string ActiveSessionCollectionPath
        {
            get
            {
                if (string.IsNullOrEmpty(ActiveSessionSavePath))
                {
                    return UnsavedCollectionPath;
                }
                else
                {
                    return GetCollectionPathFor(ActiveSessionSavePath);
                }
            }
        }

        public bool IsSaved
        {
            set => _isSaved = value;
            get => _isSaved;
        }

        public void NewSession()
        {
            if (!IsSaved)
            {
                void SaveAndNew()
                {
                    SaveAs();
                    // TODO: Error handling
                    Clear();
                    ActiveSessionSavePath = "";
                    message.Log($"New session started.", MessagePresenter.MessageType.Normal);
                }

                void NewImmediately()
                {
                    Debug.Log("Quitting.");
                    ActiveSessionSavePath = "";
                    Clear();
                }

                ModalCanvas.Instance.PushChoice(
                    "Save changes to the session before closing?",
                    ("Don't save", NewImmediately),
                    ("Cancel", ModalCanvas.Instance.CancelDialog),
                    ("SaveAs", SaveAndNew)
                );
            }
            else
            {
                Clear();
                message.Log($"New session started.", MessagePresenter.MessageType.Normal);
            }
        }

        public void Open()
        {
            var path = ActiveSessionSavePath;
            Debug.Log($"Open {path}");
            //Application.OpenURL(path);
            var filter = new ExtensionFilter[]
            {
                new ExtensionFilter("Periscope Sessions", SessionExt),
            };
            var paths = StandaloneFileBrowser.StandaloneFileBrowser.OpenFilePanel(
                $"Select a .{SessionExt} file",
                path,
                filter,
                false
            );
            StartCoroutine(LoadSessionFromFile(paths[0]));
        }

        public void Import()
        {
            var browserDir = "";
            if (Directory.Exists(_lastImportDir))
            {
                browserDir = _lastImportDir;
            }
            else if (File.Exists(ActiveSessionSavePath))
            {
                var sessionFile = new FileInfo(ActiveSessionSavePath);
                browserDir = sessionFile.DirectoryName;
            }
            else
            {
                browserDir = Application.persistentDataPath;
            }

            Debug.Log($"Open {browserDir}");
            //Application.OpenURL(path);
            var filter = new ExtensionFilter[]
            {
                //new ExtensionFilter("All types", "png", "jpg", "gif", "txt", "md"),
                new ExtensionFilter("Images", ValidImageExtensions),
                new ExtensionFilter("Text", ValidTextExtensions),
                new ExtensionFilter("Video", ValidTextExtensions),
                new ExtensionFilter("Audio", ValidTextExtensions),
                new ExtensionFilter("All", ValidTextExtensions),
            };
            var paths = StandaloneFileBrowser.StandaloneFileBrowser.OpenFilePanel(
                "Select a file in a folder",
                browserDir,
                filter,
                true);


            if (paths.Length > 0)
            {
                if (string.IsNullOrEmpty(paths[0]))
                    return;

                var plural = paths.Length > 1 ? "s" : "";

                // an aborted dialog still returns one empty result.
                if (string.IsNullOrEmpty(paths[0]))
                    return;

                var f = new FileInfo(paths[0]);
                _lastImportDir = f.DirectoryName;

                if (filter[1].Extensions.Contains(f.Extension.Substring(1, f.Extension.Length - 1)))
                {
                    message.Log($"Importing {paths.Length} text file{plural}", MessagePresenter.MessageType.Normal);
                    StartCoroutine(ImportTextRt(paths));
                }

                if (filter[0].Extensions.Contains(f.Extension.Substring(1, f.Extension.Length - 1)))
                {
                    message.Log($"Importing {paths.Length} image{plural}", MessagePresenter.MessageType.Normal);
                    StartCoroutine(ImportImagesRt(paths));
                }
            }
            else
            {
                message.Log($"Nothing imported.", MessagePresenter.MessageType.Normal);
            }
        }

        public void Save()
        {
            FileInfo file = null;

            // try/catch abuse but still the most reliable way to catch invalid paths
            try
            {
                file = new FileInfo(ActiveSessionSavePath);
            }
            catch
            {
                SaveAs();
                return;
            }

            if (!file.Exists)
                SaveAs();
            else
                IsSaved = SaveActiveSessionToFile(file);
        }

        public void SaveAs()
        {
            try
            {
                var dialogTitle = "Save As";
                var dialogDirectory = Application.dataPath;
                var dialogFileName = "Untitled";

                if (!string.IsNullOrEmpty(ActiveSessionSavePath))
                {
                    var info = new FileInfo(ActiveSessionSavePath);
                    dialogDirectory = info.DirectoryName;
                    dialogFileName = info.Name;
                }

                var filter = new ExtensionFilter[]
                {
                    new ExtensionFilter("Periscope Sessions", SessionExt),
                };
                var path = StandaloneFileBrowser.StandaloneFileBrowser.SaveFilePanel(dialogTitle, dialogDirectory,
                    dialogFileName, filter);
                var file = new FileInfo(path);
                ActiveSessionSavePath = file.FullName;
                //SaveToFile("/Users/gs/Desktop/TEST.json");
                IsSaved = SaveActiveSessionToFile(file);
            }
            catch (Exception e)
            {
                message.Log(e.Message, MessagePresenter.MessageType.Error);
            }
        }

        public void ExportMarkdown()
        {
            try
            {
                var dialogTitle = "Export as markdown";
                var dialogDirectory = Application.dataPath;
                var dialogFileName = "Untitled";

                if (!string.IsNullOrEmpty(ActiveSessionSavePath))
                {
                    var info = new FileInfo(ActiveSessionSavePath);
                    dialogDirectory = info.DirectoryName;
                    dialogFileName = info.Name;
                }

                var filter = new ExtensionFilter[]
                {
                    new ExtensionFilter("Markdown", "md"),
                };
                var path = StandaloneFileBrowser.StandaloneFileBrowser.SaveFilePanel(dialogTitle, dialogDirectory,
                    dialogFileName, filter);
                var file = new FileInfo(path);
                IsSaved = ExportToMarkdown(file);
            }
            catch (Exception e)
            {
                message.Log(e.Message, MessagePresenter.MessageType.Error);
            }
        }

        /// <summary>
        /// TODO: Just a stub
        /// </summary>
        public void ExportPng()
        {
            try
            {
                var dialogTitle = "Export as PNG";
                var dialogDirectory = Application.dataPath;
                var suggestedFileName = "Untitled";

                if (!string.IsNullOrEmpty(ActiveSessionSavePath))
                {
                    var sessionFile = new FileInfo(ActiveSessionSavePath);
                    dialogDirectory = sessionFile.DirectoryName;
                    suggestedFileName = sessionFile.Name;
                }

                var filter = new ExtensionFilter[]
                {
                    new ExtensionFilter("PNG", PngExt),
                };
                var path = StandaloneFileBrowser.StandaloneFileBrowser.SaveFilePanel(dialogTitle, dialogDirectory,
                    suggestedFileName, filter);
                var file = new FileInfo(path);
                IsSaved = ExportToPng(file);
            }
            catch (Exception e)
            {
                message.Log(e.Message, MessagePresenter.MessageType.Error);
            }
        }

        /// <summary>
        /// TODO: Just a stub
        /// </summary>
        public void ExportPlainText()
        {
            try
            {
                var dialogTitle = "Export as plain text";
                var dialogDirectory = Application.dataPath;
                var dialogFileName = "Untitled";

                if (!string.IsNullOrEmpty(ActiveSessionSavePath))
                {
                    var info = new FileInfo(ActiveSessionSavePath);
                    dialogDirectory = info.DirectoryName;
                    dialogFileName = info.Name;
                }

                var filter = new ExtensionFilter[]
                {
                    new ExtensionFilter("Text", "txt"),
                };
                var path = StandaloneFileBrowser.StandaloneFileBrowser.SaveFilePanel(dialogTitle, dialogDirectory,
                    dialogFileName, filter);
                var file = new FileInfo(path);
                IsSaved = ExportToPlainText(file);
            }
            catch (Exception e)
            {
                message.Log(e.Message, MessagePresenter.MessageType.Error);
            }
        }

        #region Import image into Cell

        private class ResourceInfo
        {
            public string localUri = null;
            public string remoteUri = null;
            public string licenseInfo = null;
        }

        public IEnumerator ImportAttachmentIntoCellRt(
            [NotNull] string sourceUri,
            [NotNull] Cell cell,
            params string[] searchPaths)
        {
            if (string.IsNullOrEmpty(sourceUri))
                yield break;
            if (cell.Equals(null))
                yield break;

            Debug.Log($"IMPORTING {sourceUri}");
            var info = new ResourceInfo();

            var isHttpUri = Uri.TryCreate(sourceUri, UriKind.Absolute, out var httpUri)
                            && (httpUri.Scheme == Uri.UriSchemeHttp || httpUri.Scheme == Uri.UriSchemeHttps);

            var isFileUri = Uri.TryCreate(sourceUri, UriKind.Absolute, out var fileUri)
                            && fileUri.Scheme == Uri.UriSchemeFile;

            if (isHttpUri)
            {
                var rt = GetRemoteImageRt(searchPaths, httpUri, x => info = x);
                yield return StartCoroutine(rt);
            }
            else if (isFileUri)
            {
                GetLocalResourceInfo(searchPaths, fileUri, ref info);
            }
            else if (searchPaths != null &&
                     searchPaths.Any(i => !string.IsNullOrEmpty(i)))
            {
                // TODO: Why do we need this? Isn't it handled by GetLocalTexture()?
                // source is a local path and DOES NOT exist
                if (!FindInPaths(sourceUri, searchPaths, out info.localUri))
                {
                    Debug.Log($"Search for cached");
                    yield break;
                }
            }
            else
            {
                message.Log($"Import failed: Invalid reference {sourceUri}.", MessagePresenter.MessageType.Normal);
                yield break;
            }

            if (!File.Exists(info.localUri))
            {
                message.Log($"Import failed.", MessagePresenter.MessageType.Warning);
                yield break;
            }

            cell.GraphicSprite = GetSpriteFromDisk(info.localUri);

            if (cell.GraphicSprite.Equals(null))
            {
                message.Log($"Invalid attachment: {info.localUri}", MessagePresenter.MessageType.Normal);
                Debug.Log($"Invalid attachment: {info.localUri}");
            }
            else
            {
                //message.Log($"Attachment saved to: {loadPath}", MessagePresenter.MessageType.Normal);
                cell.SourceAttachmentUri = sourceUri;
                cell.LocalAttachmentUri = info.localUri;
                cell.LicenseInfo = info.licenseInfo;
                cell.changed?.Invoke(cell);
                Debug.Log($"Attachment cached at: {cell.LocalAttachmentUri}");
            }
        }

        private Sprite GetSpriteFromDisk([NotNull] string path)
        {
            if (string.IsNullOrEmpty(path))
                return null;

            if (!File.Exists(path))
                return null;

            var tex = GetTextureFromDisk(path, 1024);
            if (tex != null)
            {
                var shortSide = Mathf.Min(tex.width, tex.height);
                var isWide = tex.width > tex.height;
                var sprite = Sprite.Create(
                    texture: tex,
                    rect: Rect.MinMaxRect(
                        isWide ? (tex.width - shortSide) / 2f : 0,
                        isWide ? 0 : (tex.height - shortSide) / 2f,
                        isWide ? shortSide + (tex.width - shortSide) / 2f : shortSide,
                        isWide ? shortSide : shortSide + (tex.height - shortSide) / 2f
                    ),
                    pivot: new Vector2(0.5f, 0.5f),
                    pixelsPerUnit: 100f,
                    extrude: 0,
                    meshType: SpriteMeshType.FullRect);
                sprite.name = path;
                return sprite;
            }
            else
            {
                return null;
            }
        }

        private static Texture2D GetTextureFromDisk([NotNull] string filePath, int targetSize = 1024)
        {
            Texture2D tex = null;
            Debug.Assert(!string.IsNullOrEmpty(filePath));

            if (File.Exists(filePath))
            {
                var fileData = File.ReadAllBytes(filePath);
                tex = new Texture2D(2, 2, TextureFormat.RGB24, true, false);
                var loadSuccess = tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
                if (!loadSuccess)
                {
                    Debug.Log("LoadImage failed.");
                    return null;
                }

                // only Tex2D makes sense here
                if (tex.dimension != TextureDimension.Tex2D)
                    return null;

                // detect unity's "load failed"-image
                if (tex.width == 8 && tex.height == 8)
                    return null;

                // scale imported textures to targetSize and convert to DXT to reduce memory load
                if (Mathf.Min(tex.width, tex.height) > targetSize)
                {
                    var shortSide = Mathf.Min(tex.width, tex.height);
                    var isWide = tex.width > tex.height;
                    var ratio = (float) targetSize / shortSide;
                    (int width, int height) scaledSize = ((int) (tex.width * ratio), (int) (tex.height * ratio));

                    Debug.Log($"GetTextureFromDisk bytes: {fileData.Length}, " +
                              $"scaled: ({scaledSize.width}, {scaledSize.height}), " +
                              $"ratio: {ratio}, " +
                              $"orig: ({tex.width} {tex.height}).");
                    TextureScale.Bilinear(tex, scaledSize.width, scaledSize.height);
                }

                tex.filterMode = FilterMode.Bilinear;
                //tex.Compress(true);
            }

            return tex;
        }


        private static void GetLocalResourceInfo(string[] searchPaths, Uri fileUri, ref ResourceInfo info)
        {
            Debug.Log($"URI FILE detected.");
            info.localUri = null;
            info.licenseInfo = null;

            if (File.Exists(fileUri.LocalPath))
            {
                info.localUri = fileUri.LocalPath;
                Debug.Log($"URI FILE File found at {info.localUri}");
            }
            else
            {
                // do we have a version in any of the search paths?
                var fileName = $"{Path.GetFileName(fileUri.LocalPath)}";
                Debug.Log($"URI FILE Looking for '{fileName}' in seach paths ...");
                if (searchPaths != null && searchPaths.Any(i => !string.IsNullOrEmpty(i))
                                        && FindInPaths(fileName, searchPaths, out info.localUri))
                {
                    Debug.Log($"URI FILE File found at {info.localUri}");
                }
                else
                {
                    Debug.Log($"URI FILE File not found locally");
                }
            }
        }

        private IEnumerator GetRemoteImageRt(string[] searchPaths, Uri httpUri, Action<ResourceInfo> callback)
        {
            Debug.Log($"URI HTTP detected.");

            ResourceInfo info;
            var isCached = TryGetCachedImageInfo(httpUri, out info, searchPaths);

            if (!isCached)
            {
                Debug.Log($"URI HTTP not cached");

                if (IsCcResource(httpUri))
                {
                    var rt = ExtractInfoFromCcResource(httpUri, x => info = x);
                    yield return StartCoroutine(rt);
                }
                else
                {
                    Debug.Log($"URI HTTP Generic URI detected.");
                    info.remoteUri = httpUri.AbsoluteUri;
                }

                if (!string.IsNullOrEmpty(info.remoteUri))
                {
                    var filename = GenerateImportedFileName(httpUri);
                    info.localUri = GetPathInCollection(filename);
                    var rt = ImportRemoteImageRt(info.remoteUri, info.localUri);
                    yield return StartCoroutine(rt);
                }
                else
                {
                    Debug.LogWarning($"URI HTTP Acquisition of resource from {httpUri.AbsoluteUri} failed.");
                }
            }

            callback.Invoke(info);
        }

        private string GetPathInCollection(string importedFileName)
            => $"{ActiveSessionCollectionPath}{Path.DirectorySeparatorChar}{importedFileName}";

        private IEnumerator ExtractInfoFromCcResource(Uri httpUri, Action<ResourceInfo> callback)
        {
            Debug.Log($"URI HTTP Creative Commons URI detected.");
            var licenseInfoJson = "";
            var info = new ResourceInfo();
            yield return StartCoroutine(GetCreativeCommonsImageDetailRt(httpUri, x => licenseInfoJson = x));
            var licenseInfo =
                JsonConvert.DeserializeObject<CcImageDetail>(licenseInfoJson, _ccImageDetailSerializerSettings);
            var cleanLicenseInfoJson = JsonConvert.SerializeObject(licenseInfo, _ccImageDetailSerializerSettings);
            if (licenseInfo != null)
            {
                info.remoteUri = licenseInfo.url;
                info.licenseInfo = cleanLicenseInfoJson;
            }
            else
            {
                Debug.Log($"URI HTTP Obtaining image detail failed.");
            }

            callback.Invoke(info);
        }

        private bool TryGetCachedImageInfo(Uri httpUri, out ResourceInfo info, params string[] searchPaths)
        {
            // do we have a cached version in any of the search paths?
            info = new ResourceInfo();

            var importedFileName = GenerateImportedFileName(httpUri);
            Debug.Log($"URI HTTP Looking for cached image filename {importedFileName}");
            if (searchPaths != null && searchPaths.Any(i => !string.IsNullOrEmpty(i))
                                    && FindInPaths(importedFileName, searchPaths, out info.localUri))
            {
                Debug.Log($"URI HTTP Image cached at {info.localUri}");
                return true;
            }

            return false;
        }

        private static string GenerateImportedFileName(Uri httpUri)
        {
            var importedFileName = $"{Path.GetFileName(httpUri.LocalPath)}.{PngExt}";
            return importedFileName;
        }

        private static bool IsCcResource(Uri httpUri) => httpUri.Host.Contains("creativecommons.org");

        private static IEnumerator GetCreativeCommonsImageDetailRt(Uri httpUri, Action<string> callback)
        {
            var uuidMatch = UuidMatch(httpUri);
            if (!uuidMatch.Success)
            {
                Debug.Log($"URI HTTP CC No UUID found in URI.");
                yield break;
            }

            // extract image uuid
            var queryUri = new Uri($"https://api.creativecommons.engineering/v1/images/{uuidMatch.Value}");
            Debug.Log(
                $"URI HTTP CC UUID found: {uuidMatch.Value}, querying creative commons API: {queryUri.AbsoluteUri}");
            using (var request = UnityWebRequest.Get(queryUri))
            {
                request.timeout = 500;
                yield return request.SendWebRequest();
                if (request.isNetworkError || request.isHttpError)
                {
                    Debug.Log($"{request.error}");
                    Singleton.message.Log(request.error, MessagePresenter.MessageType.Warning);
                }
                else
                {
                    var payload = request.downloadHandler.text;
                    Debug.Log(
                        $"Web request returned {request.responseCode}. {payload.Length} bytes received.\n {payload}");
                    var licenseInfo =
                        JsonConvert.DeserializeObject<CcImageDetail>(payload, _ccImageDetailSerializerSettings);
                    var cleanPayload = JsonConvert.SerializeObject(licenseInfo, _ccImageDetailSerializerSettings);
                    Debug.Log($"URI HTTP CC Source obtained {licenseInfo.license} : {licenseInfo.url}.");
                    callback.Invoke(cleanPayload);
                }
            }
        }

        private static Match UuidMatch(Uri httpUri)
        {
            var uuidRegex = new Regex(
                @"(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}");
            var uuidMatch = uuidRegex.Match(httpUri.AbsoluteUri);
            return uuidMatch;
        }

        /// <summary>
        /// Downloads the source into a local .png file
        /// </summary>
        private IEnumerator ImportRemoteImageRt(
            [NotNull] string sourceUrl,
            [NotNull] string destination)
        {
            Debug.Assert(destination.EndsWith(PngExt));

            //message.Log($"Downloading: {sourceUrl}", MessagePresenter.MessageType.Normal);
            //message.Log($"Import of attachment failed: Destination invalid ({destination}).",  MessagePresenter.MessageType.Warning);
            //yield break;

            // get the file
            using (UnityWebRequest web = UnityWebRequestTexture.GetTexture(sourceUrl))
            {
                var aborted = false;

                void CancelDownload()
                {
                    web.Abort();
                    aborted = true;
                }

                web.SendWebRequest();
                message.progressCanceled.AddListener(CancelDownload);

                while (!web.isDone)
                {
                    message.Progress(web.downloadProgress);

                    if (web.isNetworkError)
                    {
                        message.Log($"Network error: {web.error}",
                            MessagePresenter.MessageType.Warning);
                    }

                    if (web.isHttpError)
                    {
                        message.Log($"Http error: {web.error}", MessagePresenter.MessageType.Warning);
                    }

                    if (aborted)
                    {
                        message.Log("Download aborted.", MessagePresenter.MessageType.Warning);
                        yield break;
                    }

                    yield return null;
                }

                message.Progress(1f);
                message.progressCanceled.RemoveListener(CancelDownload);

                //message.Log("Download complete.", MessagePresenter.MessageType.Normal);

                var tx = DownloadHandlerTexture.GetContent(web);
                if (tx == null)
                {
                    message.Log("Import of attachment failed: Unknown type.", MessagePresenter.MessageType.Warning);
                    yield break;
                }

                var png = tx.EncodeToPNG();
                var targetPath = new FileInfo(destination);
                try
                {
                    Debug.Log($"Writing image to {targetPath.FullName} ...");
                    File.WriteAllBytes(targetPath.FullName, png);
                }
                catch (Exception e)
                {
                    message.Log($"Import of attachment failed: {e}.", MessagePresenter.MessageType.Warning);
                }
            }
        }

        #endregion

        private static bool FindInPaths(string path, string[] searchPaths, out string foundPath)
        {
            // search for file in searchPaths
            var searchFile = new FileInfo(path);
            foreach (var searchPath in searchPaths)
            {
                try
                {
                    if (string.IsNullOrEmpty(searchPath))
                        continue;
                    var searchDir = new DirectoryInfo(searchPath);
                    if (!searchDir.Exists)
                        continue;
                    var found = searchDir.GetFiles().FirstOrDefault(iterFile => iterFile.Name == searchFile.Name);
                    if (found == null) continue;
                    foundPath = found.FullName;
                    return true;
                }
                catch
                {
                    continue;
                }
            }

            foundPath = null;
            return false;
        }


        private IEnumerator CreateCellFromSessionRt(int i, [NotNull] Session session)
        {
            Debug.Assert(session != null);
            Debug.Log($"Creating cell {i} ...");

            var container = (ContainerId) session.Container[i];
            Transform parent = null;
            parent = GetContainerDropTarget(container);
            Debug.Assert(parent);
            var isWorldSpawn = IsInWorldSpace(container);
            var spawnPos = isWorldSpawn
                ? new Vector2(session.X[i], session.Y[i])
                : new Vector2(parent.position.x, parent.position.y);

            var obj = Instantiate(
                cellPrefab,
                spawnPos,
                Quaternion.identity,
                parent);
            obj.name = $"{new Vector2(session.X[i], session.Y[i])}";

            var objRecvOnDrag = obj.GetComponent<CellDragHandler>();
            objRecvOnDrag.dragGizmo = dragGizmo;

            var cell = obj.GetComponent<Cell>();
            cell.Text = session.Text[i];
            cell.Flag = session.Flag[i];
            cell.Rating = session.Rating[i];
            cell.Checked = session.Checked[i];
            cell.Favorite = session.Favorite[i];
            cell.Tags = session.Tags[i];
            cell.LicenseInfo = session.LicenseInfo[i];
            cell.Container = container;
            try
            {
                cell.LocalAttachmentUri = session.LocalAttachmentUri[i];
                cell.SourceAttachmentUri = session.SourceAttachmentUri[i];
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }

            var dropReceiver = parent.GetComponent<ICellDrop>();
            dropReceiver.Drop(cell, isWorldSpawn ? spawnPos : Vector2.zero);

            if (!string.IsNullOrEmpty(cell.SourceAttachmentUri))
            {
                var rt = ImportAttachmentIntoCellRt(cell.SourceAttachmentUri, cell, $"{session.CollectionPath}",
                    Application.streamingAssetsPath, Application.persistentDataPath, UnsavedCollectionPath);
                yield return StartCoroutine(rt);
            }

#if DEBUG_ON
            Debug.Log(
                $"Cell {new Vector2(_activeSession.X[i], _activeSession.Y[i])} Created @{cell.transform.position}");
#endif
        }

        public string SelectSingleImageOnDisk()
        {
            string browserDir = "";
            if (Directory.Exists(_lastImportDir))
                browserDir = _lastImportDir;
            else
                browserDir = Application.streamingAssetsPath;

            Debug.Log($"Open {browserDir}");
            var filter = new ExtensionFilter[]
            {
                new ExtensionFilter("Images", ValidImageExtensions),
            };
            var paths = StandaloneFileBrowser.StandaloneFileBrowser.OpenFilePanel(
                "Select an image file",
                browserDir,
                filter,
                false);

            // message.Log($"{paths.Length} files selected.", MessagePresenter.MessageType.Normal);

            if (paths.Length == 1)
            {
                if (string.IsNullOrEmpty(paths[0]))
                    return null;

                var f = new FileInfo(paths[0]);
                return f.Exists ? f.FullName : null;
            }

            // message.Log($"Nothing selected.", MessagePresenter.MessageType.Normal);
            // Debug.Log("Nothing selected");
            return null;
        }

        private IEnumerator ImportTextRt(string[] paths)
        {
            var collectedLines = new List<string>();
            var sw = Stopwatch.StartNew();
            for (var i = 0; i < paths.Length; i++)
            {
                var path = paths[i];
                var readLines = File.ReadAllLines(path);
                foreach (var line in readLines)
                {
                    if (sw.ElapsedMilliseconds > 99)
                    {
                        sw.Restart();
                        message.Progress((float) i / paths.Length / 2.0f);
                        yield return null;
                    }

                    collectedLines.Add(line);
                }
            }

            message.Log($"Importing {collectedLines.Count} items.", MessagePresenter.MessageType.Normal);

            for (var i = 0; i < collectedLines.Count; i++)
            {
                var line = collectedLines[i];
                if (sw.ElapsedMilliseconds > 33)
                {
                    sw.Restart();
                    message.Progress((float) i / paths.Length / 2.0f + 0.5f);
                    yield return null;
                }

                var cell = CreateEmptyCell(GetActiveStack().id, GetActiveStackDropTarget().position);
                cell.Text = line;
            }

            message.Progress(1.0f);
        }

        private IEnumerator ImportImagesRt(string[] paths)
        {
            message.Progress(0);
            message.Log($"Importing {paths.Length} items.", MessagePresenter.MessageType.Normal);

            Sprite MakeSprite(Texture2D tx)
            {
                Sprite sprite = null;
                if (tx != null && tx.width > 0 && tx.height > 0)
                {
                    var shortSide = Mathf.Min(tx.width, tx.height);
                    sprite = Sprite.Create(
                        texture: tx,
                        rect: Rect.MinMaxRect(0, 0, shortSide, shortSide),
                        pivot: new Vector2(0.5f, 0.5f),
                        pixelsPerUnit: 100f);
                    sprite.name = tx.name;
                }

                return sprite;
            }

            // make sprites from images
            var spriteInfos = new List<(Sprite sprite, string sourcePath)>();
            var sw = Stopwatch.StartNew();
            for (var i = 0; i < paths.Length; i++)
            {
                if (sw.ElapsedMilliseconds > 99)
                {
                    sw.Restart();
                    message.Progress((float) i / paths.Length);
                    if (InputRouter.CmdCancel)
                    {
                        message.Log("Import aborted.", MessagePresenter.MessageType.Normal);
                        message.Progress(1.0f);
                        yield break;
                    }

                    yield return null;
                }

                var tex = GetTextureFromDisk(paths[i], 1024);
                if (!tex)
                    continue;

                GC.Collect(0);
                var sprite = MakeSprite(tex);
                if (!sprite)
                    continue;

                spriteInfos.Add((sprite: sprite, sourcePath: paths[i]));
            }

            // create a card for each sprite
            var cardsCreated = 0;
            for (var i = 0; i < spriteInfos.Count; i++)
            {
                if (sw.ElapsedMilliseconds > 33)
                {
                    sw.Restart();
                    message.Progress((float) i / spriteInfos.Count);
                    yield return null;
                    if (InputRouter.CmdCancel)
                    {
                        message.Log("Import aborted.", MessagePresenter.MessageType.Normal);
                        break;
                    }
                }

                var spriteInfo = spriteInfos[i];
                var cell = CreateEmptyCell(GetActiveStack().id, GetActiveStackDropTarget().position);
                cell.LocalAttachmentUri = spriteInfo.sourcePath;
                cell.SourceAttachmentUri = spriteInfo.sourcePath;
                cell.GraphicSprite = spriteInfo.sprite;
                cardsCreated++;
            }

            message.Log($"{cardsCreated} cards added.", MessagePresenter.MessageType.Normal);

            message.Progress(1.0f);
        }

        public void DeleteUniqueAttachment(Cell cellToBeDeleted)
        {
            // delete attachments that aren't referenced anymore
            bool OtherHasSameAttachment(Cell item) =>
                item != cellToBeDeleted && item.LocalAttachmentUri == cellToBeDeleted.LocalAttachmentUri;

            bool IsInCollection(string filePath)
            {
                if (string.IsNullOrEmpty(filePath))
                    return false;
                if (!File.Exists(filePath))
                    return false;

                var collectionDir = new DirectoryInfo(Periscope.Singleton.ActiveSessionCollectionPath);

                var file = new FileInfo(filePath);
                if (!file.Exists)
                    return false;

                var fileDir = file.Directory;
                if (fileDir == null)
                    return false;

                return collectionDir.FullName == fileDir.FullName;
            }

            var isSharedRef = cells.Any(OtherHasSameAttachment);
            var isInCollection = IsInCollection(cellToBeDeleted.LocalAttachmentUri);

            if (!isSharedRef && isInCollection)
            {
                File.Delete(cellToBeDeleted.LocalAttachmentUri);
                message.Log($"Attachment deleted", MessagePresenter.MessageType.Normal);
            }
            else
            {
                Debug.Log($"Shared: {isSharedRef}; Collection: {isInCollection}");
            }
        }

        private IEnumerator LoadSessionFromFile(string path)
        {
            if (string.IsNullOrEmpty(path))
                yield break;

            string sessionJson = null;
            try
            {
                if (!File.Exists(path))
                {
                    Debug.LogWarning($"Session does not exist: {path}");
                    message.Log($"Session does not exist: {path}", MessagePresenter.MessageType.Warning);

                    yield break;
                }

                sessionJson = File.ReadAllText(path);
            }
            catch (Exception e)
            {
                message.Log($"{e.Message}", MessagePresenter.MessageType.Warning);
                yield break;
            }

            yield return null;
            yield return StartCoroutine(BuildSessionFromJson(sessionJson));
            ActiveSessionSavePath = path;
            message.Log($"Session restored from {path}", MessagePresenter.MessageType.Normal);
        }

        private bool ValidateSession([NotNull] Session session)
        {
            var itemCount = session.Container.Count;
            var validCount = session.Container.Count == itemCount
                             //             && session.LocalAttachmentUri.Count == itemCount
                             //             && session.SourceAttachmentUri.Count == itemCount
                             && session.Text.Count == itemCount
                             && session.X.Count == itemCount
                             && session.Y.Count == itemCount
                //             && session.Tags.Count == itemCount
                //             && session.Rating.Count == itemCount
                //             && session.Favorite.Count == itemCount
                //             && session.Flag.Count == itemCount
                //             && session.Checked.Count == itemCount
                //             && session.Reverse.Count == itemCount
                //             && session.Reversed.Count == itemCount
                ;
            var validMeta = !string.IsNullOrEmpty(session.Product)
                            && !string.IsNullOrEmpty(session.Version)
                            && !string.IsNullOrEmpty(session.Build)
                            && !string.IsNullOrEmpty(session.Company)
                            && !string.IsNullOrEmpty(session.Identifier)
                            && !string.IsNullOrEmpty(session.DataPath)
                            && !string.IsNullOrEmpty(session.PersistentDataPath)
                            && !string.IsNullOrEmpty(session.Timestamp);
            Debug.Log(
                $"Valid meta: {validMeta}; Valid count: {validCount} [{session.Container.Count} {session.Text.Count} {session.X.Count} {session.Y.Count}]");
            return validMeta && validCount;
        }


        private (int major, int minor, int revision) ParseVersionString(string versionString)
        {
            var ver = versionString.Split('.');
            var version = (major: int.Parse(ver[0]), minor: int.Parse(ver[1]), revision: int.Parse(ver[2]));
            return version;
        }

        private bool MigrateOldSessionFormat(Session session)
        {
            // convert old file formats
            var version = ParseVersionString(session.Version);
            if (version.major == 0 && version.minor == 5 && version.revision < 8)
            {
                session.LocalAttachmentUri.Clear();
                session.Path.ForEach(i => session.LocalAttachmentUri.Add(i));

                session.SourceAttachmentUri.Clear();
                session.Path.ForEach(i => session.SourceAttachmentUri.Add(i));
            }

            return ValidateSession(session);
        }

        private IEnumerator BuildSessionFromJson([NotNull] string json)
        {
            var session = JsonUtility.FromJson<Session>(json);
            if (!MigrateOldSessionFormat(session))
            {
                message.Log($"Invalid session. No cards restored.", MessagePresenter.MessageType.Warning);
                yield break;
            }

            // removing old session
            Clear();

            var itemsInSession = session.Container.Count;

            var stopwatch = Stopwatch.StartNew();
            yield return null;

            // TODO: Refactor to encapsulate this deactivation in QuadTreeLayout
            // deactivate qTree layout for batch-adding items
            canvas1Target.GetComponent<QuadTreeLayout>().enabled = false;
            canvas2Target.GetComponent<QuadTreeLayout>().enabled = false;
            canvas3Target.GetComponent<QuadTreeLayout>().enabled = false;
            canvas4Target.GetComponent<QuadTreeLayout>().enabled = false;
            var createdItemsCount = 0;
            for (var i = itemsInSession - 1; i >= 0; i--)
            {
                if (stopwatch.ElapsedMilliseconds > ImportFrameDuration)
                {
                    stopwatch.Restart();
                    message.Progress((float) i / itemsInSession);
                    if (InputRouter.CmdCancel)
                    {
                        message.Log("Opening session aborted.", MessagePresenter.MessageType.Normal);
                        message.Progress(1.0f);
                        break;
                    }

                    yield return null;
                }

                var rt = CreateCellFromSessionRt(i, session);
                yield return StartCoroutine(rt);
                createdItemsCount++;
            }

            canvas1Target.GetComponent<QuadTreeLayout>().enabled = true;
            canvas2Target.GetComponent<QuadTreeLayout>().enabled = true;
            canvas3Target.GetComponent<QuadTreeLayout>().enabled = true;
            canvas4Target.GetComponent<QuadTreeLayout>().enabled = true;

            message.Progress(1.0f);
            message.Log($"Session restored with {createdItemsCount} cards.",
                MessagePresenter.MessageType.Normal);
        }

        public void Clear()
        {
            // removing old session
            ClearRecovery();
            for (int i = 0;
                i < cells.Count;
                i++)
                Destroy(cells[i].gameObject);
            _lastImportDir = "";
            cells.Clear();
            ActiveSessionSavePath = "";
            SetModeDefault();
            panAndZoom.ResetView();
            IsSaved = false;
            message.Log($"Session cleared.", MessagePresenter.MessageType.Normal);
        }

        public void SaveRecoveryFile()
        {
            _saveForRecoveryQueued = true;
            //Debug.Log("SaveRecovery Queued");
        }

        private void SaveRecoveryFileHandler()
        {
            if (IsSaved
                || !_saveForRecoveryQueued
                || string.IsNullOrEmpty(ActiveSessionSavePath)
                || !(Time.time > _recoverySaveTimeout))
                return;
            var recoveryFile = new FileInfo($"{ActiveSessionSavePath}.{RecoverySuffix}");
            SaveActiveSessionToFile(recoveryFile, true);
            _saveForRecoveryQueued = false;
            _recoverySaveTimeout = Time.time + recoverySaveInterval;
        }

        private void ClearRecovery()
        {
            if (string.IsNullOrEmpty(ActiveSessionSavePath))
                return;
            var recoveryFile = new FileInfo($"{ActiveSessionSavePath}.{RecoverySuffix}");
            if (recoveryFile.Exists)
                recoveryFile.Delete();
            var recoveryCollectionDir = new DirectoryInfo($"{recoveryFile.FullName}.{CollectionSuffix}");
            if (recoveryCollectionDir.Exists)
                recoveryCollectionDir.Delete(true);
        }

        private bool ExportToPlainText(FileInfo info, bool silent = false)
        {
            if (info == null)
            {
                message.Log("Export failed: No path.", MessagePresenter.MessageType.Warning);
                return false;
            }

            if (info.Directory != null
                && (info.Directory.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
            {
                message.Log($"Export failed: Destination is not writable.", MessagePresenter.MessageType.Warning);
                return false;
            }

            var export = new StringBuilder();

            int PosToIndexEuclid(Cell c)
            {
                var cellRt = (RectTransform) c.transform;
                var treeRt = (RectTransform) cellRt.parent;
                var localPos = cellRt.localPosition;
                var nwCorner = new Vector3(treeRt.rect.xMin, treeRt.rect.yMax, 0);
                return (int) c.Container * 16384 + (int) (localPos + nwCorner).magnitude;
            }

            int PosToIndexManhattan(Cell c)
            {
                var cellRt = (RectTransform) c.transform;
                var treeRt = (RectTransform) cellRt.parent;
                var localPos = cellRt.localPosition;
                var nwCorner = new Vector3(treeRt.rect.xMin, treeRt.rect.yMax, 0);
                var posRelCorner = localPos + nwCorner;
                return (int) c.Container * 16384 + (int) (posRelCorner.x + posRelCorner.y);
            }

            var sortedCells = cells.OrderByDescending(PosToIndexManhattan).Reverse().ToList();
            foreach (var cell in sortedCells)
            {
                var rt = (RectTransform) cell.transform;
                var rect = rt.rect;
                if (!string.IsNullOrEmpty(cell.Text))
                    export.Append($"{cell.Text}\n");
                else
                    export.Append($"(no text)\n");
            }

            if (info.Exists)
                info.Delete();
            try

            {
                File.WriteAllText(info.FullName, export.ToString());
            }
            catch (Exception e)
            {
                message.Log($"{e.Message}", MessagePresenter.MessageType.Warning);
                return false;
            }

            Debug.Log($"Export saved to {info.FullName}\n{export}");
            if (!silent) message.Log($"Export saved to {info.FullName}", MessagePresenter.MessageType.Normal);
            return true;
        }

        private bool ExportToPng(FileInfo info, bool silent = false)
        {
            if (info == null)
            {
                message.Log("Export failed: No path.", MessagePresenter.MessageType.Warning);
                return false;
            }

            if (info.Directory != null
                && (info.Directory.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
            {
                message.Log($"Export failed: Destination is not writable.", MessagePresenter.MessageType.Warning);
                return false;
            }


            RenderTexture rt = new RenderTexture(4096, 4096, 24);
            exportCamera.targetTexture = rt;
            Texture2D export = new Texture2D(4096, 4096, TextureFormat.RGB24, false);
            exportCamera.Render();
            RenderTexture.active = rt;
            export.ReadPixels(new Rect(0, 0, 4096, 4096), 0, 0);
            exportCamera.targetTexture = null;
            RenderTexture.active = null; // JC: added to avoid errors
            Destroy(rt);
            byte[] bytes = export.EncodeToPNG();

            if (info.Exists)
                info.Delete();

            try
            {
                File.WriteAllBytes(info.FullName, bytes);
            }
            catch (Exception e)
            {
                message.Log($"{e.Message}", MessagePresenter.MessageType.Warning);
                return false;
            }

            Debug.Log($"Export saved to {info.FullName}");
            if (!silent) message.Log($"Export saved to {info.FullName}", MessagePresenter.MessageType.Normal);
            return true;
        }

        private bool ExportToMarkdown(FileInfo info, bool silent = false)
        {
            if (info == null)
            {
                message.Log("Export failed: No path.", MessagePresenter.MessageType.Warning);
                return false;
            }

            if (info.Directory != null
                && (info.Directory.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
            {
                message.Log($"Export failed: Destination is not writable.", MessagePresenter.MessageType.Warning);
                return false;
            }

            var export = new StringBuilder();

            int PosToIndexEuclid(Cell c)
            {
                var cellRt = (RectTransform) c.transform;
                var treeRt = (RectTransform) cellRt.parent;
                var localPos = cellRt.localPosition;
                var nwCorner = new Vector3(treeRt.rect.xMin, treeRt.rect.yMax, 0);
                return (int) c.Container * 16384 + (int) (localPos + nwCorner).magnitude;
            }

            int PosToIndexManhattan(Cell c)
            {
                var cellRt = (RectTransform) c.transform;
                var treeRt = (RectTransform) cellRt.parent;
                var localPos = cellRt.localPosition;
                var nwCorner = new Vector3(treeRt.rect.xMin, treeRt.rect.yMax, 0);
                var posRelCorner = localPos + nwCorner;
                return (int) c.Container * 16384 + (int) (posRelCorner.x + posRelCorner.y);
            }

            var sortedCells = cells.OrderByDescending(PosToIndexManhattan).Reverse().ToList();
            foreach (var cell in sortedCells)
            {
                var rt = (RectTransform) cell.transform;
                var rect = rt.rect;
                if (!string.IsNullOrEmpty(cell.GraphicSource))
                    export.Append($"![]({cell.GraphicSource})\n");
                if (rect.width == 256)
                    export.Append("### ");
                if (rect.width == 512)
                    export.Append("## ");
                if (rect.width == 1024)
                    export.Append("# ");
                if (!string.IsNullOrEmpty(cell.Text))
                    export.Append($"{cell.Text}\n");
                else
                    export.Append($"(no text)\n");

                if (!string.IsNullOrEmpty(cell.Tags))
                    export.Append($"Tags: {cell.Tags}\n");
                export.Append("\n");
            }

            if (info.Exists)
                info.Delete();
            try

            {
                File.WriteAllText(info.FullName, export.ToString());
            }
            catch (Exception e)
            {
                message.Log($"{e.Message}", MessagePresenter.MessageType.Warning);
                return false;
            }

            Debug.Log($"Export saved to {info.FullName}\n{export}");
            if (!silent) message.Log($"Export saved to {info.FullName}", MessagePresenter.MessageType.Normal);
            return true;
        }

        private bool SaveActiveSessionToFile(FileInfo file, bool silent = false)
        {
            if (file == null)
            {
                message.Log("Session not saved: No path.", MessagePresenter.MessageType.Warning);
                return false;
            }

            if (file.Directory != null
                && (file.Directory.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
            {
                message.Log($"Session not saved: Destination is not writable.", MessagePresenter.MessageType.Warning);
                return false;
            }

            //if (!silent) Message.Log($"Saving session to {info.FullName} ...", MessagePresenter.MessageType.Normal);

            // create save-state
            var session = Session.CreateEmpty();
            var sortedCells = cells
                .OrderByDescending(c => (int) c.Container * 16384 + c.transform.GetSiblingIndex())
                .ToList();
            foreach (var cell in sortedCells)
            {
                session.X.Add(cell.transform.position.x);
                session.Y.Add(cell.transform.position.y);
                //session.Path.Add(cell.GraphicSource);
                session.LocalAttachmentUri.Add(cell.LocalAttachmentUri);
                session.SourceAttachmentUri.Add(cell.SourceAttachmentUri);
                session.Text.Add(cell.Text);
                session.Flag.Add(cell.Flag);
                session.Rating.Add(cell.Rating);
                session.Checked.Add(cell.Checked);
                session.Favorite.Add(cell.Favorite);
                session.Tags.Add(cell.Tags);
                session.LicenseInfo.Add(cell.LicenseInfo);
                session.Container.Add((int) cell.Container);
                session.Reverse.Add(cell.Reverse);
                session.Reversed.Add(cell.Reversed);
            }

            CollectSessionAttachments(file, session);

            // update cell data to match new collected locations
            for (int i = 0; i < sortedCells.Count; i++)
                sortedCells[i].LocalAttachmentUri = session.LocalAttachmentUri[i];

            var jsonSaveState = JsonUtility.ToJson(session);
            if (file.Exists)
                file.Delete();
            try

            {
                File.WriteAllText(file.FullName, jsonSaveState);
            }
            catch (Exception e)
            {
                message.Log($"{e.Message}", MessagePresenter.MessageType.Warning);
                return false;
            }

            Debug.Log($"Session with {session.Container.Count} cards saved to {file.FullName}\n{jsonSaveState}");
            if (!silent) message.Log($"Session saved to {file.FullName}", MessagePresenter.MessageType.Normal);
            return true;
        }

        /// <summary>Copies all referenced attachments into a folder next to the session save path.</summary>
        /// <remarks>The passed in <paramref name="session"/> will be updated with new asset paths reflecting the collected location.</remarks>
        private void CollectSessionAttachments([NotNull] FileInfo saveFile, [NotNull] Session session)
        {
            Debug.Log($"Collecting session attachments for '{saveFile}'.");
            var collectPath = GetCollectionPathFor(saveFile.FullName);
            DirectoryInfo collectDir = null;
            if (!Directory.Exists(collectPath))
            {
                try
                {
                    collectDir = Directory.CreateDirectory(collectPath);
                }
                catch (Exception e)
                {
                    message.Log($"{e.Message}", MessagePresenter.MessageType.Warning);
                    message.Log($"Collecting failed. Couldn't create {collectPath}",
                        MessagePresenter.MessageType.Warning);
                    return;
                }
            }
            else
            {
                var files = Directory.EnumerateFiles(collectPath);
                foreach (var file in files)
                {
                    File.Delete(file);
                }
            }

            /*
            // report readonly and abort
            if (collectDir == null || (collectDir.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
            {
                message.Log($"Collecting failed. Couldn't write to {collectPath}",
                    MessagePresenter.MessageType.Warning);
                //return;
            }
            */

            //collectionDir.Delete(true);

            // copy/move files to collect path and update session accordingly
            var cellCount = session.LocalAttachmentUri.Count;
            session.CollectionPath = collectPath;
            try
            {
                for (var i = 0; i < cellCount; i++)
                {
                    // cell has no local attachment                    
                    if (string.IsNullOrEmpty(session.LocalAttachmentUri[i]))
                        continue;
                    var attachmentSource = new FileInfo(session.LocalAttachmentUri[i]);
                    if (!attachmentSource.Exists)
                        continue;

                    // Copy only files that do not yet exist in the collection.
                    // Name-duplicates will NOT be overwritten.
                    var attachmentDest = new FileInfo(
                        Path.GetFullPath(session.CollectionPath + Path.DirectorySeparatorChar + attachmentSource.Name));
                    if (attachmentDest.Exists)
                    {
                        Debug.Log($"Skipping {attachmentSource}");
                        continue;
                    }

                    // move files if they are temporary (e.g. downloaded pictures that never were collected before)
                    // copy all other files
                    if (attachmentSource.DirectoryName == UnsavedCollectionPath)
                    {
                        if (attachmentSource != attachmentDest && attachmentDest.Exists)
                            attachmentDest.Delete();
                        attachmentSource.MoveTo(attachmentDest.FullName);
                        Debug.Log($"Moving {attachmentSource} to {attachmentDest}");
                    }
                    else
                    {
                        attachmentSource.CopyTo(attachmentDest.FullName, true);
                        Debug.Log($"Copying {attachmentSource} to {attachmentDest}");
                    }

                    // update session data with new location
                    session.LocalAttachmentUri[i] = attachmentDest.FullName;
                }
            }
            catch (Exception e)
            {
                message.Log($"Error while collecting attachments: {e.Message}.", MessagePresenter.MessageType.Warning);
                Debug.LogError(e);
            }
        }

        #endregion
    }
}