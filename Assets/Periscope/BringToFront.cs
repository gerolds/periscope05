﻿using UnityEngine;

namespace Periscope
{
    public class BringToFront : MonoBehaviour
    {
        private void OnEnable()
        {
            transform.SetAsLastSibling();
        }
    }
}
