﻿using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Periscope
{
	public class TagButton : MonoBehaviour
	{

		 public Button highlight = null;
		 public Button delete = null;
		 public TextMeshProUGUI label = null;
		
	}
}
