using UnityEngine;

namespace Periscope
{
    public interface ICellDrop
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="targetPos">pos after drop</param>
        void Drop(Cell cell, Vector2 targetPos);
    }
}