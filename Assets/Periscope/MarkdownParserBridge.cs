﻿using Libs.CommonMark;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace Periscope
{
    [RequireComponent(typeof(TMP_InputField))]
    public class MarkdownParserBridge : MonoBehaviour
    {
         [SerializeField]
        private TMP_InputField inputField;

        public TMP_InputField InputField
        {
            get
            {
                if (inputField == null) inputField = GetComponent<TMP_InputField>();
                return inputField;
            }
        }

        public void MarkDown()
        {
            inputField.text = CommonMarkConverter.Convert(inputField.text); 
        }
    }
}
