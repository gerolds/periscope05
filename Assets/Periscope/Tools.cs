﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Periscope
{
    public class Tools : MonoBehaviour
    {
         public Toggle edit = null;
         public Toggle draw = null;
        public Toggle Default = null;

        // TODO: Unhook this from the update loop?
        private void Update()
        {
        
            switch (InputRouter.Mode)
            {
                case InputMode.TextEdit:
                    edit.SetIsOnWithoutNotify(true);
                    break;
                case InputMode.Drawing:
                    draw.SetIsOnWithoutNotify(true);
                    break;
                case InputMode.Default:
                    Default.SetIsOnWithoutNotify(true);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
