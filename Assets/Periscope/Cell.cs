﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Periscope
{
    public class CellChangedEvent : UnityEvent<Cell>
    {
    }

    public class Cell : MonoBehaviour
    {
        [SerializeField] public CellChangedEvent changed;
        [SerializeField] private Sprite graphicSprite = null;
        [SerializeField] private Vector2 position = Vector2.zero;
        [SerializeField] private string text = "";
        [SerializeField] private string reverseText = "";
        [SerializeField] private bool reversed = false;
        [SerializeField] private bool confirmed = false;
        [SerializeField] private bool flag = false;
        [SerializeField] private bool favorite = false;
        [SerializeField] private int rating = 0;
        [SerializeField] private int indexColor = 0;
        [SerializeField] private string tags = "No Tags";
        [SerializeField] private string graphicSource = "";
        [SerializeField] private ContainerId container = ContainerId.Invalid;
        [SerializeField] private string localAttachmentUri;
        [SerializeField] private string sourceAttachmentUri;
        [SerializeField] private string licenseInfo;

        public string LicenseInfo
        {
            get => licenseInfo;
            set
            {
                if (value == null)
                    return;
                if (value.Equals(licenseInfo))
                    return;
                licenseInfo = value;
                changed?.Invoke(this);
            }
        }

        public Vector2 Position
        {
            get => position;
            set
            {
                if (position == value) return;
                position = value;
                changed?.Invoke(this);
            }
        }

        public ContainerId Container
        {
            get => container;
            set
            {
                if (container == value) return;
                container = value;
                changed?.Invoke(this);
            }
        }

        public string GraphicSource
        {
            get => graphicSource;
            set
            {
                if (graphicSource == value) return;

                graphicSource = value;
                changed?.Invoke(this);
            }
        }

        public Sprite GraphicSprite
        {
            get => graphicSprite;
            set
            {
                if (graphicSprite == value) return;
                graphicSprite = value;
                changed?.Invoke(this);
            }
        }

        //private static MarkovWordGenerator _generator;

        public string Tags
        {
            get => tags;
            set
            {
                if (tags == value) return;

                tags = value;
                changed?.Invoke(this);
            }
        }

        public int Rating
        {
            get => rating;
            set
            {
                if (rating == value) return;
                rating = value;
                changed?.Invoke(this);
            }
        }

        public int Color
        {
            get => indexColor;
            set
            {
                if (indexColor == value) return;
                indexColor = value;
                changed?.Invoke(this);
            }
        }

        public bool Favorite
        {
            get => favorite;
            set
            {
                if (favorite == value) return;

                favorite = value;
                changed?.Invoke(this);
            }
        }

        public bool Flag
        {
            get => flag;
            set
            {
                if (flag == value) return;

                flag = value;
                changed?.Invoke(this);
            }
        }

        public bool Checked
        {
            get => confirmed;
            set
            {
                if (confirmed == value) return;

                confirmed = value;
                changed?.Invoke(this);
            }
        }

        public string Text
        {
            get => text;
            set
            {
                if (text == value) return;

                text = value;
                changed?.Invoke(this);
            }
        }

        public string Reverse
        {
            get => reverseText;
            set
            {
                if (reverseText == value) return;

                reverseText = value;
                changed?.Invoke(this);
            }
        }

        public bool Reversed
        {
            get => reversed;
            set
            {
                if (reversed == value) return;

                reversed = value;
                changed?.Invoke(this);
            }
        }


        public float BoxSize => GetComponent<RectTransform>().rect.height;

        public string SourceAttachmentUri
        {
            get => sourceAttachmentUri;
            set
            {
                if (sourceAttachmentUri == value) return;
                sourceAttachmentUri = value;
                changed?.Invoke(this);
            }
        }

        public string LocalAttachmentUri
        {
            get => localAttachmentUri;
            set
            {
                if (localAttachmentUri == value) return;
                localAttachmentUri = value;
                changed?.Invoke(this);
            }
        }


        private void Awake()
        {
            changed = new CellChangedEvent();
            if (!Periscope.Singleton.cells.Contains(this))
                Periscope.Singleton.cells.Add(this);
        }

        private void OnDestroy()
        {
            Periscope.Singleton.cells.Remove(this);
            Debug.Log("Cell destroyed");
        }

        private void Start()
        {
            Debug.Assert(changed != null);
            changed?.Invoke(this);
            changed.AddListener(_ => Periscope.Singleton.IsSaved = false);
            changed.AddListener(_ => Periscope.Singleton.SaveRecoveryFile());
            GetComponent<CellSelectHandler>().enabled = true;
        }

        /*public void CreateDummyData()
        {
            // Create dummy data
            Text = "";
            if (Random.Range(0, 5) <= 1)
            {
                //var next = _generator.NextWord.ToLower();
                //Text = char.ToUpper(next[0]) + next.Substring(1);
                var strBuilder = new StringBuilder("");
                var len = Mathf.FloorToInt(Mathf.Pow(Random.Range(1.1f, 2f), 4f));
                for (int i = 0; i < len; i++)
                {
                    strBuilder.Append(_generator.NextWord + " ");
                    var rng = Random.Range(0, 100);
                    if (rng < 10)
                    {
                        strBuilder.Append(". ");
                        continue;
                    }

                    if (rng < 12)
                    {
                        strBuilder.Append("? ");
                        continue;
                    }

                    if (rng < 14)
                    {
                        strBuilder.Append("! ");
                        continue;
                    }

                    if (rng < 20)
                    {
                        strBuilder.Append(", ");
                        continue;
                    }
                }

                strBuilder.Append(".");
                Text = strBuilder.ToString().ToLower();
                Text = Regex.Replace(Text, @"\s+([.,-?!:;]+)", "$1");
                Text = char.ToUpper(Text[0]) + Text.Substring(1);
            }

            //Note = "No note";

            var Rating = Random.Range(0, 10) < 3 ? -1 : 0; // 20.0% get rejected
            if (Rating == 0)
                Rating = Random.Range(0, 10) < 3 ? 1 : Rating; // ~20.0% get 1* 
            if (Rating == 1)
                Rating = Random.Range(0, 10) < 5 ? 2 : Rating; // ~10.0% get 2*
            if (Rating == 2)
                Rating = Random.Range(0, 10) < 4 ? 3 : Rating; // ~3.0% get 3*
            if (Rating == 3)
                Rating = Random.Range(0, 10) < 5 ? 4 : Rating; // ~1.5% get 4*
            if (Rating == 4)
                Rating = Random.Range(0, 10) < 7 ? 5 : Rating; // ~1% get 5*

            Favorite = Random.Range(0, 10) < 2;
            Flag = Random.Range(0, 10) < 2;
            Checked = Random.Range(0, 10) < 2;
            //var dummyGraphicsCount = Periscope.Singleton.DummyGraphics.Length;
            //var randomGraphicIndex = Random.Range(0, dummyGraphicsCount);
            //GraphicSprite = Periscope.Singleton.DummyGraphics[randomGraphicIndex];

            changed?.Invoke(this);
        }

        private static readonly string[] WordSamples =
        {
            "call", "me", "ishmael", "some", "years", "ago", "never", "mind", "how",
            "long", "precisely", "having", "little", "or", "no", "money", "in", "my", "purse",
            "and", "nothing", "particular", "to", "interest", "me", "on", "shore", "i", "thought",
            "i", "would", "sail", "about", "a", "little", "and", "see", "the", "watery", "part", "of", "the",
            "world", "it", "is", "a", "way", "i", "have", "of", "driving", "off", "the", "spleen", "and",
            "regulating", "the", "circulation", "whenever", "i", "find", "myself",
            "growing", "grim", "about", "the", "mouth", "whenever", "it", "is", "a", "damp",
            "drizzly", "november", "in", "my", "soul", "whenever", "i", "find", "myself",
            "involuntarily", "pausing", "before", "coffin", "warehouses", "and", "bring",
            "ing", "up", "the", "rear", "of", "every", "funeral", "i", "meet", "and", "especially",
            "whenever", "my", "hypos", "get", "such", "an", "upper", "hand", "of", "me", "that",
            "it", "requires", "a", "strong", "moral", "principle", "to", "prevent", "me", "from",
            "deliberately", "stepping", "into", "the", "street", "and", "methodically",
            "knocking", "people's", "hats", "off", "then", "i", "account", "it", "high", "time",
            "to", "get", "to", "sea", "as", "soon", "as", "i", "can", "this", "is", "my", "substitute", "for",
            "pistol", "and", "ball", "with", "a", "philosophical", "flourish", "cato", "throws",
            "himself", "upon", "his", "sword", "i", "quietly", "take", "to", "the", "ship",
            "there", "is", "nothing", "surprising", "in", "this", "if", "they", "but", "knew",
            "it", "almost", "all", "men", "in", "their", "degree", "some", "time", "or", "other",
            "cherish", "very", "nearly", "the", "same", "feelings", "toward", "the", "ocean",
            "with", "me", "there", "now", "is", "your", "insular", "city", "of", "the", "manhattoes",
            "belted", "round", "by", "wharves", "as", "indian", "isles", "by", "coral", "reefs",
            "commerce", "surrounds", "it", "with", "her", "surf", "right", "and", "left", "the",
            "streets", "take", "you", "waterward", "its", "extreme", "down", "-town", "is", "the",
            "battery", "where", "that", "noble", "mole", "is", "washed", "by", "waves", "and",
            "cooled", "by", "breezes", "which", "a", "few", "hours", "previous", "were", "out", "of",
            "sight", "of", "land", "look", "at", "the", "crowds", "of", "water", "-gazers", "there",
            "circuambulate", "the", "city", "of", "a", "dreamy", "sabbath", "after-",
            "noon", "go", "from", "corlears", "hook", "to", "coenties", "slip", "and",
            "from", "thence", "by", "whitehall", "northward", "what", "do", "you",
            "see", "posted", "like", "silent", "sentinels", "all", "around", "the", "town",
            "stand", "thousands", "upon", "thousands", "of", "mortal", "men", "fixed",
            "in", "ocean", "reveries", "some", "leaning", "against", "the", "spiles",
            "some", "seated", "upon", "the", "pier-heads", "some", "looking", "over",
            "vhe", "bulwarks", "of", "ships", "from", "china", "some", "high", "aloft", "in",
            "the", "rigging", "as", "if", "striving", "to", "get", "a", "still", "better", "seaward",
            "peep", "but", "these", "are", "all", "landsmen", "of", "week", "days", "pent",
            "up", "in", "lath", "and", "plaster", "tied", "to", "counters", "nailed", "to", "benches",
            "clinched", "to", "desks", "how", "then", "is", "this", "are", "the", "green",
            "fields", "gone", "what", "do", "they", "here",
            "but", "look", "here", "come", "more", "crowds", "pacing", "straight", "for",
            "the", "water", "and", "seemingly", "bound", "for", "a", "dive", "strange",
            "nothing", "will", "content", "them", "but", "the", "extremest", "limit", "of", "the",
            "land", "loitering", "under", "the", "shady", "lee", "of", "yonder", "warehouses",
            "will", "not", "suffice", "no", "they", "must", "get", "just", "as", "nigh", "the",
            "water", "as", "they", "possibly", "can", "without", "falling", "in", "and", "there",
            "they", "stand", "miles", "of", "them", "leagues", "inlanders", "all", "they",
            "come", "from", "lanes", "and", "alleys", "streets", "and", "avenues", "north",
            "east", "south", "and", "west", "yet", "here", "they", "all", "unite", "tell", "me",
            "does", "the", "magnetic", "virtue", "of", "the", "needles", "of", "the", "compasses",
            "of", "all", "those", "ships", "attract", "them", "thither",
            "once", "more", "say", "you", "are", "in", "the", "country", "in", "some",
            "high", "land", "of", "lakes", "take", "almost", "any", "path", "you", "please",
            "and", "ten", "to", "one", "it", "carries", "you", "down", "in", "a", "dale", "and", "leaves",
            "you", "there", "by", "a", "pool", "in", "the", "stream", "there", "is", "magic", "in", "it",
            "let", "the", "most", "absent-minded", "of", "men", "be", "plunged", "in", "his",
            "deepest", "reveries", "stand", "that", "man", "on", "his", "legs", "set", "his", "feet",
            "a-going", "and", "he", "will", "infallibly", "lead", "you", "to", "water", "if", "water",
            "there", "be", "in", "all", "that", "region", "should", "you", "ever", "be", "athirst",
            "in", "the", "great", "american", "desert", "try", "this", "experiment", "if", "your"
        };*/

        private static string RemoveTag(string tagString, string tagToRemove)
        {
            if (string.IsNullOrEmpty(tagString))
                return tagString;
            if (string.IsNullOrEmpty(tagToRemove))
                return tagString;
            var tags = new HashSet<string>(tagString.Split(','));
            tags.Remove(tagToRemove);
            return string.Join(",", tags);
        }

        private static string AddTag([NotNull] string tagString, [NotNull] string tagToAdd)
        {
            if (string.IsNullOrEmpty(tagToAdd))
                throw new Exception("Attempt to add empty tag");
            var tags = new HashSet<string>(tagString.Split(','));
            tags.Add(tagToAdd);
            return string.Join(",", tags);
        }

        private static bool HasTag([NotNull] string tagString, [NotNull] string searchTag)
        {
            if (string.IsNullOrEmpty(tagString))
                return false;
            if (string.IsNullOrEmpty(searchTag))
                return false;
            return tagString.Contains(searchTag);
        }

        public void RemoveTag([NotNull] string t)
        {
            Tags = RemoveTag(Tags, t);
        }

        public void AddTag([NotNull] string t)
        {
            Tags = AddTag(Tags, t);
        }

        public bool HasTag([NotNull] string t)
        {
            return HasTag(Tags, t);
        }

        public void Delete()
        {
            Destroy(gameObject);
        }

        public void AddAttachment(Uri uri)
        {
            //LocalAttachmentUri = uri.LocalPath;
            //SourceAttachmentUri = uri.LocalPath;
            var rt = Periscope.Singleton.ImportAttachmentIntoCellRt(uri.AbsoluteUri, this);
            StartCoroutine(rt);
        }

        public void RemoveAttachment()
        {
            StopAllCoroutines();
            Periscope.Singleton.DeleteUniqueAttachment(this);
            GraphicSprite = null;
            LocalAttachmentUri = "";
            SourceAttachmentUri = "";
            LicenseInfo = "";
        }
    }
}