﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Periscope
{
    public class StackFlow : MonoBehaviour, IScrollHandler
    {
        [SerializeField] private Transform container = null;

        //private int _currentIndex = 0;
        private RectTransform _rect = null;
        private Transform _previous = null;

        private int _slidingItemsCount;

        RectTransform Rect
        {
            get
            {
                if (_rect == null)
                    _rect = GetComponent<RectTransform>();
                return _rect;
            }
        }

        public void OnScroll(PointerEventData eventData)
        {
            //Debug.Log(eventData.scrollDelta);
            var itemCount = container.childCount;
            if (itemCount < 2)
                return;

            const float slideDuration = 0.25f;

            if (eventData.scrollDelta.y > 0.1f || eventData.scrollDelta.y < -0.1f)
            {
                //Debug.Log(eventData.scrollDelta.y);
                var pushedSibling = container.GetChild(itemCount - 1);
                if (eventData.scrollDelta.y > 0)
                {
                    // scrolling down
                    var firstSibling = container.GetChild(0);
                    if (_previous == firstSibling)
                    {
                        firstSibling.DOComplete(true);
                        firstSibling = container.GetChild(1);
                    }

                    void SlideIn()
                    {
                        firstSibling.SetAsLastSibling();
                        firstSibling.DOMove(Rect.position, slideDuration);
                        if (_previous == firstSibling)
                            _previous = null;
                    }

                    firstSibling
                        .DOLocalMove(Vector3.right * Rect.sizeDelta.x * 0.9f, slideDuration)
                        .OnComplete(SlideIn);

                    _previous = firstSibling;
                }
                else
                {
                    // scrolling down
                    var lastSibling = container.GetChild(itemCount - 1);
                    if (_previous == lastSibling)
                    {
                        lastSibling.DOComplete(true);
                        lastSibling = container.GetChild(itemCount - 2);
                    }

                    void SlideIn()
                    {
                        lastSibling.SetAsFirstSibling();
                        lastSibling.DOMove(Rect.position, slideDuration);
                        if (_previous == lastSibling)
                            _previous = null;
                    }

                    lastSibling
                        .DOLocalMove(Vector3.right * Rect.sizeDelta.x * 0.9f, slideDuration)
                        .OnComplete(SlideIn);

                    _previous = lastSibling;
                }
            }
        }
    }
}