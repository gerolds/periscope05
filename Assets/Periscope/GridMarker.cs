﻿using System;
using JetBrains.Annotations;
using UnityEngine;

namespace Periscope
{
    [Obsolete]
    public class GridMarker : MonoBehaviour
    {
        [NotNull]
        public GameObject RingMarker;
        public GameObject BoxMarker;
        public GameObject CircleMarker;


        private void Awake()
        {
            Debug.Assert(RingMarker != null, "Assigned in editor");
            Debug.Assert(BoxMarker != null, "Assigned in editor");
            Debug.Assert(CircleMarker != null, "Assigned in editor");
        }

        public void MarkCircle(Vector2 pos)
        {
            CircleMarker.transform.localPosition = new Vector3(pos.x, pos.y, 0);
            CircleMarker.SetActive(true);
        }

        public void ClearCircle()
        {
            CircleMarker.SetActive(false);
        }
    
        public void MarkBox(Vector2 pos)
        {
            BoxMarker.transform.localPosition = new Vector3(pos.x, pos.y, 0);
            BoxMarker.SetActive(true);
        }

        public void ClearBox()
        {
            BoxMarker.SetActive(false);
        }
    
        public void MarkRing(Vector2 pos)
        {
            RingMarker.transform.localPosition = new Vector3(pos.x, pos.y, 0);
            RingMarker.SetActive(true);
        }

        public void ClearRing()
        {
            RingMarker.SetActive(false);
        }
    }
}
