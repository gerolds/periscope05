using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Periscope
{
    [AddComponentMenu("Layout/Flow Layout Group", 153)]
    public class FlowLayoutGroup : LayoutGroup
    {
        public enum Corner { 
            UpperLeft = 0, 
            UpperRight = 1, 
            LowerLeft = 2, 
            LowerRight = 3 
        }

        public enum Constraint { 
            Flexible = 0, 
            FixedColumnCount = 1, 
            FixedRowCount = 2 
        }

        protected Vector2 _cellSize = new Vector2(100, 100);
        public Vector2 CellSize { 
            get { return _cellSize; } 
            set { SetProperty(ref _cellSize, value); } 
        }

         [SerializeField] protected Vector2 _spacing = Vector2.zero;
        public Vector2 Spacing { 
            get { return _spacing; } 
            set { SetProperty(ref _spacing, value); } 
        }

        protected FlowLayoutGroup()
        {}

#if UNITY_EDITOR
        protected override void OnValidate()
        {
            base.OnValidate();
        }

#endif

        public override void CalculateLayoutInputHorizontal()
        {
            base.CalculateLayoutInputHorizontal();

            int minColumns = 0;
            int preferredColumns = 0;

            minColumns = 1;
            preferredColumns = Mathf.CeilToInt(Mathf.Sqrt(rectChildren.Count));

            SetLayoutInputForAxis(
                padding.horizontal + (CellSize.x + Spacing.x) * minColumns - Spacing.x,
                padding.horizontal + (CellSize.x + Spacing.x) * preferredColumns - Spacing.x,
                -1, 0);
        }

        public override void CalculateLayoutInputVertical()
        {
            int minRows = 0;

            float width = rectTransform.rect.size.x;
            int cellCountX = Mathf.Max(1, Mathf.FloorToInt((width - padding.horizontal + Spacing.x + 0.001f) / (CellSize.x + Spacing.x)));
//      minRows = Mathf.CeilToInt(rectChildren.Count / (float)cellCountX);
            minRows = 1;
            float minSpace = padding.vertical + (CellSize.y + Spacing.y) * minRows - Spacing.y;
            SetLayoutInputForAxis(minSpace, minSpace, -1, 1);
        }

        public override void SetLayoutHorizontal()
        {
            SetCellsAlongAxis(0);
        }

        public override void SetLayoutVertical()
        {
            SetCellsAlongAxis(1);
        }

        int _cellsPerMainAxis, _actualCellCountX, _actualCellCountY;
        int _positionX;
        int _positionY;
        float _totalWidth = 0; 
        float _totalHeight = 0;

        float _lastMaxHeight = 0;

        private void SetCellsAlongAxis(int axis)
        {
            // Normally a Layout Controller should only set horizontal values when invoked for the horizontal axis
            // and only vertical values when invoked for the vertical axis.
            // However, in this case we set both the horizontal and vertical position when invoked for the vertical axis.
            // Since we only set the horizontal position and not the size, it shouldn't affect children's layout,
            // and thus shouldn't break the rule that all horizontal layout must be calculated before all vertical layout.

            float width = rectTransform.rect.size.x;
            float height = rectTransform.rect.size.y;

            int cellCountX = 1;
            int cellCountY = 1;

            if (CellSize.x + Spacing.x <= 0)
                cellCountX = int.MaxValue;
            else
                cellCountX = Mathf.Max(1, Mathf.FloorToInt((width - padding.horizontal + Spacing.x + 0.001f) / (CellSize.x + Spacing.x)));

            if (CellSize.y + Spacing.y <= 0)
                cellCountY = int.MaxValue;
            else
                cellCountY = Mathf.Max(1, Mathf.FloorToInt((height - padding.vertical + Spacing.y + 0.001f) / (CellSize.y + Spacing.y)));

            _cellsPerMainAxis = cellCountX;
            _actualCellCountX = Mathf.Clamp(cellCountX, 1, rectChildren.Count);
            _actualCellCountY = Mathf.Clamp(cellCountY, 1, Mathf.CeilToInt(rectChildren.Count / (float)_cellsPerMainAxis));

            Vector2 requiredSpace = new Vector2(
                _actualCellCountX * CellSize.x + (_actualCellCountX - 1) * Spacing.x,
                _actualCellCountY * CellSize.y + (_actualCellCountY - 1) * Spacing.y
            );
            Vector2 startOffset = new Vector2(
                GetStartOffset(0, requiredSpace.x),
                GetStartOffset(1, requiredSpace.y)
            );

            _totalWidth = 0;
            _totalHeight = 0;
            Vector2 currentSpacing = Vector2.zero;
            for (int i = 0; i < rectChildren.Count; i++)
            {
                SetChildAlongAxis(rectChildren[i], 0, startOffset.x + _totalWidth /*+ currentSpacing[0]*/, rectChildren[i].rect.size.x);
                SetChildAlongAxis(rectChildren[i], 1, startOffset.y + _totalHeight  /*+ currentSpacing[1]*/, rectChildren[i].rect.size.y);

                currentSpacing = Spacing;

                _totalWidth += rectChildren[i].rect.width + currentSpacing[0];

                if (rectChildren[i].rect.height > _lastMaxHeight)
                {
                    _lastMaxHeight = rectChildren[i].rect.height;
                }

                if (i<rectChildren.Count-1)
                {
                    if (_totalWidth + rectChildren[i+1].rect.width + currentSpacing[0] > width )
                    {
                        _totalWidth = 0;
                        _totalHeight += _lastMaxHeight + currentSpacing[1];
                        _lastMaxHeight = 0;
                    }
                }
            }
        }
    }
}
