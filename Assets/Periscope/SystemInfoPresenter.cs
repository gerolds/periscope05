﻿using TMPro;
using UnityEngine;

namespace Periscope
{
    public class SystemInfoPresenter : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI textTarget;

        // Update is called once per frame
        void Update()
        {
            // TODO: Unhook this from the update loop?
            var allocated = System.GC.GetTotalMemory(false) / 1048576;
            var allocatedPct = (float)allocated / (float)SystemInfo.systemMemorySize;
            if (Time.frameCount % 5 == 0)
                textTarget.text = $"Cores: {SystemInfo.processorCount}, RAM: {allocatedPct:P0} used ({allocated} MB), VRAM: {SystemInfo.graphicsMemorySize} MB";
        }
    }
}
