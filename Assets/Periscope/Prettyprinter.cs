using System;
using System.Linq;
using System.Text;
using System.Xml.Schema;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Periscope
{
    public class Prettyprinter : MonoBehaviour
    {
        public static StringBuilder strBuilder = new StringBuilder();
        private static readonly string NodePrefix = "node";
        private const int MaxLen = 32;

        [SerializeField] [TextArea] public string output;
        [SerializeField] public Transform root;
        [SerializeField] private bool plantUML = true;

        private static readonly string htmlTABLE =
            "<TABLE BORDER=\"0\" CELLBORDER=\"1\" CELLSPACING=\"0\" CELLPADDING=\"4\">{0}</TABLE>";

        private static readonly string htmlTH =
            "<TD ALIGN=\"left\" PORT=\"{1}\"><FONT FACE=\"MaisonNeue-Bold\">{0}</FONT></TD>";

        private static readonly string htmlTD =
            "<TD ALIGN=\"left\" PORT=\"{1}\">{0}</TD>";

        private static readonly string htmlTR =
            "<TR>{0}</TR>";

        public void Walk()
        {
            Debug.Log("WALK");
            strBuilder = new StringBuilder();
            if (!root)
            {
                var rootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();
                foreach (var rootObj in rootGameObjects)
                {
                    Debug.Assert(rootObj.transform);
                    Walker(rootObj.transform, ref strBuilder);
                }
            }
            else
            {
                Walker(root, ref strBuilder);
            }

            output = $"digraph {SceneManager.GetActiveScene().name.AlphaNumOnly()} " +
                     "{\n" +
                     $"# {DateTime.UtcNow}\n" +
                     "rankdir=LR\n" +
                     "node [shape=record,fontname=\"MaisonNeue-Book\",penwidth=1]\n" +
                     "graph [splines=ortho,nodesep=0.5]\n" +
                     $"{strBuilder}\n" +
                     "}\n";

            if (plantUML)
                output = $"@startdot\n{output}@enddot\n";
        }

        public void WalkPlain()
        {
            Debug.Log("WALK PLAIN");
            strBuilder = new StringBuilder();
            if (!root)
            {
                var rootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();
                foreach (var rootObj in rootGameObjects)
                {
                    Debug.Assert(rootObj.transform);
                    WalkerPlain(rootObj.transform, ref strBuilder);
                }
            }
            else
            {
                WalkerPlain(root, ref strBuilder);
            }

            output = $"digraph {SceneManager.GetActiveScene().name.AlphaNumOnly()} " +
                     "{\n" +
                     $"# {DateTime.UtcNow}\n" +
                     "rankdir=LR\n" +
                     "node [shape=record,fontname=\"MaisonNeue-Book\",penwidth=1]\n" +
                     "graph [splines=ortho,nodesep=0.5]\n" +
                     $"{strBuilder}\n" +
                     "}\n";

            if (plantUML)
                output = $"@startdot\n{output}@enddot\n";
        }


        public void WalkHtml()
        {
            Debug.Log("WALK HTML");

            strBuilder = new StringBuilder();
            if (!root)
            {
                var rootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();
                foreach (var rootObj in rootGameObjects)
                {
                    Debug.Assert(rootObj.transform);
                    WalkerHtml(rootObj.transform, ref strBuilder);
                }
            }
            else
            {
                WalkerHtml(root, ref strBuilder);
            }

            output = $"digraph {SceneManager.GetActiveScene().name.AlphaNumOnly()} " +
                     "{\n" +
                     $"# {DateTime.UtcNow}\n" +
                     "rankdir=LR\n" +
                     "node [shape=plaintext,fontname=\"MaisonNeue-Book\",penwidth=1]\n" +
                     "graph [splines=ortho,nodesep=0.5]\n" +
                     $"{strBuilder}\n" +
                     "}\n";

            if (plantUML)
                output = $"@startdot\n{output}@enddot\n";
        }


        public static int Depth(Transform t)
        {
            if (t.root == t)
                return 0;
            return Depth(t.parent) + 1;
        }

        public static void WalkerPlain([NotNull] Transform target, ref StringBuilder builder)
        {
            // add components on this target
            var components = target.GetComponents(typeof(Component));
            var depth = Depth(target);
            var dSpacer = new String(' ', depth * 4);
            var compNames = components.Select(c => $": {c.GetType().Name.Truncate(MaxLen, true)}");
            var structLayers = string.Join("\n", compNames);
            var structLabel = $"{target.name.Truncate(MaxLen, true)}: {target.gameObject.GetType().Name}";
            var structJoined =
                $"{NodePrefix}{target.GetInstanceID().ToUInt32()} [label = \"{structLabel}\n{structLayers}\"]";//
            builder.Append($"{structJoined}\n");

            // add edge from parent
            if (target.parent != null)
            {
                Debug.Assert(builder != null);
                builder.Append(
                    $"{NodePrefix}{target.parent.GetInstanceID().ToUInt32()} -> {NodePrefix}{target.GetInstanceID().ToUInt32()}\n");
            }

            // visit children
            foreach (Transform child in target)
                WalkerPlain(child, ref builder);
        }

        public static void Walker([NotNull] Transform target, ref StringBuilder builder)
        {
            // add components on this target
            var components = target.GetComponents(typeof(Component));
            var depth = Depth(target);
            var dSpacer = new String(' ', depth * 4);
            var compNames = components.Select(c => $"{c.GetType().FullName.Truncate(MaxLen, true)}");
            var structLayers = string.Join("|", compNames);
            var structLabel = target.name.Truncate(MaxLen, true);
            var structJoined =
                $"{NodePrefix}{target.GetInstanceID().ToUInt32()} [label = \"{{{{{structLabel}}}|{{{structLayers}}}}}\"]";
            builder.Append($"{structJoined}\n");

            // add edge from parent
            if (target.parent != null)
            {
                Debug.Assert(builder != null);
                builder.Append(
                    $"{NodePrefix}{target.parent.GetInstanceID().ToUInt32()} -> {NodePrefix}{target.GetInstanceID().ToUInt32()}\n");
            }

            // visit children
            foreach (Transform child in target)
                Walker(child, ref builder);
        }

        public static void WalkerHtml([NotNull] Transform target, ref StringBuilder builder)
        {
            // add components on this target
            var components = target.GetComponents(typeof(Component));
            var depth = Depth(target);
            var layerCells = components.Select(component => string.Format(htmlTD,
                component.GetType().FullName.Truncate(MaxLen, true),
                $"p{component.GetInstanceID().ToUInt32()}"
            ));
            var layerRows = layerCells.Select(td => string.Format(htmlTR, td));
            var header = string.Format(htmlTR, string.Format(htmlTH, target.name.Truncate(MaxLen, true), "p0"));
            var tableRows = header + string.Join("", layerRows);
            var table = string.Format(htmlTABLE, tableRows);
            var structJoined =
                $"{NodePrefix}{target.GetInstanceID().ToUInt32()} [label = <{table}>]";
            builder.Append($"{structJoined}\n");

            // add edge from parent
            if (target.parent != null)
            {
                Debug.Assert(builder != null);
                builder.Append(
                    $"{NodePrefix}{target.parent.GetInstanceID().ToUInt32()} -> {NodePrefix}{target.GetInstanceID().ToUInt32()}\n");
            }

            // visit children
            foreach (Transform child in target)
                WalkerHtml(child, ref builder);
        }
    }
}