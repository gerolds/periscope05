﻿using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;

namespace Periscope
{
    public class CellHotkeyHandler : MonoBehaviour
    {
        [FormerlySerializedAs("droppableObjectRoot")] [SerializeField] private Cell cell = null;

        [FormerlySerializedAs("selectHandler")] [SerializeField]
        private CellSelectHandler cellSelectHandler = null;

        //[FormerlySerializedAs("cellData")] [SerializeField]
        //public Cell cell;

        private void Awake()
        {
            //Debug.Assert(cellComponent != null, $"Trigger requires a {nameof(Cell)} reference.");
            Debug.Assert(cellSelectHandler != null, $"Trigger requires a {nameof(CellSelectHandler)} reference.");
            Debug.Assert(cell != null, $"Trigger requires a {nameof(Cell)} reference.");
        }

        // Update is called once per frame
        void Update()
        {
            // TODO: Unhook this from the update loop?

            
            if (!InputRouter.AnyKey)
                return;

            ObserveSendToStack();
            ObserveMoveToTrash();
            ObserveCancel();
            ObserveDeleteCell();
            ObserveNavigation();
        }

        private static void ObserveNavigation()
        {
            if (InputRouter.CmdHorizontal || InputRouter.CmdVertical)
            {
                // key navigation should always respond immediately without selecting a cell first
                var lastMarked = CellSelectHandler.MarkedCells.LastOrDefault();
                if (lastMarked != null)
                    lastMarked.Select();
            }
        }



        private void ObserveCancel()
        {
            if (InputRouter.CmdCancel)
            {
                cellSelectHandler.Deselect();
            }
        }

        private void ObserveMoveToTrash()
        {
            if (InputRouter.CmdDeleteCell)
            {
                var isMarked = CellSelectHandler.MarkedCells.Any(i => i == cellSelectHandler);

                if (isMarked)
                    Periscope.Singleton.GetContainerDropHandler(ContainerId.Trash).Drop(cell, Vector2.zero);
            }
        }
        
        private void ObserveDeleteCell()
        {
            if (InputRouter.CmdDeleteCell && cellSelectHandler.IsSelected && cell.Container == ContainerId.Trash)
            {
                var rect = GetComponent<RectTransform>();
                rect.DOComplete(true);
                rect.DOScale(0.1f, 0.25f).OnComplete(() => Destroy(cell.gameObject));
            }
        }
        
        private void ObserveSendToStack()
        {
            if (InputRouter.CmdSendToStack)
            {
                var isMarked = CellSelectHandler.MarkedCells
                    .Any(x => x == cellSelectHandler);

                if (isMarked)
                    Periscope.Singleton.GetActiveStack().Drop(cell, Vector2.zero);
            }
        }
    }
}