﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Periscope
{
    [RequireComponent(typeof(RectTransform))]
    [ExecuteAlways]
    public class ScaleToMatchSize : MonoBehaviour, ILayoutController
    {
        [FormerlySerializedAs("target")] [SerializeField] private RectTransform matchSizeOf = null;
        [SerializeField] private RectTransform _transform = null;
        [SerializeField] private Vector2 _originalSize;
        [SerializeField] private float padding = 0;
        [SerializeField] private float factor = 1f;

        private void Start()
        {
            _transform = GetComponent<RectTransform>();
            Debug.Assert(_transform!=null);
            _originalSize = _transform.rect.size;
            Debug.Log($"Start {_transform.rect.size}");
        }

        private void Reset()
        {
            _transform = GetComponent<RectTransform>();
            Debug.Assert(_transform!=null);
            _originalSize = _transform.rect.size;
            Debug.Log($"Reset {_transform.rect.size}");
        }
        
        private void Awake()
        {
            _transform = GetComponent<RectTransform>();
            Debug.Assert(_transform!=null);
            Debug.Log($"Awake {_transform.rect.size}");
            _originalSize = _transform.rect.size;
        }

        public void SetLayoutHorizontal()
        {
            AdjustScale();
        }

        public void SetLayoutVertical()
        {
            AdjustScale();
        }

        // TODO: Unhook this from the update loop?
        private void Update()
        {
            AdjustScale();
        }

        private void AdjustScale()
        {
            if (_originalSize.x < 0.0000001f)
            {
                _originalSize = _transform.rect.size;
            }
            if (_transform == null)
                _transform = GetComponent<RectTransform>();
            
            if (matchSizeOf != null)
            {
                var targetSize = matchSizeOf.rect.size;
                _transform.localScale = new Vector3(
                    targetSize.x / (_originalSize.x + padding),
                    targetSize.y / (_originalSize.y + padding),
                    1.0f) * factor;
            }
        }
    }
}