﻿using QTree;
using UnityEngine;
using UnityEngine.Serialization;

namespace Periscope
{
    public class CellController : MonoBehaviour
    {
        [SerializeField] private CellDetail detailPanel = null;
        [SerializeField] private Cell data = null;
        [SerializeField] private CellSelectHandler selectHandler = null;
        [SerializeField] private CellDragHandler dragHandler = null;
        [SerializeField] private CellHotkeyHandler hotkeyHandler = null;
        [SerializeField] private CellPresenter presenter = null;

        public Cell Data => data;
        public CellDetail DetailPanel => detailPanel;
        public CellPresenter Presenter => presenter;
        public CellHotkeyHandler Hotkeys => hotkeyHandler;
        public CellDragHandler Dragable => dragHandler;
        public CellSelectHandler Selectable => selectHandler;

        // Start is called before the first frame update
        void Awake()
        {
            Debug.Assert(Data != null, $"{data} is assignable in editor.");
            Debug.Assert(DetailPanel != null, $"{nameof(DetailPanel)} is assignable in editor.");
            Debug.Assert(Selectable != null, $"{nameof(Selectable)} is assignable in editor.");
            Debug.Assert(Dragable != null, $"{nameof(Dragable)} is assignable in editor.");
            Debug.Assert(Hotkeys != null, $"{nameof(Hotkeys)} is assignable in editor.");
            Debug.Assert(Presenter != null, $"{nameof(Presenter)} is assignable in editor.");
        }
    }
}