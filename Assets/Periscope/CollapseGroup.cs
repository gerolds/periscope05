﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Periscope
{
    public class CollapseGroup : MonoBehaviour, IPointerClickHandler
    {
        private bool _isExpanded = true;
         public Image targetGraphic;
         public CanvasGroup targetGroup;
         public Sprite collapsed;
         public Sprite expanded;
         public float transitionDuration = 0.1f;

        void Toggle()
        {
            _isExpanded = !_isExpanded;
            targetGroup.DOFade(_isExpanded ? 1.0f : 0.0f, transitionDuration);
            targetGraphic.sprite = _isExpanded ? expanded : collapsed;
        }

        private void Update()
        {
            // TODO: Unhook this from the update loop?

            if (InputRouter.CmdTogglePanels)
            {
                Toggle();
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Toggle();
        }
    }
}