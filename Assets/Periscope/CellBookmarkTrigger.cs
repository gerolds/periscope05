using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Periscope
{
    public class CellBookmarkTrigger : MonoBehaviour
    {
        [FormerlySerializedAs("cellData")] [SerializeField] private Cell cell = null;
        [SerializeField] private CellSelectHandler cellSelectHandler = null;
        [SerializeField] private CanvasGroup targetGroup = null;
        [SerializeField] private Button targetButton = null;
        [SerializeField] private Image targetGraphic = null;
        [SerializeField] private Sprite addBookmark = null;
        [SerializeField] private Sprite removeBookmark = null;
        private bool _isOn;
        private const string Bookmarked = "Bookmarked";

        private void Awake()
        {
            Debug.Assert(cell, $"Trigger requires a {nameof(Cell)} reference.");
            Debug.Assert(targetButton, $"Trigger requires a {nameof(Button)} reference.");
            Debug.Assert(cellSelectHandler, $"Trigger requires a {nameof(CellSelectHandler)} reference.");
            Debug.Assert(targetGraphic, $"Trigger requires a {nameof(Image)} reference.");
            Debug.Assert(addBookmark, $"Trigger requires a {nameof(Sprite)} reference.");
            Debug.Assert(removeBookmark, $"Trigger requires a {nameof(Sprite)} reference.");
            Debug.Assert(targetGroup, $"Trigger requires a {nameof(CanvasGroup)} reference.");
        }

        private void Start()
        {
            if (!cell)
                return;

            targetButton.onClick.AddListener(Toggle);
            _isOn = cell.HasTag(Bookmarked);
            if (_isOn)
                On();
            else
                Off();
        }

        private void Toggle()
        {
            if (!cell)
                return;

            if (_isOn)
                Off();
            else
                On();
        }

        private void Off()
        {
            cell.RemoveTag(Bookmarked);
            _isOn = false;
            if (targetGraphic)
                targetGraphic.sprite = addBookmark;
            if (targetGroup)
                targetGroup.DOFade(0.0f, 0.25f);
        }

        private void On()
        {
            cell.AddTag(Bookmarked);
            _isOn = true;
            if (targetGraphic)
                targetGraphic.sprite = removeBookmark;
            if (targetGroup)
                targetGroup.DOFade(1.0f, 0.25f);
        }

        private void Update()
        {
            // TODO: Can this be unhooked from the update loop?
            if (cellSelectHandler.IsSelected && InputRouter.CmdMarkCell && targetButton.interactable)
            {
                Toggle();
            }
        }
    }
}