﻿using TMPro;
using UnityEngine;

namespace Periscope
{
    public class AppTitleBox : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI titleTarget = null;

        void Start()
        {
#if UNITY_STANDALONE
            var buildId = Application.buildGUID.Substring(0, Mathf.Min(8, Application.buildGUID.Length));
            titleTarget.text = $"<b><size=18>{Application.productName} {Application.version}</size></b>\n" +
                               "P R O T O T Y P E\n" +
                               $"<color=#999><size=9>Build {buildId}</size></color>";
#elif UNITY_WEBGL
            var buildId = Application.buildGUID.Substring(0, Mathf.Min(8, Application.buildGUID.Length));
            titleTarget.text = $"<b><size=18>{Application.productName} {Application.version}</size></b>\n" +
                               "P R O T O T Y P E\n" +
                               $"<color=#999><size=9>Build {buildId}</size></color>";
#else
            titleTarget.text = $"<b><size=18>{Application.productName} {Application.version}</size></b>\n" +
                               "P R O T O T Y P E\n";
#endif
        }
    }
}