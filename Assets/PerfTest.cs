﻿using UnityEngine;

public class PerfTest : MonoBehaviour
{

    public int instances;
    public GameObject prefab;
    public static float foo = 1.02f;

    public void Start()
    {
        for (var i = 0; i < instances; i++)
        {
            Instantiate(prefab, Random.insideUnitSphere * 10f, Quaternion.identity);
        }
    }
}
