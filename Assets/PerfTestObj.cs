using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class PerfTestObj : MonoBehaviour
{
    public float rng;

    private void Start()
    {
        rng = Random.Range(0, 1000.0f);
        gameObject.name = $"{rng:f4}";
    }

    private void Update()
    {
        if (PerfTest.foo > 0)
        {
            // Do nothing
            PerfTest.foo -= rng;
        }
        else
        {
            PerfTest.foo += rng;
        }

        var p = transform.position;
        
        p = new Vector3(rng, p.y, p.z);
    }
}