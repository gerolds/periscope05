# What is periscope?

Periscope is a prototype software that helps you think in an explorative way. To do this, Periscope provides an environment that facilitates innovation: The discovery of configurations of elements that open new doors into adjacent possibilities[^1]. Deliberate effort towards innovation aims to discover these doors and to walk through them. This works best when the search for them happens at the edge of chaos, where chance connections between elements may be formed, that persist just long enough to be recognised as useful building blocks.

[^1]:  Periscope’s design was inspired by [The Origins of Order](https://books.google.de/books/about/The_Origins_of_Order.html?id=lZcSpRJz0dgC) by Stuart A. Kauffman and [The Sciences of the Artificial](https://mitpress.mit.edu/books/sciences-artificial) by Herbert A. Simon.

In the use case the current prototype version is aiming for --- deliberate innovation of man-made things --- this recognition of useful building blocks is often given by a local network of people, informally sharing and discussing their work with each other, challenging assumptions, offering interpretations and most of all, inspiration: passing around ideas as building blocks to one another. Periscope aims to add a similar quality to working on ideas with a computer, making it an interesting adversary to engage with. To do this Periscope provides an environment that offers a primordial soup of elements, chance connections and the opportunity to discover useful building blocks towards a stated purpose, track those connections, maintain a sense of overview and visual cues that can serve as a map of the emerging ideas.

Periscope was developed as part of a Bachelor’s Thesis in Computer Science at the  [Human-Centered Computing (HCC) Lab](https://www.mi.fu-berlin.de/en/inf/groups/hcc/index.html) of the Institute of Computer Science at Freie Universität Berlin.

# What does it do?

The interface of Periscope implements a simple interaction pattern that is composed of cards, the concept of a deck of cards and a grid that automatically subdivides when new cards get added or moved around, resulting in a quad tree like pattern. Cards can contain any media type (as long as it’s text, video, audio, drawings or a picture). Text and drawings can be created directly in the application (done) or imported (done), audio and video can be recorded (pending), images can be imported (done) or captured (pending).

![](stacktree.png)

# Can I try it?

A current build of the prototype is available in the [downloads](https://bitbucket.org/gerolds/periscope05/downloads/) section.